<?php
/*
  |--------------------------------------------------------------------------
  | Laravel - Magento SOAP Integration Configuration
  |--------------------------------------------------------------------------
  | You can add as many connections as you want but at least the "default"
  | is needed if not an explicit name is given.
  |
  | Names need to be UNIQUE!
  |
  |   'default'	=>	[                               // Connection name
  |       'site_url'	=>	'http://magentohost',   // Site URL ( as in example )
  |       'user'		=>	'',                     // API username
  |       'key'         =>	'',                     // API key
  |       'version'     =>  'v2'                    // API version
  |   ],
 */

return [

    'connections' => [

        // Default connection
//        'default' => [
//            'site_url' => 'http://andreimagento.dev1.nextlogic.ro',
//            'user' => 'all_resources',
//            'key' => '037/<v;urCwBQ5A',
//            'version' => 'v2'
//        ]
        
        
        'default' => [
            'site_url' => 'http://varehus.dev2.glommadata.no/',
            'user' => 'all_resources',
            'key' => '037/<v;urCwBQ5A',
            'version' => 'v2'
        ]
    ],
    // enable to see SOAP stack
    'show_stack' => true
];
