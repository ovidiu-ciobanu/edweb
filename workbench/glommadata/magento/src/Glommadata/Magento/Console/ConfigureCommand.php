<?php namespace Glommadata\Magento\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ConfigureCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'glommadata:magento:config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the configure file to app.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $this->call( 'config:publish', [ 'package' => 'glommadata/magento' ] );
    }

}
