<?php namespace Glommadata\Magento\Objects;

use Glommadata\Magento\Objects\MagentoObject;
use Glommadata\Magento\Objects\MagentoObjectCollectionException;

class MagentoObjectCollection
{

    /**
     *  @var collection - stores items
     */
    protected $collection;

    /**
     *  @var count - total number of collection items
     */
    protected $count;

    /**
     *  Constructor
     *
     *  @see Varien_Object
     */
    public function __construct()
    {
        $args = func_get_args();
        if ( empty( $args[ 0 ] ) ) {
            $args[ 0 ] = array();
        }
        $data = $args[ 0 ];

        if ( is_array( $data ) ) {
            foreach ( $data as $item ) {
                $object = new MagentoObject( $item );
                $this->collection[] = $object;
            }
        }

        $this->count = count( $this->collection );
    }

    /**
     *  Get Collection
     *
     *  @return Glommadata\Magento\Objects\MagentoObjectCollection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     *  Get First Item
     *
     *  @return MagentoObject
     */
    public function getFirstItem()
    {
        if ( count( $this->collection ) ) {
            return current( $this->collection );
        }
    }

    /**
     * Get Functions
     *
     * Generates a list of available functions for a given object
     * based on the keys in it's data collection.
     *
     * @return mixed
     */
    public function getFunctions()
    {
        if ( $this->getFirstItem() instanceof MagentoObject ) {
            return $this->getFirstItem()->getFunctions();
        }
    }

    /**
     *  Get Count
     *
     *  @return int
     */
    public function getCount()
    {
        return $this->count;
    }

}
