<?php namespace Glommadata\Magento\Facades;

use Illuminate\Support\Facades\Facade;

class MagentoSoapClient extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'magento_soap_client';
    }

}
