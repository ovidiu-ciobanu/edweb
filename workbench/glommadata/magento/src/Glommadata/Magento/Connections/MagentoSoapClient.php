<?php namespace Glommadata\Magento\Connections;

use Glommadata\Magento\Connections\MagentoSoapClientException;
use Glommadata\Magento\Connections\MagentoSoapConfigurationException;
use Glommadata\Magento\Objects\MagentoObjectCollection;
use Glommadata\Magento\Objects\MagentoObject;

class MagentoSoapClient extends \SoapClient
{

    /**
     * 	@var wsdl
     */
    protected $wsdl;

    /**
     * 	@var connection
     */
    protected $connection;

    /**
     * 	@var client
     */
    protected $session;

    /**
     * 	@var results
     */
    protected $results;

    /**
     * 	@var debugTrace
     */
    protected $debugTrace;

    /**
     * 	Constructor
     *
     * 	@return void
     */
    public function __construct( $connection = null, $options = array( 'trace' => true, 'exceptions' => true ) )
    {
        if ( !is_null( $connection ) ) {
            try {
                $this->connection = $connection[ key( $connection ) ];
                $this->wsdl = $this->getConstructedUrl();
                
                parent::__construct( $this->wsdl, $options );

                $this->session = $this->login( $this->connection[ 'user' ], $this->connection[ 'key' ] );
            } catch ( Exception $e ) {
                throw new MagentoSoapClientException( '__construct: ' . $e->getMessage() );
            }
        }
    }

    /**
     * 	Capture and format SOAP calls for both
     * 	v1 and v2 of the Magento api. 
     */
    public function __call( $method, $args )
    {
        if ( is_array( $args ) && count( $args ) == 1 ) {
            $args = current( $args );
        }

        try {
            if ( $method == 'login' ) {
                $this->results = $this->__soapCall( $method, $args );
            } elseif ( $method == 'call' ) {
                $this->results = $this->__soapCall( $method, $args );
            } else {
                if ( is_array( $args ) && count( $args ) > 0 ) {
                    $this->results = $this->__soapCall( $method, array( $this->session ) + $args );
                } else {
                    $this->results = $this->__soapCall( $method, array( $this->session ) );
                }
            }

            return $this->getResultsCollections();
        } catch ( \Exception $e ) {
            throw new MagentoSoapClientException( '__call: ' . $e->getMessage() );
        }
    }

    public function call( $method, $params )
    {
        try {
            $data = array_merge( array( $this->session, $method ), array( $params ) );
            return parent::call( $data );
        } catch ( Exception $e ) {
            throw new MagentoSoapClientException( 'call: ' . $e->getMessage() );
        }
    }

    /**
     * 	Get Results Collection
     *
     * Determines whether to return a formatted collection for
     * array returns, a single object for single array returns
     * or simply the response, for single var/int returns
     *
     * 	@return mixed
     */
    public function getResultsCollections()
    {
        if ( is_array( $this->results ) && $this->isMulti() ) {

            return new MagentoObjectCollection( $this->results );
        } elseif ( is_array( $this->results ) && !$this->isMulti() ) {

            return new MagentoObject( $this->results );
        } elseif ( is_object( $this->results ) ) {

            return new MagentoObject( $this->results );
        } else {

            return $this->results;
        }
    }

    /**
     * 	Get Functions
     *
     * 	Extension of the __getFunctions method core to SoapClient
     * 	
     * 	@return array
     */
    public function getFunctions()
    {
        return $this->__getFunctions();
    }

    /**
     * 	Get Last Response
     *
     * 	Extension of the __getLastResponse method core to SoapClient
     * 	
     * 	@return array
     */
    public function getLastResponse()
    {
        return $this->__getLastResponse();
    }

    /**
     * 	Get Soap Version
     *
     * 	Returns the selected version of the SOAP API	
     *
     * 	@return string
     */
    public function getSoapVersion()
    {

        $version = isset( $this->connection[ 'version' ] ) ? $this->connection[ 'version' ] : '';

        if ( isset( $this->connection[ 'version' ] ) && in_array( strtolower( $this->connection[ 'version' ] ), $this->getAvailableVersions() ) ) {

// Return the requested version
            return strtolower( $this->connection[ 'version' ] );
        } elseif ( !isset( $this->connection[ 'version' ] ) || !strlen( $this->connection[ 'version' ] ) ) {

// Fallback to v2 if no version is supplied
            return end( $this->getAvailableVersions() );
        }
        throw new MagentoSoapConfigurationException( "The supplied version [$version] is invalid. Please check your configuration." );
    }

    /**
     * 	Get Applicable Soap Version
     *
     * 	Returns an array of possible API versoins
     *
     * 	@return string
     * 	@todo add support for XML responses
     */
    public function getAvailableVersions()
    {
        return array( 'v1', 'v2' );
    }

    /**
     * 	Is Multi
     *
     * 	Check whether the returned result is a multi-level array
     *
     * 	@return bool
     */
    protected function isMulti()
    {
        return isset( $this->results[ 0 ] );
    }

    /**
     * 	Test SOAP Connection
     *
     * 	Returns either a boolean reponse or response headers, depending
     * 	on whether $showHeaders is set to true.
     *
     * 	@return mixed
     */
    public function testConnection( $connection, $showHeaders )
    {
        $testUrl = $this->getConstructedUrl( $connection );
        try {
            file_get_contents( $testUrl );
            if ( $showHeaders === true ) {
                return $http_response_header;
            }
            $responseCode = array_values( $http_response_header )[ 0 ];
            return strpos( $responseCode, '200 OK' ) === false ? false : true;
        } catch ( \Exception $e ) {
            if ( $showHeaders === true ) {
                return "No response headers. Test failed.";
            }

            return false;
        }
    }

    /**
     * 	Get Magento Version
     *
     * 	Returns the Magento build version for either the default
     * 	connection or the connection passed through to the function
     *
     * 	@return string
     * 	@example 
     */
    public function getMagentoVersion()
    {
        $version = $this->getSoapVersion();
        switch ( $version ) {
            case 'v1':
                $response = $this->call( 'core_magento.info', array() );
                break;

            case 'v2':
                $response = $this->__call( 'magentoInfo', array() );
                break;
        }

        if ( isset( $response ) ) {
            return sprintf( '%s %s', $response->getMagentoEdition(), $response->getMagentoVersion() );
        }
    }

    /**
     * 	Construct URL
     *
     * 	Used to created either a Soap V1 or V2 URL for the WSDL
     * 	based on the configuration for the primary connection.
     *
     * 	@return string
     * 	@todo modify to work with rewrites
     */
    private function getConstructedUrl( $connection = null )
    {
        $url = is_null( $connection ) ? rtrim( $this->connection[ 'site_url' ], '/' ) : rtrim( $connection[ key( $connection ) ][ 'site_url' ], '/' );
        $version = is_null( $connection ) ? strtolower( $this->connection[ 'version' ] ) : strtolower( $connection[ key( $connection ) ][ 'version' ] );
        $wsdl_url = '';

        switch ( $version ) {
            case 'v1':
                $wsdl_url = sprintf( "%s/api/soap/?wsdl", $url );
                break;

            case 'v2':
                $wsdl_url = sprintf( "%s/api/v2_soap?wsdl", $url );
                break;

            default:
                $wsdl_url = sprintf( "%s/api/v2_soap?wsdl", $url );
                break;
        }
        return $wsdl_url;
    }

    public function __destruct()
    {
        
    }

}
