<?php namespace Glommadata\Magento\Connections;

class ConnectionNotProvidedException extends \InvalidArgumentException
{
    
}

class InvalidConnectionException extends \InvalidArgumentException
{
    
}

class MagentoSoapClientException extends \InvalidArgumentException
{
    
}

class MagentoSoapConfigurationException extends \InvalidArgumentException
{
    
}
