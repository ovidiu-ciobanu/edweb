<?php namespace Glommadata\Magento\Connections;

class MagentoSoapStorage
{

    /**
     * 	@var services
     */
    protected $services;

    /**
     * 	@var primary
     */
    protected $primary;

    /**
     * Create an instance of available services and primary connection
     *
     * @return void
     */
    public function __construct()
    {
        $this->primary = \Cache::has( 'magento_soap_primary' ) ? \Cache::get( 'magento_soap_primary' ) : 'default';
        $this->services = \Cache::has( 'magento_soap_services' ) ? \Cache::get( 'magento_soap_services' ) : array();
    }

    /**
     * 	Register Connection
     *
     * 	@return Glommadata\Magento\Connections\MagentoSoapClient
     */
    public function add( $connection )
    {
        if ( !array_key_exists( key( $connection ), $this->services ) ) {
            $this->services[ key( $connection ) ] = $connection[ key( $connection ) ];
            \Cache::forever( 'magento_soap_services', $connection );
        }
        return $connection;
    }

    /**
     * 	Remove Connection
     *
     * 	@return Glommadata\Magento\Connections\MagentoSoapClient
     */
    public function remove( $connection )
    {
        if ( array_key_exists( key( $connection ), $this->services ) ) {
            unset( $this->services[ key( $connection ) ] );
            \Cache::forever( 'magento_soap_services', $this->services );
        }
        return $connection;
    }

    /**
     * 	Serve available connections
     *
     * 	@return array
     */
    public function services()
    {
        return $this->services;
    }

    /**
     * 	Serve primary connection
     *
     * 	@return array
     */
    public function primary( $name = null )
    {
        if ( !is_null( $name ) ) {
            \Cache::forever( 'magento_soap_primary', $name );
            $this->primary = \Cache::get( 'magento_soap_primary' );
        }
        return $this->primary;
    }

}
