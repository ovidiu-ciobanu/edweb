<?php namespace Glommadata\Magento;

use Illuminate\Support\ServiceProvider;

class MagentoServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package( 'glommadata/magento' );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMagento();
        $this->registerSoapClient();
        $this->registerSoapStorage();

        $this->registerCommands();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    /**
     * Register the Magento Facacde Accessor
     *
     * @return void
     */
    private function registerMagento()
    {
        $this->app[ 'magento' ] = $this->app->share( function($app) {
            return new Magento( $app[ 'config' ] );
        } );
    }

    /**
     * Register the Magento Soap Client
     *
     * @return void
     */
    private function registerSoapClient()
    {
        $this->app[ 'magento_soap_client' ] = $this->app->share( function($app) {
            return new Connections\MagentoSoapClient;
        } );
        $this->app->booting( function() {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias( 'MagentoSoapClient', 'Glommadata\Magento\Facades\MagentoSoapClient' );
        } );
    }

    /**
     * Register the Magento Soap Storage
     *
     * @return void
     */
    private function registerSoapStorage()
    {
        $this->app[ 'magento_soap_storage' ] = $this->app->share( function($app) {
            return new Connections\MagentoSoapStorage;
        } );
        $this->app->booting( function() {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias( 'MagentoSoapStorage', 'Glommadata\Magento\Facades\MagentoSoapStorage' );
        } );
    }

    /** Commands */
    private function registerCommands()
    {
        $this->registerConfigureCommand();

        $this->commands( 'glommadata::commands.magento.config' );
    }

    public function registerConfigureCommand()
    {
        $this->app[ 'glommadata::commands.magento.config' ] = $this->app->share( function($app) {
            return new Console\ConfigureCommand;
        } );
    }

}
