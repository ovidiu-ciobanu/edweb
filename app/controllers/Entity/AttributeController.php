<?php

namespace App\Controllers\Entity;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\Eav\Attribute as EavAttribute;
use App\Models\Eav\Attribute\Label as EavAttributeLabel;
use App\Models\Eav\Attribute\Option as AttrbiuteOption;
use App\Models\Eav\Attribute\Option\Value as AttrbiuteOptionValue;
use App\Models\Eav\Attribute\Input\Type as InputType;
use App\Models\Core\Store as CoreStore;
use App\Models\Eav\Entity\Type as EavEntityType;

class AttributeController extends BaseController
{

    protected $layout = 'layouts.main';

    /**
     * Display all attributes.
     */
    public function index()
    {
        $perPage = \Config::get( 'pagination.per_page' );
        $search_options = [
            'attribute_label' => 'Attribute label',
        ];

        if ( Input::has( 'option' ) && Input::has( 'search' ) && null !== Input::get( 'search', null ) ) {
            if ( array_key_exists( Input::get( 'option', null ), $search_options ) ) {
                $selected_eav_attribute = EavAttributeLabel::where( 'value', 'like', '%' . Input::get( 'search' ) . '%' )->get();
                $attributes = EavAttribute::whereIn( 'attribute_id', $selected_eav_attribute->lists( 'attribute_id' ) )->paginate( $perPage );
            }
        } else {
            $attributes = EavAttribute::paginate( $perPage );
        }

        $this->layout->content = View::make( 'entity.attribute.index' )->with( [ 'attributes' => $attributes, 'search_options' => $search_options ] );
    }

    /**
     * Create attribute.
     */
    public function create()
    {
        $input_types = InputType::all()->lists( 'input_type_name', 'input_type_code' );
        $stores = CoreStore::all()->lists( 'name', 'store_id' );
        $entity_types = EavEntityType::all()->lists( 'entity_type_name', 'entity_type_id' );

        $this->layout->content = View::make( 'entity.attribute.create' )->with( [ 'input_types' => $input_types, 'stores' => $stores, 'entity_types' => $entity_types ] );
    }

    /**
     * Store the information from create page.
     * 
     * @return void
     */
    public function store()
    {
        $store = CoreStore::first();

        $attribute_validator = Validator::make( Input::get( 'attribute' ), EavAttribute::rules() );
        $frontend_label_first_validator = Validator::make( Input::get( 'frontend_label' ), array( $store->store_id => 'required' ) );

        if ( $attribute_validator->passes() && $frontend_label_first_validator->passes() ) {

            $attribute = new EavAttribute();

            $attribute->entity_type_id = Input::get( 'attribute.entity_type' );
            $attribute->attribute_code = Input::get( 'attribute.code' );
            $attribute->frontend_input = Input::get( 'attribute.input_type' );
            $attribute->is_required = Input::get( 'attribute.required' );

            $default_value = '';
            if ( Input::has( 'attribute.default_value_text' ) && Input::get( 'attribute.default_value_text' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_text' );
            } elseif ( Input::has( 'attribute.default_value_decimal' ) && Input::get( 'attribute.default_value_decimal' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_decimal' );
            } elseif ( Input::has( 'attribute.default_value_datetime' ) && Input::get( 'attribute.default_value_datetime' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_datetime' );
            } elseif ( Input::has( 'attribute.default_value_textarea' ) && Input::get( 'attribute.default_value_textarea' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_textarea' );
            } elseif ( Input::has( 'attribute.default_value_boolean' ) && Input::get( 'attribute.default_value_boolean' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_boolean' );
            }
            $attribute->default_value = $default_value;

            $attribute->save();

            $admin_attribute_label = '';
            $first = true;
            foreach ( Input::get( 'frontend_label' ) as $store_id => $value ) {
                if ( $first === true ) {
                    $admin_attribute_label = $value;
                    $first = false;
                }

                $attribute_label = new EavAttributeLabel();

                $attribute_label->attribute_id = $attribute->attribute_id;
                $attribute_label->store_id = $store_id;
                $attribute_label->value = ( $value != '' ) ? $value : $admin_attribute_label;

                $attribute_label->save();
            }

            if ( Input::has( 'option' ) ) {
                foreach ( Input::get( 'option' ) as $key => $options ) {

                    $attribute_option = new AttrbiuteOption();
                    $attribute_option->attribute_id = $attribute->attribute_id;
                    $attribute_option->sort_order = (int) Input::get( 'option.' . $key . '.sort_order' );
                    $attribute_option->save();

                    $first = true;
                    foreach ( $options['value'] as $store_id => $value ) {

                        if ( $first && '' == $value ) {
                            $attribute_option->delete();
                            break;
                        }

                        if ( $first ) {
                            $admin_attribute_option = $value;
                            $first = false;
                        }

                        $attribute_option_value = new AttrbiuteOptionValue();

                        $attribute_option_value->option_id = $attribute_option->option_id;
                        $attribute_option_value->store_id = $store_id;
                        $attribute_option_value->value = ( '' != $value ) ? $value : $admin_attribute_option;

                        $attribute_option_value->save();
                    }
                }
            }

            return Redirect::to( 'entity/attribute' )->withMessage( 'Attribute created!' );
        } else {
            $error_messages = array_merge_recursive(
                    $attribute_validator->messages()->toArray(), $frontend_label_first_validator->messages()->toArray() );

            return Redirect::back()->withErrors( $error_messages )->withInput();
        }
    }

    /**
     * Edit attribute.
     * 
     * @param int $attribute_id
     * @return void
     */
    public function edit( $attribute_id )
    {
        $attribute = EavAttribute::find( $attribute_id );

        if ( null == $attribute ) {
            return Redirect::to( 'entity/attribute' );
        }

        $stores = CoreStore::all()->lists( 'name', 'store_id' );

        $this->layout->content = View::make( 'entity.attribute.edit' )->with( [ 'stores' => $stores, 'attribute' => $attribute ] );
    }

    /**
     * Update the information from edit page.
     * 
     * @param int $attribute_id
     * @return void
     */
    public function update( $attribute_id )
    {
        $attribute = EavAttribute::find( $attribute_id );

        if ( null == $attribute ) {
            return Redirect::to( 'entity/attribute' );
        }

        // First key from labels is required
        $first_key = array_keys( Input::get( 'frontend_label' ) )[0];

        $attribute_validator = Validator::make( Input::get( 'attribute' ), EavAttribute::rules( $attribute->attribute_id ) );
        $frontend_label_first_validator = Validator::make( Input::get( 'frontend_label' ), [ $first_key => 'required' ], [ $first_key => 'The Admin title is required.' ] );

        if ( $attribute_validator->passes() && $frontend_label_first_validator->passes() ) {

            $attribute->is_required = Input::get( 'attribute.required' );

            $default_value = '';
            if ( Input::has( 'attribute.default_value_text' ) && Input::get( 'attribute.default_value_text' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_text' );
            } elseif ( Input::has( 'attribute.default_value_decimal' ) && Input::get( 'attribute.default_value_decimal' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_decimal' );
            } elseif ( Input::has( 'attribute.default_value_datetime' ) && Input::get( 'attribute.default_value_datetime' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_datetime' );
            } elseif ( Input::has( 'attribute.default_value_textarea' ) && Input::get( 'attribute.default_value_textarea' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_textarea' );
            } elseif ( Input::has( 'attribute.default_value_boolean' ) && Input::get( 'attribute.default_value_boolean' ) != '' ) {
                $default_value = Input::get( 'attribute.default_value_boolean' );
            }
            $attribute->default_value = $default_value;
            if ( 0 < count( $attribute->isDirty() ) ) {
                $attribute->save();
            }

            $admin_attribute_label = '';
            $first = true;
            foreach ( Input::get( 'frontend_label' ) as $attribute_label_id => $value ) {
                if ( $first ) {
                    $admin_attribute_label = $value;
                    $first = false;
                }

                $attribute_label = EavAttributeLabel::find( $attribute_label_id );

                if ( null == $attribute_label ) {
                    break;
                }

                $attribute_label->value = ( '' != $value ) ? $value : $admin_attribute_label;

                if ( 0 < count( $attribute_label->isDirty() ) ) {
                    $attribute_label->save();
                }
            }

            // Delete options that are marked as delete
            if ( Input::has( 'delete_option' ) ) {
                AttrbiuteOption::whereIn( 'option_id', Input::get( 'delete_option' ) )->delete();
            }

            // Update old options
            if ( Input::has( 'old_option' ) ) {
                foreach ( Input::get( 'old_option' ) as $option_id => $options ) {

                    $attribute_option = AttrbiuteOption::find( $option_id );
                    if ( null == $attribute_option ) {
                        break;
                    }
                    $attribute_option->sort_order = (int) Input::get( 'old_option.' . $option_id . '.sort_order' );

                    if ( 0 < count( $attribute_option->isdirty() ) ) {
                        $attribute_option->save();
                    }

                    $first = true;
                    foreach ( $options['value'] as $value_id => $value ) {

                        if ( $first && '' == $value ) {
                            $attribute_option->delete();
                            break;
                        }

                        if ( $first ) {
                            $admin_attribute_option = $value;
                            $first = false;
                        }

                        $attribute_option_value = AttrbiuteOptionValue::find( $value_id );

                        if ( null == $attribute_option_value ) {
                            break;
                        }

                        $attribute_option_value->value = ( '' != $value ) ? $value : $admin_attribute_option;

                        if ( 0 < count( $attribute_option_value->isDirty() ) ) {
                            $attribute_option_value->save();
                        }
                    }
                }
            }

            // Add new options
            if ( Input::has( 'option' ) ) {
                foreach ( Input::get( 'option' ) as $key => $options ) {

                    $attribute_option = new AttrbiuteOption();
                    $attribute_option->attribute_id = $attribute->attribute_id;
                    $attribute_option->sort_order = (int) Input::get( 'option.' . $key . '.sort_order' );
                    $attribute_option->save();

                    $first = true;
                    foreach ( $options['value'] as $store_id => $value ) {

                        if ( $first && '' == $value ) {
                            $attribute_option->delete();
                            break;
                        }

                        if ( $first ) {
                            $admin_attribute_option = $value;
                            $first = false;
                        }

                        $attribute_option_value = new AttrbiuteOptionValue();

                        $attribute_option_value->option_id = $attribute_option->option_id;
                        $attribute_option_value->store_id = $store_id;
                        $attribute_option_value->value = ( '' != $value ) ? $value : $admin_attribute_option;

                        $attribute_option_value->save();
                    }
                }
            }

            return Redirect::to( 'entity/attribute' )->withMessage( 'Attribute updated!' );
        } else {

            $error_messages = array_merge_recursive(
                    $attribute_validator->messages()->toArray(), $frontend_label_first_validator->messages()->toArray() );

            return Redirect::back()->withErrors( $error_messages )->withInput();
        }
    }

    /**
     * Delete attribute.
     * Deleting from other tables are managed by the database throw foreign keys.
     * 
     * @param int $attribute_id
     * @return void
     */
    public function destroy( $attribute_id )
    {
        EavAttribute::find( $attribute_id )->delete();

        return Redirect::back()->withMessage( 'Attribute deleted!' );
    }

}
