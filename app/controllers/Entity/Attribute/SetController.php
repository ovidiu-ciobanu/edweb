<?php namespace App\Controllers\Entity\Attribute;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\Eav\Attribute as EavAttribute;
use App\Models\Eav\Attribute\Set as EavAttributeSet;
use App\Models\Eav\Attribute\Group as EavAttributeGroup;
use App\Models\Eav\Attribute\Group\Attribute as EavAttributeGroupAttribute;
use App\Models\Eav\Entity\Type as EavEntityType;

class SetController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $this->layout->content = View::make( 'entity.attribute.set.index' )->with( 'attribute_sets', EavAttributeSet::all() );
    }

    public function create()
    {
        $entity_types = EavEntityType::all()->lists( 'entity_type_name', 'entity_type_id' );
        $this->layout->content = View::make( 'entity.attribute.set.create' )->with( [ 'attributes' => EavAttribute::all(), 'entity_types' => $entity_types ] );
    }

    public function store()
    {
        $attribute_set = new EavAttributeSet();

        $attribute_set->entity_type_id = Input::get( 'entity_type' );
        $attribute_set->attribute_set_name = Input::get( 'name' );

        $attribute_set->save();

        foreach ( Input::get( 'group_name' ) as $key => $group_name ) {

            $attribute_group = new EavAttributeGroup();
            $attribute_group->attribute_set_id = $attribute_set->attribute_set_id;
            $attribute_group->attribute_group_name = $group_name;
            $attribute_group->sort_order = Input::get( 'group_sort_order.' . $key );

            $attribute_group->save();

            foreach ( Input::get( 'group.' . $key ) as $attribute_id ) {

                $attribute_group_attribute = new EavAttributeGroupAttribute();

                $attribute_group_attribute->attribute_set_id = $attribute_set->attribute_set_id;
                $attribute_group_attribute->attribute_group_id = $attribute_group->attribute_group_id;
                $attribute_group_attribute->attribute_id = $attribute_id;
                $attribute_group_attribute->sort_order = Input::get( 'attribute_sort_order.' . $attribute_id );

                $attribute_group_attribute->save();
            }
        }

        return Redirect::to( 'entity/attribute/set' )->withMessage( 'Attribute set created!' );
    }

    public function edit( $attribute_set_id )
    {
        $attribute_set = EavAttributeSet::find( $attribute_set_id );

        if ( null === $attribute_set ) {
            return Redirect::to( 'entity/attribute/set' );
        }

        $attribute_group_attributes = $attribute_set->groups_attributes->lists( 'attribute_id' );
        $attributes = EavAttribute::where( 'entity_type_id', $attribute_set->entity_type_id )->whereNotIn( 'attribute_id', $attribute_group_attributes )->get();

        $this->layout->content = View::make( 'entity.attribute.set.edit' )->with( [ 'attribute_set' => $attribute_set, 'attributes' => $attributes ] );
    }

    public function update( $attribute_set_id )
    {
        $attribute_set = EavAttributeSet::find( $attribute_set_id );

        if ( null === $attribute_set ) {
            return Redirect::to( 'entity/attribute/set' );
        }

        $attribute_set->attribute_set_name = Input::get( 'name' );

        if ( count( $attribute_set->isDirty() ) > 0 ) {
            $attribute_set->save();
        }

        $array_group_ids = $attribute_set->groups()->lists( 'attribute_group_id' );
        $array_attribute_ids = $attribute_set->groups_attributes->lists( 'attribute_id' );

        foreach ( Input::get( 'group_name', array() ) as $key => $group_name ) {

            if ( in_array( $key, $array_group_ids ) ) {
                $attribute_group = EavAttributeGroup::find( $key );
                $array_group_ids = array_diff( $array_group_ids, [ $key ] );
            } else {
                $attribute_group = new EavAttributeGroup();
            }

            $attribute_group->attribute_set_id = $attribute_set_id;
            $attribute_group->attribute_group_name = $group_name;
            $attribute_group->sort_order = Input::get( 'group_sort_order.' . $key );

            $attribute_group->save();

            foreach ( Input::get( 'group.' . $key, array() ) as $attribute_id ) {

                if ( in_array( $attribute_id, $array_attribute_ids ) ) {
                    $attribute_group_attribute = EavAttributeGroupAttribute::where( 'attribute_id', '=', $attribute_id )->where( 'attribute_set_id', '=', $attribute_set_id )->first();
                    $array_attribute_ids = array_diff( $array_attribute_ids, [ $attribute_id ] );
                } else {
                    $attribute_group_attribute = new EavAttributeGroupAttribute();
                }

                $attribute_group_attribute->attribute_set_id = $attribute_set_id;
                $attribute_group_attribute->attribute_group_id = $attribute_group->attribute_group_id;
                $attribute_group_attribute->attribute_id = $attribute_id;
                $attribute_group_attribute->sort_order = Input::get( 'attribute_sort_order.' . $attribute_id );

                $attribute_group_attribute->save();
            }
        }

        if ( count( $array_group_ids ) > 0 ) {
            EavAttributeGroup::whereIn( 'attribute_group_id', $array_group_ids )->delete();
        }
        if ( count( $array_attribute_ids ) > 0 ) {
            EavAttributeGroupAttribute::whereIn( 'attribute_id', $array_attribute_ids )->where( 'attribute_set_id', '=', $attribute_set_id )->delete();
        }

        return Redirect::to( 'entity/attribute/set' )->withMessage( 'Attribute set updated!' );
    }

    public function destroy( $attribute_set_id )
    {
        EavAttributeSet::find( $attribute_set_id )->delete();

        return Redirect::to( 'entity/attribute/set' )->withMessage( 'Attribute set deleted!' );
    }

}
