<?php

namespace App\Controllers\Catalog;

use \Input;
use \Redirect;
use \Session;
use \View;
use \Validator;
use App\Controllers\BaseController;
use App\Models\Eav\Attribute\Set as EavAttributeSet;
use App\Models\Eav\Entity\Type as EavEntityType;
use App\Models\Entity\Category as Category;
use App\Models\Entity\Special as Special;
use App\Models\Eav\Attribute as EavAttribute;
use App\Models\Eav\Attribute\Input\Type as EavAttributeInputType;
use App\Models\Eav\Entity\Text as EavEntityText;
use App\Models\Eav\Entity\Varchar as EavEntityVarchar;
use App\Models\Eav\Entity\Int as EavEntityInt;
use App\Models\Eav\Entity\Decimal as EavEntityDecimal;
use App\Models\Eav\Entity\Datetime as EavEntityDatetime;

class CategoryController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $perPage = \Config::get( 'pagination.per_page' );
        $search_options = [
            'category_name' => 'Category name',
        ];

        if ( Input::has( 'option' ) && Input::has( 'search' ) && null !== Input::get( 'search', null ) ) {
            if ( array_key_exists( Input::get( 'option', null ), $search_options ) ) {
                $selected_eav_attribute = EavAttribute::where( 'attribute_code', '=', Input::get( 'option' ) )->first();
                $eav_entity_attributes = EavEntityVarchar::where( 'attribute_id', '=', $selected_eav_attribute->attribute_id )->where( 'entity_type_id', '=', $selected_eav_attribute->entity_type_id )->where( 'store_id', '=', Session::get( 'store_id' ) )->where( 'value', 'like', '%' . Input::get( 'search' ) . '%' )->get();
                $categories = Category::whereIn( 'entity_id', $eav_entity_attributes->lists( 'entity_id' ) )->paginate( $perPage );
            }
        } else {
            $categories = Category::paginate( $perPage );
        }

        $this->layout->content = View::make( 'catalog.category.index' )->with( [ 'categories' => $categories, 'search_options' => $search_options ] );
    }

    public function create()
    {
        $entity_type_id = EavEntityType::where( 'entity_type_code', EavEntityType::CATEGORY_CODE )->first()->entity_type_id;
        $attribute_sets = EavAttributeSet::where( 'entity_type_id', $entity_type_id )->lists( 'attribute_set_name', 'attribute_set_id' );
        $this->layout->content = View::make( 'catalog.category.create' )->with( [ 'attribute_sets' => $attribute_sets ] );
    }

    public function store()
    {
        $validator = Validator::make( Input::all(), [ 'attribute.set' => [ 'required', 'numeric', 'exists:eav_attribute_set,attribute_set_id' ] ] );

        if ( $validator->passes() ) {

            $category_entity_type = EavEntityType::where( 'entity_type_code', EavEntityType::CATEGORY_CODE )->first();

            $category_entity = new Category();
            $category_entity->entity_type_id = $category_entity_type->entity_type_id;
            $category_entity->attribute_set_id = Input::get( 'attribute.set' );
            $category_entity->save();

            return Redirect::route( 'catalog.category.edit', [ 'id' => $category_entity->entity_id ] );
        } else {
            return Redirect::back()->withErrors( $validator );
        }
    }
    
    public function destroy ($category_id) {
        Category::delete_category($category_id);
        
        return Redirect::to( 'catalog/category' );
    }
    
    public function destroy_all() {
        $categories = Category::all();
        foreach ($categories as $category) {
            Category::delete_category($category->entity_id);
        }
        return Redirect::to( 'catalog/category' );
    }

    public function edit( $category_id ) {
        $select_options = [
            0 => '',
            1 => 'Products with no category',
            2 => 'SKU'
        ];

        $category = Category::find( $category_id );

        if ( Input::has( 'option' ) && Input::get( 'option' ) > 0 && isset( $select_options[Input::get( 'option' )] ) ) {
            switch ( Input::get( 'option' ) ) {
                case 1:
                    $products = \App\Models\Entity\Product::whereNotIn( 'entity_id', function($query) {
                                $query->select( 'product_id' )
                                        ->from( 'category_product' );
                            } );
                    break;

                case 2:
                    $product_entity_type = EavEntityType::where( 'entity_type_code', EavEntityType::PRODUCT_CODE )->first();
                    $skuAttribute = EavAttribute::where( 'attribute_code', '=', 'sku' )->first();
                    $product_ids = EavEntityVarchar::where( 'value', 'like', '%' . (Input::has( 'search' ) ? Input::get( 'search' ) : '') . '%' )
                                    ->where( 'entity_type_id', '=', $product_entity_type->entity_type_id )
                                    ->where( 'attribute_id', '=', $skuAttribute->attribute_id )
                                    ->where( 'store_id', '=', Session::get( 'store_id' ) )->get()->lists( 'entity_id' );


                    $products = \App\Models\Entity\Product::whereIn( 'entity_id', $product_ids );
                    break;
            }
        } else {
            $products = $category->products();
        }
        $products = $products->paginate( \Config::get( 'pagination.per_page' ) );

        $this->layout->content = View::make( 'catalog.category.edit' )->with( [ 'category' => $category, 'products' => $products, 'select_options' => $select_options ] );
    }

    public function update( $category_id )
    {
        $category = Category::find( $category_id );

        if ( $category == null ) {
            return Redirect::to( 'catalog/category' );
        }

        foreach ( Input::get( 'eav.attribute' ) as $attribute_id => $value ) {

            $attribute = EavAttribute::find( $attribute_id );
            $eav_entity_value = null;

            if ( $attribute->frontend_input == EavAttributeInputType::TEXTAREA ) {

                $eav_entity_value = EavEntityText::where( 'entity_type_id', $category->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $category_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityText();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::TEXT ) {

                $eav_entity_value = EavEntityVarchar::where( 'entity_type_id', $category->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $category_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityVarchar();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::BOOLEAN ) {
                $eav_entity_value = EavEntityInt::where( 'entity_type_id', $category->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $category_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityInt();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::DECIMAL ) {
                $eav_entity_value = EavEntityDecimal::where( 'entity_type_id', $category->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $category_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityDecimal();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::DATETIME ) {
                $eav_entity_value = EavEntityDatetime::where( 'entity_type_id', $category->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $category_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityDatetime();
                }
            }

            if ( $eav_entity_value !== null ) {

                $eav_entity_value->entity_type_id = $category->entity_type_id;
                $eav_entity_value->attribute_id = $attribute_id;
                $eav_entity_value->store_id = Session::get( 'store_id' );
                $eav_entity_value->entity_id = $category_id;
                $eav_entity_value->value = $value;

                if ( count( $eav_entity_value->isDirty() ) > 0 ) {
                    $eav_entity_value->save();
                }
            }
        }

        // Change profit %
        $special_profit_percent_value = Input::get( 'specials.profit_percent' );
        if ( $special_profit_percent_value == '' ) {
            $special_profit_percent_value = 0;
        }
        $special_profit_percent = new Special();
        $special_profit_percent->entity_id = $category->entity_id;
        $special_profit_percent->entity_type_id = $category->entity_type_id;
        $special_profit_percent->type = 'profit_percent';
        $special_profit_percent->value = $special_profit_percent_value;

        $category->special()->save( $special_profit_percent );

        return Redirect::to( 'catalog/category' );
    }

}
