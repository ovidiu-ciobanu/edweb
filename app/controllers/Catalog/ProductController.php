<?php

namespace App\Controllers\Catalog;

use \View;
use \Validator;
use \Input;
use \Redirect;
use \Session;
use \Paginator;
use App\Controllers\BaseController;
use App\Models\Eav\Attribute\Set as EavAttributeSet;
use App\Models\Entity\Product as Product;
use App\Models\Entity\Category as Category;
use App\Models\Entity\Special as Special;
use App\Models\Eav\Entity\Type as EavEntityType;
use App\Models\Eav\Attribute as EavAttribute;
use App\Models\Eav\Attribute\Input\Type as EavAttributeInputType;
use App\Models\Eav\Entity\Text as EavEntityText;
use App\Models\Eav\Entity\Varchar as EavEntityVarchar;
use App\Models\Eav\Entity\Int as EavEntityInt;
use App\Models\Eav\Entity\Decimal as EavEntityDecimal;
use App\Models\Eav\Entity\Datetime as EavEntityDatetime;

class ProductController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $perPage = \Config::get( 'pagination.per_page' );
        $search_options = [
            'sku' => 'SKU',
            'product_name' => 'Product name'
        ];

        if ( Input::has( 'option' ) && Input::has( 'search' ) && null !== Input::get( 'search', null ) ) {
            if ( array_key_exists( Input::get( 'option', null ), $search_options ) ) {
                $selected_eav_attribute = EavAttribute::where( 'attribute_code', '=', Input::get( 'option' ) )->first();
                $eav_entity_attributes = EavEntityVarchar::where( 'attribute_id', '=', $selected_eav_attribute->attribute_id )->where( 'entity_type_id', '=', $selected_eav_attribute->entity_type_id )->where( 'store_id', '=', Session::get( 'store_id' ) )->where( 'value', 'like', '%' . Input::get( 'search' ) . '%' )->get();
                $products = Product::whereIn( 'entity_id', $eav_entity_attributes->lists( 'entity_id' ) )->paginate( $perPage );
            }
        } else {
            $products = Product::paginate( $perPage );
        }

        $this->layout->content = View::make( 'catalog.product.index' )->with( [ 'products' => $products, 'search_options' => $search_options ] );
    }
    

    public function create()
    {
        $entity_type_id = EavEntityType::where( 'entity_type_code', EavEntityType::PRODUCT_CODE )->first()->entity_type_id;
        $attribute_sets = EavAttributeSet::where( 'entity_type_id', $entity_type_id )->lists( 'attribute_set_name', 'attribute_set_id' );
        $this->layout->content = View::make( 'catalog.product.create' )->with( [ 'attribute_sets' => $attribute_sets ] );
    }

    public function store()
    {
        $validator = Validator::make( Input::all(), [ 'attribute.set' => [ 'required', 'numeric', 'exists:eav_attribute_set,attribute_set_id' ] ] );

        if ( $validator->passes() ) {

            $product_entity_type = EavEntityType::where( 'entity_type_code', EavEntityType::PRODUCT_CODE )->first();

            $product_entity = new Product();
            $product_entity->entity_type_id = $product_entity_type->entity_type_id;
            $product_entity->attribute_set_id = Input::get( 'attribute.set' );
            $product_entity->save();

            return Redirect::route( 'catalog.product.edit', [ 'id' => $product_entity->entity_id ] );
        } else {
            return Redirect::back()->withErrors( $validator );
        }
    }

    public function edit( $product_id )
    {
        $product = Product::find( $product_id );
        $categories = Category::all();

        $this->layout->content = View::make( 'catalog.product.edit' )->with( [ 'product' => $product, 'categories' => $categories ] );
    }
    
    public function destroy ($product_id) {
        Product::delete_product($product_id);
        
        return Redirect::to( 'catalog/product' );
    }
    
    public function destroy_all() {
        $products = Product::all();
        foreach ($products as $product) {
            Product::delete_product($product->entity_id);
        }
        return Redirect::to( 'catalog/product' );
    }

    public function update( $product_id )
    {
        $product = Product::find( $product_id );

        if ( $product == null ) {
            return Redirect::to( 'catalog/product' );
        }

        foreach ( Input::get( 'eav.attribute' ) as $attribute_id => $value ) {

            $attribute = EavAttribute::find( $attribute_id );
            $eav_entity_value = null;

            if ( $attribute->frontend_input == EavAttributeInputType::TEXTAREA ) {

                $eav_entity_value = EavEntityText::where( 'entity_type_id', $product->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $product_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityText();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::TEXT ) {

                $eav_entity_value = EavEntityVarchar::where( 'entity_type_id', $product->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $product_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityVarchar();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::BOOLEAN ) {
                $eav_entity_value = EavEntityInt::where( 'entity_type_id', $product->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $product_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityInt();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::DECIMAL ) {
                $eav_entity_value = EavEntityDecimal::where( 'entity_type_id', $product->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $product_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityDecimal();
                }
            } elseif ( $attribute->frontend_input == EavAttributeInputType::DATETIME ) {
                $eav_entity_value = EavEntityDatetime::where( 'entity_type_id', $product->entity_type_id )->where( 'attribute_id', $attribute_id )->where( 'store_id', Session::get( 'store_id' ) )->where( 'entity_id', $product_id )->first();
                if ( $eav_entity_value === null ) {
                    $eav_entity_value = new EavEntityDatetime();
                }
            }

            if ( $eav_entity_value !== null ) {

                $eav_entity_value->entity_type_id = $product->entity_type_id;
                $eav_entity_value->attribute_id = $attribute_id;
                $eav_entity_value->store_id = Session::get( 'store_id' );
                $eav_entity_value->entity_id = $product_id;
                $eav_entity_value->value = $value;

                if ( count( $eav_entity_value->isDirty() ) > 0 ) {
                    $eav_entity_value->save();
                }
            }
        }

        // Attach product with categories
        $product->categories()->sync( (array) Input::get( 'categories' ) );

        // Change profit %
        $special_profit_percent_value = Input::get( 'specials.profit_percent' );
        if ( $special_profit_percent_value == '' ) {
            $special_profit_percent_value = 0;
        }
        $special_profit_percent = new Special();
        $special_profit_percent->entity_id = $product->entity_id;
        $special_profit_percent->entity_type_id = $product->entity_type_id;
        $special_profit_percent->type = 'profit_percent';
        $special_profit_percent->value = $special_profit_percent_value;

        $product->special()->save( $special_profit_percent );

        return Redirect::to( 'catalog/product' );
    }

}
