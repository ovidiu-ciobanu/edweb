<?php

namespace App\Controllers;

use \View;
use \Validator;
use \Input;
use \Redirect;
use \Hash;
use App\Controllers\BaseController;
use App\Models\User;

class RegisterController extends BaseController
{

    protected $layout = 'layouts.main';

    public function __construct()
    {
        $this->beforeFilter( 'csrf', [ 'on' => 'post' ] );
    }

    public function create()
    {
        $this->layout->content = View::make( 'register.create' );
    }

    public function store()
    {
        $validator = Validator::make( Input::all(), User::rules() );

        if ( $validator->passes() ) {

            $user = new User();
            $user->firstname = Input::get( 'firstname' );
            $user->lastname = Input::get( 'lastname' );
            $user->email = Input::get( 'email' );
            $user->password = Hash::make( Input::get( 'password' ) );
            $user->save();

            return Redirect::to( 'login' )->with( 'message', 'Thank you for registering!' );
        } else {
            return Redirect::to( 'register' )->withErrors( $validator )->withInput();
        }
    }

}
