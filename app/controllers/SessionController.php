<?php

namespace App\Controllers;

use \View;
use \Auth;
use \Redirect;
use \Input;
use App\Controllers\BaseController;

class SessionController extends BaseController
{

    protected $layout = 'layouts.main';

    /**
     * Display login form to user.
     */
    public function create()
    {
        $this->layout->content = View::make( 'session.create' );
    }

    /**
     * Login the user.
     */
    public function store()
    {
        if ( Auth::attempt( array( 'email' => Input::get( 'email' ), 'password' => Input::get( 'password' ) ) ) ) {
            return Redirect::to( '/' )->with( 'message', 'You are now logged in!' );
        } else {
            return Redirect::to( 'login' )
                            ->with( 'message', 'Your username/password combination was incorrect.' )
                            ->withInput();
        }
    }

    /**
     * Logout the user.
     */
    public function destroy()
    {
        Auth::logout();
        return Redirect::to( 'login' )->with( 'message', 'Your are now logged out!' );
    }

}
