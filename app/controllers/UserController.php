<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class UserController extends BaseController
{

    protected $layout = 'layouts.main';

    public function __construct()
    {
        $this->beforeFilter( 'csrf', array( 'on' => 'post' ) );
    }

    public function index()
    {
        
    }

    public function create()
    {
        
    }

    public function store()
    {
        
    }

    public function show()
    {
        
    }

    public function edit()
    {
        
    }

    public function update()
    {
        
    }

    public function destroy()
    {
        
    }

}
