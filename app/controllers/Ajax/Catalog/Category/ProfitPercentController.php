<?php

namespace App\Controllers\Ajax\Catalog\Category;

use \Input;
use App\Controllers\BaseController;
use App\Models\Entity\Category as Category;
use App\Models\Entity\Special as Special;

class ProfitPercentController extends BaseController
{

    public function store()
    {
        $profit_percent = Input::get( 'profit_percent', null );
        $categories_input = Input::get( 'categories', null );
        $all_categories_input = Input::get( 'all', null );
        $categories = null;

        if ( $categories_input !== null && is_array( $categories_input ) ) {
            $categories = Category::whereIn( 'entity_id', $categories_input )->get();
        } elseif ( $all_categories_input !== null && $all_categories_input == 1 ) {
            $categories = Category::all();
        }

        if ( $categories !== null && $profit_percent !== null ) {
            foreach ( $categories as $category ) {
                $special = Special::firstOrNew( [ 'entity_id' => $category->entity_id, 'entity_type_id' => $category->entity_type_id, 'type' => 'profit_percent' ] );
                $special->value = $profit_percent;
                $special->save();
            }
        }
    }

}
