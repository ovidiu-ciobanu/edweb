<?php

namespace App\Controllers\Ajax\Catalog;

use \Input;
use App\Controllers\BaseController;
use App\Models\Entity\Category as Category;

class CategoryController extends BaseController
{

    /**
     * Add product to category from category edit page.
     * 
     * @param int $category_id
     */
    public function update( $category_id )
    {
        $category = Category::find( $category_id );
        $products = $category->products()->lists( 'entity_id' );

        if ( Input::get( 'checked' ) == 1 ) {
            // Add product to this category
            if ( !in_array( Input::get( 'product_id' ), $products ) ) {
                $category->products()->attach( Input::get( 'product_id' ) );
            }
        } else {
            // Remove product from category
            $category->products()->detach( Input::get( 'product_id' ) );
        }
    }

}
