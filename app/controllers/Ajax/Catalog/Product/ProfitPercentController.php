<?php

namespace App\Controllers\Ajax\Catalog\Product;

use \Input;
use App\Controllers\BaseController;
use App\Models\Entity\Product as Product;
use App\Models\Entity\Special as Special;

class ProfitPercentController extends BaseController
{

    public function store()
    {
        $profit_percent = Input::get( 'profit_percent', null );
        $products_input = Input::get( 'products', null );
        $all_products_input = Input::get( 'all', null );
        $products = null;

        if ( $products_input !== null && is_array( $products_input ) ) {
            $products = Product::whereIn( 'entity_id', $products_input )->get();
        } elseif ( $all_products_input !== null && $all_products_input == 1 ) {
            $products = Product::all();
        }

        if ( $products !== null && $profit_percent !== null ) {
            foreach ( $products as $product ) {
                $special = Special::firstOrNew( [ 'entity_id' => $product->entity_id, 'entity_type_id' => $product->entity_type_id, 'type' => 'profit_percent' ] );
                $special->value = $profit_percent;
                $special->save();
            }
        }
    }

}
