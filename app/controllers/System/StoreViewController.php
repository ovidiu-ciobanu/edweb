<?php

namespace App\Controllers\System;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\Core\Website as CoreWebsite;
use App\Models\Core\Store\Group as CoreStoreGroup;
use App\Models\Core\Store as CoreStore;

class StoreViewController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $this->layout->content = View::make( 'system.store.index' );
    }

    public function create()
    {
        $store_groups = [ ];
        foreach ( CoreWebsite::where( 'website_id', '>', 0 )->get() as $core_website ) {
            $store_groups[ $core_website->name ] = $core_website->store_groups->lists( 'name', 'group_id' );
        }

        if ( count( $store_groups ) <= 0 ) {
            return Redirect::to( 'system/store_website/create' )->with( 'message', 'Website needs to be created first!' );
        }

        $this->layout->content = View::make( 'system.store_view.create' )->with( 'store_groups', $store_groups );
    }

    public function store()
    {
        $validator = Validator::make( Input::all(), CoreStore::rules() );

        if ( $validator->passes() ) {

            $store_group = CoreStoreGroup::find( Input::get( 'store' ) );

            $core_store = new CoreStore();
            $core_store->code = Input::get( 'code' );
            $core_store->website_id = $store_group->website_id;
            $core_store->group_id = $store_group->group_id;
            $core_store->name = Input::get( 'name' );
            $core_store->sort_order = (int) Input::get( 'sort_order' );
            $core_store->is_active = Input::get( 'status' );
            $core_store->save();

            return Redirect::to( 'system/store' )->with( 'message', 'Store view created!' );
        } else {
            return Redirect::back()->withErrors( $validator )->withInput();
        }
    }

    public function edit( $store_id )
    {
        $store = CoreStore::find( $store_id );

        if ( $store == null ) {
            return Redirect::to( 'system/store' );
        }

        $store_groups = [ ];
        foreach ( CoreWebsite::where( 'website_id', '>', 0 )->get() as $core_website ) {
            $store_groups[ $core_website->name ] = $core_website->store_groups->lists( 'name', 'group_id' );
        }

        if ( count( $store_groups ) <= 0 ) {
            return Redirect::to( 'system/store_website/create' )->with( 'message', 'Website needs to be created first!' );
        }

        $this->layout->content = View::make( 'system.store_view.edit' )->with( [ 'store_groups' => $store_groups, 'store' => $store ] );
    }

    public function update( $store_id )
    {
        $core_store = CoreStore::find( $store_id );

        if ( $core_store == null ) {
            return Redirect::to( 'system/store' );
        }

        $validator = Validator::make( Input::all(), CoreStore::rules( $store_id ) );

        if ( $validator->passes() ) {

            $store_group = CoreStoreGroup::find( Input::get( 'store' ) );

            $core_store->code = Input::get( 'code' );
            $core_store->website_id = $store_group->website_id;
            $core_store->group_id = $store_group->group_id;
            $core_store->name = Input::get( 'name' );
            $core_store->sort_order = (int) Input::get( 'sort_order' );
            $core_store->is_active = Input::get( 'status' );

            if ( count( $core_store->isDirty() ) > 0 ) {
                $core_store->save();
            }

            return Redirect::to( 'system/store' )->with( 'message', 'Store view <b>' . $core_store->name . '</b> updated!' );
        } else {
            return Redirect::back()->withErrors( $validator )->withInput();
        }
    }

}
