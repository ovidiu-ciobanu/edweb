<?php

namespace App\Controllers\System;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\Core\Website as CoreWebsite;

class StoreWebsiteController extends BaseController
{

    protected $layout = 'layouts.main';

    public function create()
    {
        $this->layout->content = View::make( 'system.store_website.create' );
    }

    public function store()
    {
        $validator = Validator::make( Input::all(), CoreWebsite::rules() );

        if ( $validator->passes() ) {

            $core_website = new CoreWebsite();
            $core_website->name = Input::get( 'name' );
            $core_website->code = Input::get( 'code' );
            $core_website->sort_order = (int) Input::get( 'sort_order' );
            $core_website->is_default = ( CoreWebsite::count() == 0 ) ? 1 : 0;
            $core_website->save();

            return Redirect::to( 'system/store' )->with( 'message', 'Website <b>' . $core_website->name . '</b> created!' );
        } else {
            return Redirect::back()->withErrors( $validator )->withInput();
        }
    }

    public function edit( $website_id )
    {
        $website = CoreWebsite::find( $website_id );

        if ( $website == null ) {
            return Redirect::to( 'system/store' );
        }

        $this->layout->content = View::make( 'system.store_website.edit' )->with( 'website', $website );
    }

    public function update( $website_id )
    {
        $core_website = CoreWebsite::find( $website_id );

        if ( $core_website == null ) {
            return Redirect::to( 'system/store' );
        }

        $validator = Validator::make( Input::all(), CoreWebsite::rules( $website_id ) );

        if ( $validator->passes() ) {

            $core_website->name = Input::get( 'name' );
            $core_website->code = Input::get( 'code' );
            $core_website->sort_order = (int) Input::get( 'sort_order' );
            $core_website->default_group_id = (int) Input::get( 'default_store' );
            $core_website->is_default = ( CoreWebsite::count() == 0 ) ? 1 : 0;
            if ( count( $core_website->isDirty() ) > 0 ) {
                $core_website->save();
            }

            return Redirect::to( 'system/store' )->with( 'message', 'Website <b>' . $core_website->name . '</b> updated!' );
        } else {
            return Redirect::back()->withErrors( $validator )->withInput();
        }
    }

}
