<?php

namespace App\Controllers\System\Import\Profile\Attribute\Set;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\Eav\Attribute\Set as EavAttributeSet;
use App\Models\System\Import\Profile\Attribute\Set\Group as ImportProfileAttributeSetGroup;
use App\Models\System\Import\Profile\Attribute\Set\Group\Sets as ImportProfileAttributeSetGroupSet;

class GroupController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $importProfileAttributeSetGroups = ImportProfileAttributeSetGroup::all();

        $this->layout->content = View::make( 'system.import.profile.attribute.set.group.index' )->with( [ 'import_profile_attribute_set_groups' => $importProfileAttributeSetGroups ] );
    }

    public function create()
    {
        $attribute_sets = EavAttributeSet::all();

        $this->layout->content = View::make( 'system.import.profile.attribute.set.group.create' )->with( 'attribute_sets', $attribute_sets );
    }

    public function store()
    {
        $importProfileAttributeSetGroup = new ImportProfileAttributeSetGroup();
        $importProfileAttributeSetGroup->name = Input::get( 'name' );
        $importProfileAttributeSetGroup->save();

        foreach ( Input::get( 'attribute_sets', [ ] ) as $attribute_set_id ) {
            $importProfileAttributeSetGroupSet = new ImportProfileAttributeSetGroupSet();
            $importProfileAttributeSetGroupSet->system_import_profile_attribute_set_group_id = $importProfileAttributeSetGroup->system_import_profile_attribute_set_group_id;
            $importProfileAttributeSetGroupSet->attribute_set_id = $attribute_set_id;
            $importProfileAttributeSetGroupSet->save();
        }

        return Redirect::to( 'system/import/profile/attribute/set/group' )->with( 'message', 'Import profile attrivute set <b>' . $importProfileAttributeSetGroup->name . '</b> created!' );
    }

    public function edit( $system_import_profile_attribute_set_group_id )
    {
        $import_profile_attribute_set_group = ImportProfileAttributeSetGroup::find( $system_import_profile_attribute_set_group_id );

        if ( $import_profile_attribute_set_group == null ) {
            return Redirect::to( 'system/import/profile/attribute/set/group' );
        }

        $attribute_sets = EavAttributeSet::all();

        $this->layout->content = View::make( 'system.import.profile.attribute.set.group.edit' )->with( [ 'attribute_sets' => $attribute_sets, 'import_profile_attribute_set_group' => $import_profile_attribute_set_group ] );
    }

    public function update( $system_import_profile_attribute_set_group_id )
    {
        $import_profile_attribute_set_group = ImportProfileAttributeSetGroup::find( $system_import_profile_attribute_set_group_id );

        if ( $import_profile_attribute_set_group == null ) {
            return Redirect::to( 'system/import/profile/attribute/set/group' );
        }

        $import_profile_attribute_set_group->name = Input::get( 'name' );
        $import_profile_attribute_set_group->save();

        $import_profile_attribute_set_group->sets()->delete();
        foreach ( Input::get( 'attribute_sets', [ ] ) as $attribute_set_id ) {
            $importProfileAttributeSetGroupSet = new ImportProfileAttributeSetGroupSet();
            $importProfileAttributeSetGroupSet->system_import_profile_attribute_set_group_id = $import_profile_attribute_set_group->system_import_profile_attribute_set_group_id;
            $importProfileAttributeSetGroupSet->attribute_set_id = $attribute_set_id;
            $importProfileAttributeSetGroupSet->save();
        }
        
        return Redirect::to( 'system/import/profile/attribute/set/group' )->with( 'message', 'Import profile attrivute set <b>' . $import_profile_attribute_set_group->name . '</b> updated!' );
    }

}
