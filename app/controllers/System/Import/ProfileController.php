<?php

namespace App\Controllers\System\Import;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\Eav\Attribute as EavAttribute;
use App\Models\Eav\Attribute\Set as EavAttributeSet;
use App\Models\System\Import\Profile as SystemImportProfile;
use App\Models\System\Import\Profile\Attribute as SystemImportProfileAttribute;
use App\Models\System\Import\Profile\Attribute\Set\Group as SystemImportProfileAttributeSetGroup;

class ProfileController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {

        $this->layout->content = View::make( 'system.import.profile.index' )->with( 'profiles', SystemImportProfile::all() );
    }

    public function edit( $profile_id )
    {
        $import_profile = SystemImportProfile::find( $profile_id );

        if ( $import_profile == null ) {
            return Redirect::to( 'system/import' );
        }

        $attribute_sets = [ ];
        if ( $import_profile->system_import_profile_attribute_set_group_id ) {
            foreach ( $import_profile->profile_attribute_set_group->sets->lists( 'attribute_set_id' ) as $attribute_set_id ) {
                $attribute_sets[] = EavAttributeSet::find( $attribute_set_id );
            }
        }

        $import_keys = $this->getImportKeys( $import_profile->imports->last()->path );

        $this->layout->content = View::make( 'system.import.profile.edit' )->with( [ 'import_profile' => $import_profile, 'profile_attribute_set_groups' => SystemImportProfileAttributeSetGroup::all()->lists( 'name', 'system_import_profile_attribute_set_group_id' ), 'attribute_sets' => $attribute_sets, 'import_keys' => $import_keys ] );
    }

    public function update( $profile_id )
    {
        $import_profile = SystemImportProfile::find( $profile_id );

        if ( $import_profile == null ) {
            return Redirect::to( 'system/import' );
        }

        // Change profile attribute set group and reset the attributes from profile
        if ( Input::has( 'profile_attribute_set_group' ) ) {

            // Delete old attributes
            if ( $import_profile->attribute_set_id ) {
                $import_profile->attributes()->delete();
            }

            $import_profile->system_import_profile_attribute_set_group_id = (int) Input::get( 'profile_attribute_set_group' );
            $import_profile->save();
            return Redirect::to( 'system/import/profile' );
        }

        $import_profile->name = Input::get( 'profile.name' );

        if ( count( $import_profile->isDirty() ) > 0 ) {
            $import_profile->save();
        }

        SystemImportProfileAttribute::where( 'profile_id', '=', $profile_id )->delete();
        foreach ( Input::get( 'profile.attribute', [ ] ) as $key => $attribute_id ) {
            if ( $attribute_id <= 0 ) {
                continue;
            }

            $default_value = Input::get( 'profile.default_value.' . $key );

            $profile_attribute = new SystemImportProfileAttribute();
            $profile_attribute->profile_id = $profile_id;
            $profile_attribute->attribute_id = $attribute_id;
            $profile_attribute->key = Input::get( 'profile.key.' . $key );
            $profile_attribute->default_value = $default_value;
            $profile_attribute->save();
        }

        return Redirect::to( 'system/import/profile' )->with( 'message', 'Profile updated!' );
    }

    public function destroy( $profile_id )
    {
        $import_profile = SystemImportProfile::find( $profile_id );

        if ( $import_profile == null ) {
            return Redirect::to( 'system/import/profile' )->with( 'message', 'No profile to delete.' );
        }

        if ( count( $import_profile->imports ) > 0 ) {
            return Redirect::to( 'system/import/profile' )->with( 'message', 'Profile used and can not delete.' );
        }

        $import_profile->delete();
        return Redirect::to( 'system/import/profile' )->with( 'message', 'Profile deleted with success.' );
    }

    /**
     * Get keys from multidimensional array.
     * 
     * @param string $path
     * @return array
     */
    private function getImportKeys( $path )
    {
        $contents = file_get_contents( storage_path( 'imports' ) . '/' . $path );
        $xml_obj = simplexml_load_string( $contents );

        if ( is_object( $xml_obj ) ) {
            return (array) $this->getKeys( $xml_obj );
        }
    }

    private function getKeys( $array )
    {
        foreach ( $array as $value ) {

            if ( count( $value ) <= 0 ) {
                return array_keys( (array) $array );
            }

            return $this->getKeys( $value );
        }
    }

}
