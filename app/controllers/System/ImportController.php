<?php

namespace App\Controllers\System;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\System\Cronjobb as Cronjobb; 
use App\Models\System\Import as SystemImport;
use App\Models\System\Import\Profile as SystemImportProfile;

class ImportController extends BaseController {

    protected $layout = 'layouts.main';

    public function index() {
        $active_import = Cronjobb::where('cron_type','=','import')
                ->where('status','=','started')
                ->orwhere('cron_type','=','import')
                ->where('status','=','in progress')
                ->first();
        if ($active_import) {
            $active_import->params = json_decode($active_import->params);
        }
        $this->layout->content = View::make('system.import.index', [ 'imports' => SystemImport::all(),'active_import' =>  $active_import]);
    }

    public function create() {
        $this->layout->content = View::make('system.import.create', [ 'profiles' => SystemImportProfile::all()->lists('name', 'import_profile_id')]);
    }

    public function store() {
        if (Input::hasFile('import')) {
            $import = new SystemImport();
            $import_profile = ( Input::get('profile') == 0 ) ? new SystemImportProfile() : SystemImportProfile::find(Input::get('profile'));
            if (null == $import_profile) {
                return Redirect::to('system/import');
            }

            $file = Input::file('import');
            $filename = time() . '_' . $file->getClientOriginalName();

            $file->move(storage_path('imports'), $filename);

            $import->path = $filename;

            if (Input::has('clone_profile') && Input::get('clone_profile') == 'clone') {
                $clone_import_profile = $import_profile->replicate();
                $clone_import_profile->save();
                foreach ($import_profile->attributes as $attribute) {
                    $new_attribute = $attribute->replicate();
                    $new_attribute->profile_id = $clone_import_profile->import_profile_id;
                    $new_attribute->save();
                }
                $import_profile = $clone_import_profile;
            } else {
                if (null == $import_profile->name) {
                    $import_profile->name = $filename;
                }
                if (count($import_profile->isDirty()) > 0) {
                    $import_profile->save();
                }
            }

            $import->import_profile_id = $import_profile->import_profile_id;
            $import->save();

            return Redirect::route('system.import.profile.edit', [ 'id' => $import_profile->import_profile_id]);
        }

        return Redirect::to('system/import/create')->with('message', 'Please select a file.');
    }

    public function addImportToCron($import_id) {

        $active_import = Cronjobb::where('cron_type','=','import')
                ->where('status','=','started')
                ->orwhere('cron_type','=','import')
                ->where('status','=','in progress')
                ->first();
        if ($active_import){
            echo "Error! Import Job active!";
            die();
        }

        if ($import_id) {
            $cronjob = new Cronjobb();
            $cronjob->params = json_encode(array('import_id' => $import_id));
            $cronjob->status = 'started';
            $cronjob->cron_type = 'import';
            $cronjob->save();
        }
        
        return Redirect::to('system/import/');
    }
    
    public function getImportStep(){
        $active_import = Cronjobb::where('cron_type','=','import')
                ->where('status','=','started')
                ->orwhere('cron_type','=','import')
                ->where('status','=','in progress')
                ->first();
        return json_encode($active_import);
    }

    public function destroy($import_id) {
        $import = SystemImport::find($import_id);

        if ($import == null) {
            return Redirect::to('system/import')->with('message', 'No import to delete.');
        }

        unlink(storage_path('imports') . '/' . $import->path);

        $import->delete();
        return Redirect::to('system/import')->with('message', 'Import deleted with success.');
    }

}
