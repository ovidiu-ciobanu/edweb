<?php

namespace App\Controllers\System;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\User as User;
use Illuminate\Support\Facades\Hash as Hash;

class UserController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $users = User::paginate( 30 );

        $this->layout->content = View::make( 'system.user.index' )->with( 'users', $users );
    }

    public function create()
    {
        $this->layout->content = View::make( 'system.user.create' );
    }

    public function store()
    {
        $validator = Validator::make( Input::all(), User::rules() );

        if ( $validator->passes() ) {

            $user = new User();
            $user->firstname = Input::get( 'firstname' );
            $user->lastname = Input::get( 'lastname' );
            $user->email = Input::get( 'email' );
            $user->save();

            return Redirect::to( 'system/user' )->with( 'message', 'User <b>' . $user->firstname . ' ' . $user->lastname . '</b> created!' );
        } else {
            return Redirect::to( 'system/user/create' )->withErrors( $validator )->withInput();
        }
    }

    public function edit( $user_id )
    {
        $user = User::find( $user_id );

        if ( null == $user ) {
            return Redirect::to( 'system/user' );
        }

        $this->layout->content = View::make( 'system.user.edit' )->with( [ 'user' => $user ] );
    }

    public function update( $user_id )
    {
        $user = User::find( $user_id );

        if ( null == $user ) {
            return Redirect::to( 'system/user' );
        }

        $validation_rules = User::rules( $user_id );
        $validation_rules['password'] = 'sometimes|' . $validation_rules['password'];
        $validation_rules['password_confirmation'] = 'sometimes|' . $validation_rules['password_confirmation'];

        $inputs = Input::all();
        if ( Input::get( 'password' ) == '' ) {
            $inputs = Input::except( 'password', 'password_confirmation' );
        }
        $validator = Validator::make( $inputs, $validation_rules );

        if ( $validator->passes() ) {

            $user->firstname = Input::get( 'firstname' );
            $user->lastname = Input::get( 'lastname' );
            $user->email = Input::get( 'email' );
            if ( Input::get( 'password' ) != '' ) {
                $user->password = Hash::make( Input::get( 'password' ) );
            }

            if ( count( $user->isDirty() ) > 0 ) {
                $user->save();
            }

            return Redirect::to( 'system/user' )->with( 'message', 'User <b>' . $user->firstname . ' ' . $user->lastname . '</b> updated!' );
        } else {
            return Redirect::back()->withErrors( $validator )->withInput();
        }
    }

    public function destroy( $user_id )
    {
        $user = User::find( $user_id );

        if ( null == $user ) {
            return Redirect::to( 'system/user' )->with( 'message', 'No user to delete.' );
        }

        $user->delete();
        return Redirect::to( 'system/user' )->with( 'message', 'User deleted with success.' );
    }

}
