<?php

namespace App\Controllers\System;

use \View;
use \Validator;
use \Input;
use \Redirect;
use App\Controllers\BaseController;
use App\Models\Core\Website as CoreWebsite;
use App\Models\Core\Store\Group as CoreStoreGroup;

class StoreController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $core_websites = CoreWebsite::where( 'website_id', '>', 0 )->get();

        $this->layout->content = View::make( 'system.store.index' )->with( 'core_websites', $core_websites );
    }

    public function create()
    {
        $websites = CoreWebsite::where( 'website_id', '>', 0 )->lists( 'name', 'website_id' );

        if ( count( $websites ) <= 0 ) {
            return Redirect::to( 'system/store_website/create' )->with( 'message', 'Website needs to be created first!' );
        }

        $this->layout->content = View::make( 'system.store.create' )->with( 'websites', $websites );
    }

    public function store()
    {
        $validator = Validator::make( Input::all(), CoreStoreGroup::rules() );

        if ( $validator->passes() ) {

            $core_store_group = new CoreStoreGroup();
            $core_store_group->website_id = Input::get( 'website' );
            $core_store_group->name = Input::get( 'name' );
            $core_store_group->save();

            return Redirect::to( 'system/store' )->with( 'message', 'Store <b>' . $core_store_group->name . '</b> created!' );
        } else {
            return Redirect::to( 'system/store/create' )->withErrors( $validator )->withInput();
        }
    }

    public function edit( $store_group_id )
    {
        $store_group = CoreStoreGroup::find( $store_group_id );

        if ( $store_group == null ) {
            return Redirect::to( 'system/store' );
        }

        $websites = CoreWebsite::where( 'website_id', '>', 0 )->lists( 'name', 'website_id' );

        if ( count( $websites ) <= 0 ) {
            return Redirect::to( 'system/store_website/create' )->with( 'message', 'Website needs to be created first!' );
        }

        $this->layout->content = View::make( 'system.store.edit' )->with( [ 'websites' => $websites, 'store_group' => $store_group ] );
    }

    public function update( $store_group_id )
    {
        $core_store_group = CoreStoreGroup::find( $store_group_id );

        if ( $core_store_group == null ) {
            return Redirect::to( 'system/store' );
        }

        $validator = Validator::make( Input::all(), CoreStoreGroup::rules() );

        if ( $validator->passes() ) {

            $core_store_group->website_id = Input::get( 'website' );
            $core_store_group->name = Input::get( 'name' );
            //$core_store_group->root_category_id = '';
            $core_store_group->default_store_id = Input::get( 'default_store' );

            if ( count( $core_store_group->isDirty() ) > 0 ) {
                $core_store_group->save();
            }

            return Redirect::to( 'system/store' )->with( 'message', 'Store <b>' . $core_store_group->name . '</b> updated!' );
        } else {
            return Redirect::back()->withErrors( $validator )->withInput();
        }
    }

}
