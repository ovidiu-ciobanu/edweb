<?php

namespace App\Controllers;

use \View;
use App\Controllers\BaseController;

class LogController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        
        $this->layout->content = View::make( 'system.log.index' );
    }
    
    public function show()
    {
        
    }
    
}
