<?php

namespace App\Controllers;

use \View;
use App\Controllers\BaseController;

class DashboardController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $this->layout->content = View::make( 'dashboard.index' );
    }

}
