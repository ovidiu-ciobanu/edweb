<?php

namespace App\Controllers;

use \Controller;
use \View;
use \Session;

class BaseController extends Controller
{

    public function __construct()
    {
        $this->beforeFilter( 'csrf', array( 'on' => array( 'post', 'put', 'patch', 'delete' ) ) );

        if ( !Session::has( 'store_id' ) ) {
            Session::put( 'store_id', 0 );
        }
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( !is_null( $this->layout ) ) {
            $this->layout = View::make( $this->layout );
        }
    }

}
