<?php namespace App\Controllers\Sales;

use \Input;
use \View;
use App\Controllers\BaseController;
use App\Models\Sales\Order;

class OrderController extends BaseController
{

    protected $layout = 'layouts.main';

    public function index()
    {
        $orders = Order::all();

        $this->layout->content = View::make('sales.order.index')->with([ 'orders' => $orders]);
    }

    public function show($order_id)
    {
        $order = Order::find($order_id);

        $this->layout->content = View::make('sales.order.show')->with([ 'order' => $order]);
    }

}
