<?php

function currency_convert_price( $price, $currency_code = 'EUR' )
{
    $_currencies = [
        'EUR' => [
            'symbol' => '&euro;',
            'position' => 'left'
        ],
        'USD' => [
            'symbol' => '$',
            'position' => 'left'
        ]
    ];

    if ( !isset( $_currencies[$currency_code] ) ) {
        throw new Exception( 'currency_convert: Currency code ' . $currency_code . ' not available!' );
    }

    $currency = $_currencies[$currency_code];
    $price = number_format( $price, 2 );

    $return = '';
    if ( $currency['position'] == 'left' ) {
        $return .= $currency['symbol'] . ' ' . $price;
    } else {
        $return .= $price . $currency['symbol'];
    }

    return $return;
}
