<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add( new App\Commands\Import\Products() );
Artisan::add( new App\Commands\Export\Products\Magento() );
Artisan::add( new App\Commands\Export\Products\Magento\Media() );

Artisan::add( new App\Commands\Export\Magento\Categories() );

Artisan::add( new App\Commands\Magento\Orders\Import() );
