<?php namespace App\Models\Entity;

use \Eloquent;

class Export extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'entity_export';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'entity_export_id';
    
}
