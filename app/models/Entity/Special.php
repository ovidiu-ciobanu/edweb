<?php

namespace App\Models\Entity;

use \Eloquent;

class Special extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'entity_specials';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'entity_special_id';
    public $timestamps = false;
    protected $fillable = [ 'entity_id', 'entity_type_id', 'type', 'value' ];

}
