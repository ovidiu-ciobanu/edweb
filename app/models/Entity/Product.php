<?php

namespace App\Models\Entity;

use \Eloquent;
use DB;

class Product extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'product_entity';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'entity_id';

    public function attributeSet()
    {
        return $this->hasOne( 'App\Models\Eav\Attribute\Set', 'attribute_set_id', 'attribute_set_id' );
    }

    public function categories()
    {
        return $this->belongsToMany( 'App\Models\Entity\Category' );
    }

    public function entityExport()
    {
        return $this->hasOne( 'App\Models\Entity\Export', 'entity_id', 'entity_id' )->where( 'entity_type_id', '=', $this->entity_type_id );
    }

    public function special()
    {
        return $this->hasOne( 'App\Models\Entity\Special', 'entity_id', 'entity_id' )->where( 'entity_type_id', '=', $this->entity_type_id );
    }

    public static function delete_product($product_id) {
        
        DB::table('product_entity')     ->where('entity_id','=',$product_id)->delete();
        DB::table('eav_entity_datetime')->where('entity_id','=',$product_id)->where('entity_type_id','=','1')->delete();
        DB::table('eav_entity_decimal') ->where('entity_id','=',$product_id)->where('entity_type_id','=','1')->delete();
        DB::table('eav_entity_int')     ->where('entity_id','=',$product_id)->where('entity_type_id','=','1')->delete();
        DB::table('eav_entity_text')    ->where('entity_id','=',$product_id)->where('entity_type_id','=','1')->delete();
        DB::table('eav_entity_varchar') ->where('entity_id','=',$product_id)->where('entity_type_id','=','1')->delete();
        DB::table('entity_export')      ->where('entity_id','=',$product_id)->where('entity_type_id','=','1')->delete();
        
        return true;
    }
    /**
     * Get attributes values.
     * 
     * @param string $name
     * @param array $arguments
     */
    public function __call( $name, $arguments )
    {

        if ( 'attribute' == substr( $name, 0, 9 ) ) {
            $attribute_code = snake_case( substr( $name, 9 ) );

            $attribute = \App\Models\Eav\Attribute::where( 'attribute_code', $attribute_code )->where( 'entity_type_id', $this->entity_type_id )->first();

            if ( !$attribute ) {
                throw new \Exception( 'Attribute with code not found: ' . $attribute_code );
            }

            return $attribute->entity( $this->entity_id )->first();
        }

        return parent::__call( $name, $arguments );
    }

}
