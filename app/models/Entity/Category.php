<?php

namespace App\Models\Entity;

use \Eloquent;
use DB;

class Category extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'category_entity';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'entity_id';

    public function attributeSet()
    {
        return $this->hasOne( 'App\Models\Eav\Attribute\Set', 'attribute_set_id', 'attribute_set_id' );
    }

    public function entityExport()
    {
        return $this->hasOne( 'App\Models\Entity\Export', 'entity_id', 'entity_id' )->where( 'entity_type_id', '=', $this->entity_type_id );
    }

    public function products()
    {
        return $this->belongsToMany( 'App\Models\Entity\Product' );
    }
    
    public static function delete_category($cat_id) {
        
        DB::table('category_entity')->where('entity_id','=',$cat_id)->delete();
        DB::table('eav_entity_datetime')->where('entity_id','=',$cat_id)->where('entity_type_id','=','2')->delete();
        DB::table('eav_entity_decimal')->where('entity_id','=',$cat_id)->where('entity_type_id','=','2')->delete();
        DB::table('eav_entity_int')->where('entity_id','=',$cat_id)->where('entity_type_id','=','2')->delete();
        DB::table('eav_entity_text')->where('entity_id','=',$cat_id)->where('entity_type_id','=','2')->delete();
        DB::table('eav_entity_varchar')->where('entity_type_id','=','2')->where('entity_id','=',$cat_id)->delete();
        DB::table('entity_export')->where('entity_id','=',$cat_id)->where('entity_type_id','=','2')->delete();
        
        return true;
    }
    
    public static function getCategoriesIn($category_ids){
        return Category::select('entity_export.export_id')
                ->leftJoin('entity_export as cat_id',function($join){
                    $join->on('category_entity.entity_id','=','entity_export.entity_id')
                        ->on('category_entity.entity_type_id','=','entity_export.entity_type_id');
                })->whereIn('category_entity.entity_id',$category_ids)->get();
    }

    public function special()
    {
        return $this->hasOne( 'App\Models\Entity\Special', 'entity_id', 'entity_id' )->where( 'entity_type_id', '=', $this->entity_type_id );
    }

    /**
     * Get attributes values.
     * 
     * @param string $name
     * @param array $arguments
     */
    public function __call( $name, $arguments )
    {

        if ( 'attribute' == substr( $name, 0, 9 ) ) {
            
            $attribute_code = snake_case( substr( $name, 9 ) );
            $attribute = \App\Models\Eav\Attribute::where( 'attribute_code', $attribute_code )->where( 'entity_type_id', $this->entity_type_id )->first();

            if ( !$attribute ) {
                throw new \Exception( 'Attribute with code not found: ' . $attribute_code );
            }

            return $attribute->entity( $this->entity_id )->first();
        }

        return parent::__call( $name, $arguments );
    }

}
