<?php

namespace App\Models;

use \Eloquent;
use \Session;

class Log extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'log_id';

    public static function info( $message, $store_id = null, $entity_id = null, $entity_type_id = null )
    {
        static::_message( $message, $store_id, $entity_id, $entity_type_id );
    }

    private static function _message( $message, $store_id = null, $entity_id = null, $entity_type_id = null )
    {
        $instance = new static;

        if ( null === $store_id ) {
            $store_id = Session::get( 'store_id' );
        }

        $instance->store_id = $store_id;
        $instance->entity_id = $entity_id;
        $instance->entity_type_id = $entity_type_id;
        $instance->data = $message;

        $instance->save();
    }

}
