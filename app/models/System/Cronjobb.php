<?php
namespace App\Models\System;

use \Eloquent;
use DB;

class Cronjobb extends Eloquent {
    protected $table = 'cron_jobs';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'cron_job_id';
    public $timestamps = true;
    
    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
            'status' => 'required',
            'total' => 'required' ,
            'cron_type' => 'required',
            'step' => 'required'
        ], $merge );
    }
    
//    public static function getActiveImport() {
//        $cronjob =  Cronjobb::where('cron_type','=','import')
//                ->where('status','=','started')
//                ->orwhere('cron_type','=','import')
//                ->where('status','=','working')
//                
//                ->firstOrFail();
//        if ($cronjob) {
//            $cronjob->params = json_decode($cronjob->params);
//            return $cronjob;
//        }
//        return false;
//    }
}