<?php

namespace App\Models\System\Import;

use \Eloquent;

class Profile extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'system_import_profile';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'import_profile_id';

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }

    public function imports()
    {
        return $this->hasMany( 'App\Models\System\Import', 'import_profile_id' );
    }

    public function attributes()
    {
        return $this->hasMany( 'App\Models\System\Import\Profile\Attribute', 'profile_id' );
    }

    public function profileAttributeSetGroup()
    {
        return $this->hasOne( 'App\Models\System\Import\Profile\Attribute\Set\Group', 'system_import_profile_attribute_set_group_id', 'system_import_profile_attribute_set_group_id' );
    }

}
