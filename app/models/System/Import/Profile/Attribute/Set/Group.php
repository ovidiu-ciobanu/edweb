<?php

namespace App\Models\System\Import\Profile\Attribute\Set;

use \Eloquent;

class Group extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'system_import_profile_attribute_set_group';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'system_import_profile_attribute_set_group_id';
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }

    public function sets()
    {
        return $this->hasMany( 'App\Models\System\Import\Profile\Attribute\Set\Group\Sets', 'system_import_profile_attribute_set_group_id' );
    }

}
