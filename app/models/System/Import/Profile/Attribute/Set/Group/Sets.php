<?php

namespace App\Models\System\Import\Profile\Attribute\Set\Group;

use \Eloquent;

class Sets extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'system_import_profile_attribute_set_group_sets';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }

    public function attributeSet()
    {
        return $this->hasOne( 'App\Models\Eav\Attribute\Set', 'attribute_set_id', 'attribute_set_id' );
    }

}
