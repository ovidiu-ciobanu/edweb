<?php

namespace App\Models\System;

use \Eloquent;

class Import extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'system_import';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'import_id';
    
    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }
    
    public function profile()
    {
        return $this->belongsTo( 'App\Models\System\Import\Profile', 'import_profile_id' );
    }
    
}
