<?php

namespace App\Models\Eav\Attribute;

use \Eloquent;

class Option extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'eav_attribute_option';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'option_id';
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }
    
    public function values()
    {
        return $this->hasMany( 'App\Models\Eav\Attribute\Option\Value', 'option_id' );
    }

}
