<?php namespace App\Models\Eav\Attribute\Input;

use \Eloquent;

class Type extends Eloquent
{

    const TEXT = 'text';
    const DECIMAL = 'decimal';
    const TEXTAREA = 'textarea';
    const BOOLEAN = 'boolean';
    const SELECT = 'select';
    const MULTISELECT = 'multiselect';
    const DATETIME = 'datetime';

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'eav_attribute_input_type';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'input_type_id';
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }

}
