<?php

namespace App\Models\Eav\Attribute;

use \Eloquent;

class Set extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'eav_attribute_set';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'attribute_set_id';
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }
    
    public function groups()
    {
        return $this->hasMany( 'App\Models\Eav\Attribute\Group', 'attribute_set_id' );
    }
    
    public function groupsAttributes()
    {
        return $this->hasMany( 'App\Models\Eav\Attribute\Group\Attribute', 'attribute_set_id' );
    }
    
    public function entityType()
    {
        return $this->hasOne( 'App\Models\Eav\Entity\Type', 'entity_type_id', 'entity_type_id' );
    }
    
}
