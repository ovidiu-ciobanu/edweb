<?php

namespace App\Models\Eav\Entity;

use \Eloquent;

class Text extends Eloquent
{
    protected $fillable = array('entity_type_id', 'attribute_id', 'store_id','entity_id');
    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'eav_entity_text';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'value_id';
    public $timestamps = false;
    
    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
                /* 'store' => 'required|numeric|exists:core_store_groups,group_id',
                  'name' => 'required',
                  'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
                  'status' => 'required|boolean',
                  'sort_order' => 'sometimes|numeric' */
                ], $merge );
    }
    
}
