<?php

namespace App\Models\Eav;

use \Eloquent;
use \Session;

class Attribute extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'eav_attribute';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'attribute_id';
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
            'input_type' => 'sometimes|required|alpha',
            'code' => 'sometimes|required|unique:eav_attribute,attribute_code' . ( $id ? ',' . $id . ',attribute_id' : '' ),
            'required' => 'required|boolean',
            'default_value_boolean' => 'sometimes|boolean',
            'default_value_text' => 'sometimes',
            'default_value_textarea' => 'sometimes',
                ], $merge );
    }

    public function label()
    {
        return $this->hasOne( 'App\Models\Eav\Attribute\Label', 'attribute_id' )->where( 'store_id', '=', Session::get( 'store_id' ) );
    }

    public function labels()
    {
        return $this->hasMany( 'App\Models\Eav\Attribute\Label', 'attribute_id' );
    }

    public function options()
    {
        return $this->hasMany( 'App\Models\Eav\Attribute\Option', 'attribute_id' );
    }

    public function inputType()
    {
        return $this->hasOne( '\App\Models\Eav\Attribute\Input\Type', 'input_type_code', 'frontend_input' );
    }

    public function entityType()
    {
        return $this->hasOne( 'App\Models\Eav\Entity\Type', 'entity_type_id', 'entity_type_id' );
    }

    public function entity( $entity_id )
    {
        return $this->select(
                        [
                            \DB::raw( 'COALESCE( eav_entity_varchar.value_id, eav_entity_text.value_id, eav_entity_int.value_id, eav_entity_decimal.value_id, eav_entity_datetime.value_id ) AS `value_id`' ),
                            \DB::raw( 'COALESCE( eav_entity_varchar.entity_type_id, eav_entity_text.entity_type_id, eav_entity_int.entity_type_id, eav_entity_decimal.entity_type_id, eav_entity_datetime.entity_type_id ) AS `entity_type_id`' ),
                            \DB::raw( 'COALESCE( eav_entity_varchar.attribute_id, eav_entity_text.attribute_id, eav_entity_int.attribute_id, eav_entity_decimal.attribute_id, eav_entity_datetime.attribute_id ) AS `attribute_id`' ),
                            \DB::raw( 'COALESCE( eav_entity_varchar.store_id, eav_entity_text.store_id, eav_entity_int.store_id, eav_entity_decimal.store_id, eav_entity_datetime.store_id ) AS `store_id`' ),
                            \DB::raw( 'COALESCE( eav_entity_varchar.entity_id, eav_entity_text.entity_id, eav_entity_int.entity_id, eav_entity_decimal.entity_id, eav_entity_datetime.entity_id ) AS `entity_id`' ),
                            \DB::raw( 'COALESCE( eav_entity_varchar.value, eav_entity_text.value, eav_entity_int.value, eav_entity_decimal.value, eav_entity_datetime.value ) AS `value`' ),
                        ]
                )->leftJoin( 'eav_entity_varchar', function( $join ) use ( $entity_id ) {
                    $join->on( 'eav_entity_varchar.entity_type_id', '=', 'eav_attribute.entity_type_id' )->on( 'eav_entity_varchar.attribute_id', '=', 'eav_attribute.attribute_id' )->where( 'eav_entity_varchar.store_id', '=', Session::get( 'store_id' ) )->where( 'eav_entity_varchar.entity_id', '=', $entity_id );
                } )->leftJoin( 'eav_entity_text', function( $join ) use ( $entity_id ) {
                    $join->on( 'eav_entity_text.entity_type_id', '=', 'eav_attribute.entity_type_id' )->on( 'eav_entity_text.attribute_id', '=', 'eav_attribute.attribute_id' )->where( 'eav_entity_text.store_id', '=', Session::get( 'store_id' ) )->where( 'eav_entity_text.entity_id', '=', $entity_id );
                } )->leftJoin( 'eav_entity_int', function( $join ) use ( $entity_id ) {
                    $join->on( 'eav_entity_int.entity_type_id', '=', 'eav_attribute.entity_type_id' )->on( 'eav_entity_int.attribute_id', '=', 'eav_attribute.attribute_id' )->where( 'eav_entity_int.store_id', '=', Session::get( 'store_id' ) )->where( 'eav_entity_int.entity_id', '=', $entity_id );
                } )->leftJoin( 'eav_entity_decimal', function( $join ) use ( $entity_id ) {
                    $join->on( 'eav_entity_decimal.entity_type_id', '=', 'eav_attribute.entity_type_id' )->on( 'eav_entity_decimal.attribute_id', '=', 'eav_attribute.attribute_id' )->where( 'eav_entity_decimal.store_id', '=', Session::get( 'store_id' ) )->where( 'eav_entity_decimal.entity_id', '=', $entity_id );
                } )->leftJoin( 'eav_entity_datetime', function( $join ) use ( $entity_id ) {
                    $join->on( 'eav_entity_datetime.entity_type_id', '=', 'eav_attribute.entity_type_id' )->on( 'eav_entity_datetime.attribute_id', '=', 'eav_attribute.attribute_id' )->where( 'eav_entity_datetime.store_id', '=', Session::get( 'store_id' ) )->where( 'eav_entity_datetime.entity_id', '=', $entity_id );
                } )->where( 'eav_attribute.attribute_id', '=', $this->attribute_id );
    }

}
