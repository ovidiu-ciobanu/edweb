<?php
namespace App\Models\Core;

use \Eloquent;

class Store extends Eloquent
{
    
    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'core_stores';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'store_id';
    public $timestamps = false;
	
	public static function rules( $id = 0, $merge = [] )
	{
		return array_merge( [
			'store' => 'required|numeric|exists:core_store_groups,group_id',
			'name' => 'required',
			'code' => 'required|alpha|unique:core_stores,code' . ( $id ? ',' . $id . ',store_id' : '' ),
			'status' => 'required|boolean',
			'sort_order' => 'sometimes|numeric'
		], $merge);
	}
	
}