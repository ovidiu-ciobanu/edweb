<?php

namespace App\Models\Core\Store;

use \Eloquent;

class Group extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'core_store_groups';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'group_id';
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
            'website' => 'required|numeric',
            'name' => 'required'
                ], $merge );
    }

    public function storeViews()
    {
        return $this->hasMany( 'App\Models\Core\Store', 'group_id' );
    }

    public function website()
    {
        return $this->belongsTo( 'App\Models\Core\Website' );
    }

}
