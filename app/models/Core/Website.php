<?php

namespace App\Models\Core;

use \Eloquent;

class Website extends Eloquent
{

    /**
     * The database table used by the model.
     * 
     * @var string 
     */
    protected $table = 'core_websites';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'website_id';
    public $timestamps = false;

    public static function rules( $id = 0, $merge = [ ] )
    {
        return array_merge( [
            'name' => 'required',
            'code' => 'required|alpha|unique:core_websites,code' . ( $id ? ',' . $id . ',website_id' : '' ),
            'sort_order' => 'sometimes|numeric',
            'default_store' => 'sometimes|exists:core_store_groups,group_id'
                ], $merge );
    }

    public function storeGroups()
    {
        return $this->hasMany( 'App\Models\Core\Store\Group', 'website_id' );
    }

}
