<?php

namespace App\Models\Sales;

use \Eloquent;

class Order extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'order_id';

    public function items()
    {
        return $this->hasMany( 'App\Models\Sales\Order\Item' );
    }

    public function shipping()
    {
        return $this->hasOne( 'App\Models\Sales\Order\Shipping' );
    }

    public function billing()
    {
        return $this->hasOne( 'App\Models\Sales\Order\Billing' );
    }

}
