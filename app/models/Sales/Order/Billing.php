<?php

namespace App\Models\Sales\Order;

use \Eloquent;

class Billing extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_billing';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

}
