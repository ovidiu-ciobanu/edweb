<?php

namespace App\Models\Sales\Order;

use \Eloquent;

class Item extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'item_id';

}
