<?php

namespace App\Models\Sales\Order;

use \Eloquent;

class Shipping extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_shipping';

    /**
     * The database table primary key used by the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

}
