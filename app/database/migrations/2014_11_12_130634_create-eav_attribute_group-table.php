<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'eav_attribute_group', function( $table ) {
            
            $table->increments( 'attribute_group_id' );
            $table->integer( 'attribute_set_id' )->unsigned();
            $table->string( 'attribute_group_name' );
            $table->integer( 'sort_order' )->default( 0 );
            
            // Delete attributes from this table if the attribute sets are deleted.
            $table->foreign( 'attribute_set_id' )->references( 'attribute_set_id' )->on( 'eav_attribute_set' )->onDelete( 'cascade' );
            
        } );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'eav_attribute_group' );
	}

}
