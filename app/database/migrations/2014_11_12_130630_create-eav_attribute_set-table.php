<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeSetTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'eav_attribute_set', function( $table ) {

            $table->increments( 'attribute_set_id' );
            $table->integer( 'entity_type_id' )->unsigned();
            $table->string( 'attribute_set_name' );
            $table->integer( 'sort_order' )->default( 0 );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'eav_attribute_set' );
    }

}
