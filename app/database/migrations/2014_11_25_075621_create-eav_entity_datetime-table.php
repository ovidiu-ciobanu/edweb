<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavEntityDatetimeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'eav_entity_datetime', function( $table ) {

            $table->increments( 'value_id' );
            $table->integer( 'entity_type_id' )->unsigned();
            $table->integer( 'attribute_id' )->unsigned();
            $table->integer( 'store_id' )->unsigned();
            $table->integer( 'entity_id' )->unsigned();
            $table->dateTime( 'value' )->nullable();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'eav_entity_datetime' );
    }

}
