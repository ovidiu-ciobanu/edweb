<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityExportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create( 'entity_export', function( $table ) {
            
            $table->increments( 'entity_export_id' );
            $table->integer( 'entity_id' )->unsigned();
            $table->integer( 'entity_type_id' )->unsigned();
            $table->integer( 'export_id' );
            $table->timestamps();
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'entity_export' );
	}

}
