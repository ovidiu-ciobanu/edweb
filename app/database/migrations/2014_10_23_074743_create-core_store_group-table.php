<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreStoreGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'core_store_groups', function( $table ) {
			$table->smallInteger( 'group_id', true );
			$table->smallInteger( 'website_id' );
			$table->string( 'name', 64 );
			$table->unsignedInteger( 'root_category_id' )->nullable();
			$table->smallInteger( 'default_store_id' )->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'core_store_groups' );
	}

}
