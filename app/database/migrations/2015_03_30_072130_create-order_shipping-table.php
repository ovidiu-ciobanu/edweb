<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderShippingTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'order_shipping', function( $table ) {

            $table->increments( 'id' );
            $table->integer( 'order_id' );
            $table->string( 'firstname' );
            $table->string( 'lastname' );
            $table->string( 'street' );
            $table->string( 'city' );
            $table->string( 'region' );
            $table->integer( 'postcode' );
            $table->string( 'telephone' );
            $table->string( 'country_id' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'order_shipping' );
    }

}
