<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'order_items', function( $table ) {

            $table->increments( 'item_id' );
            $table->integer( 'order_id' );
            $table->integer( 'quote_item_id' );

            $table->timestamps();

            $table->integer( 'product_id' );
            $table->string( 'product_type' );

            $table->string( 'sku' );
            $table->string( 'name' );

            $table->decimal( 'qty_ordered', 12, 4 );
            
            $table->decimal( 'price', 12, 4 );
            $table->decimal( 'base_price', 12, 4 );
            $table->decimal( 'original_price', 12, 4 );
            $table->decimal( 'base_original_price', 12, 4 );
            $table->decimal( 'tax_percent', 12, 4 );
            $table->decimal( 'tax_amount', 12, 4 );
            $table->decimal( 'base_tax_amount', 12, 4 );
            
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'order_items' );
    }

}
