<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemImportProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create( 'system_import_profile', function( $table ) {
            
            $table->increments( 'import_profile_id' );
            $table->integer( 'attribute_set_id' )->nullable()->unsigned();
            $table->string( 'name' )->nullable();
            
            $table->timestamps();
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'system_import_profile' );
	}

}
