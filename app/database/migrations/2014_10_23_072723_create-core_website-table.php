<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreWebsiteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'core_websites', function( $table ) {
			$table->smallInteger( 'website_id', true );
			$table->string( 'code', 32 )->unique();
			$table->string( 'name', 64 );
			$table->smallInteger( 'sort_order' )->default( 0 );
			$table->smallInteger( 'default_group_id' )->nullable();
			$table->tinyInteger( 'is_default' )->default( 0 );
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'core_websites' );
	}

}
