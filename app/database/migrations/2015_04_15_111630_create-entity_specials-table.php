<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitySpecialsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'entity_specials', function( Blueprint $table ) {

            $table->increments( 'entity_special_id' );
            $table->integer( 'entity_id' )->unsigned();
            $table->integer( 'entity_type_id' )->unsigned();
            $table->string( 'type' );
            $table->string( 'value' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'entity_specials' );
    }

}
