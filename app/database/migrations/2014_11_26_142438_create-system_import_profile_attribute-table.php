<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemImportProfileAttributeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create( 'system_import_profile_attribute', function( $table ) {
            
            $table->increments( 'profile_attribute_id' );
            $table->integer( 'profile_id' )->unsigned();
            $table->integer( 'attribute_id' );
            $table->string( 'key' );
            $table->string( 'default_value' )->nullable();
            
            $table->foreign( 'profile_id' )->references( 'import_profile_id' )->on( 'system_import_profile' )->onDelete( 'cascade' );
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'system_import_profile_attribute' );
	}

}
