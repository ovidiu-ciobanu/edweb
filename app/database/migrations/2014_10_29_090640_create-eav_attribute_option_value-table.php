<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeOptionValueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'eav_attribute_option_value', function( $table ) {

            $table->increments( 'value_id' );
            $table->integer( 'option_id' )->unsigned();
            $table->integer( 'store_id' )->default( 0 );
            $table->string( 'value' )->nullable();
            
            $table->foreign( 'option_id' )->references( 'option_id' )->on( 'eav_attribute_option' )->onDelete( 'cascade' );
            
        } );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'eav_attribute_option_value' );
	}

}
