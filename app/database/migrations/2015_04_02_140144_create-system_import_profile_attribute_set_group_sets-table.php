<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemImportProfileAttributeSetGroupSetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'system_import_profile_attribute_set_group_sets', function($table) {
            $table->integer( 'system_import_profile_attribute_set_group_id' )->unsigned();
            $table->integer( 'attribute_set_id' )->unsigned();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'system_import_profile_attribute_set_group_sets' );
    }

}
