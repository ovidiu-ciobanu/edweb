<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavEntityTextTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'eav_entity_text', function( $table ) {

            $table->increments( 'value_id' );
            $table->integer( 'entity_type_id' )->unsigned();
            $table->integer( 'attribute_id' )->unsigned();
            $table->integer( 'store_id' )->unsigned();
            $table->integer( 'entity_id' )->unsigned();
            $table->text( 'value' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'eav_entity_text' );
    }

}
