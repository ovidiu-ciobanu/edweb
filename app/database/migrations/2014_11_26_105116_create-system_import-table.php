<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemImportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create( 'system_import', function( $table ) {
            
            $table->increments( 'import_id' );
            $table->integer( 'import_profile_id' )->unsigned();
            $table->string( 'path' );
            
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'system_import' );
	}

}
