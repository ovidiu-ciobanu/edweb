<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductEntityTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'product_entity', function( $table ) {
            $table->increments( 'entity_id' );
            $table->integer( 'entity_type_id' )->unsigned();
            $table->integer( 'attribute_set_id' )->unsigned();
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'product_entity' );
    }

}
