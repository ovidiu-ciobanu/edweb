<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'eav_attribute', function( $table ) {
            $table->increments( 'attribute_id' );
            $table->integer( 'entity_type_id' )->unsigned();
            $table->string( 'attribute_code' );
            $table->string( 'frontend_input' );
            $table->boolean( 'is_required' )->default( 0 );
            $table->string( 'default_value' )->nullable();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'eav_attribute' );
    }

}
