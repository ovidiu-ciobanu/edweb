<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavEntityTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create( 'eav_entity_type', function( $table )  {
            
            $table->increments( 'entity_type_id' );
            $table->string( 'entity_type_code' );
            $table->string( 'entity_type_name' );
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'eav_entity_type' );
	}

}
