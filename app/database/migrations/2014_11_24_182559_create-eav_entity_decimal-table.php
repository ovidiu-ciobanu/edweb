<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavEntityDecimalTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'eav_entity_decimal', function( $table ) {

            $table->increments( 'value_id' );
            $table->integer( 'entity_type_id' )->unsigned();
            $table->integer( 'attribute_id' )->unsigned();
            $table->integer( 'store_id' )->unsigned();
            $table->integer( 'entity_id' )->unsigned();
            $table->decimal( 'value', 12, 4 );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'eav_entity_decimal' );
    }

}
