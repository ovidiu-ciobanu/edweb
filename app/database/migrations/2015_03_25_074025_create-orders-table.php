<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'orders', function( $table ) {
            $table->string( 'increment_id' );
            $table->integer( 'parent_id' )->nullable();
            $table->integer( 'store_id' );

            $table->timestamps();

            $table->boolean( 'is_active' )->default( true );

            $table->integer( 'customer_id' );

            $table->decimal( 'tax_amount', 12, 4 )->nullable();
            $table->decimal( 'shipping_amount', 12, 4 )->nullable();
            $table->decimal( 'discount_amount', 12, 4 )->nullable();
            $table->decimal( 'subtotal', 12, 4 )->nullable();
            $table->decimal( 'grand_total', 12, 4 )->nullable();

            $table->decimal( 'total_paid', 12, 4 )->nullable();
            $table->decimal( 'total_refunded', 12, 4 )->nullable();
            $table->decimal( 'total_qty_ordered', 12, 4 )->nullable();
            $table->decimal( 'total_canceled', 12, 4 )->nullable();
            $table->decimal( 'total_invoiced', 12, 4 )->nullable();
            $table->decimal( 'total_online_refunded', 12, 4 )->nullable();
            $table->decimal( 'total_offline_refunded', 12, 4 )->nullable();
            $table->decimal( 'base_tax_amount', 12, 4 )->nullable();
            $table->decimal( 'base_shipping_amount', 12, 4 )->nullable();
            $table->decimal( 'base_subtotal', 12, 4 )->nullable();
            $table->decimal( 'base_grand_total', 12, 4 )->nullable();
            $table->decimal( 'base_total_paid', 12, 4 )->nullable();
            $table->decimal( 'base_total_refunded', 12, 4 )->nullable();
            $table->decimal( 'base_total_qty_ordered', 12, 4 )->nullable();
            $table->decimal( 'base_total_canceled', 12, 4 )->nullable();
            $table->decimal( 'base_total_invoiced', 12, 4 )->nullable();
            $table->decimal( 'base_total_online_refunded', 12, 4 )->nullable();
            $table->decimal( 'base_total_offline_refunded', 12, 4 )->nullable();
            $table->decimal( 'base_discount_amount', 12, 4 )->nullable();

            $table->integer( 'billing_address_id' )->nullable();
            $table->string( 'billing_firstname' )->nullable();
            $table->string( 'billing_lastname' )->nullable();
            $table->integer( 'shipping_address_id' )->nullable();
            $table->string( 'shipping_firstname' )->nullable();
            $table->string( 'shipping_lastname' )->nullable();
            $table->string( 'billing_name' )->nullable();
            $table->string( 'shipping_name' )->nullable();

            $table->decimal( 'store_to_base_rate', 12, 4 )->nullable();
            $table->decimal( 'store_to_order_rate', 12, 4 )->nullable();
            $table->decimal( 'base_to_global_rate', 12, 4 )->nullable();
            $table->decimal( 'base_to_order_rate', 12, 4 )->nullable();
            $table->decimal( 'weight', 12, 4 )->nullable();
            $table->string( 'store_name' )->nullable();
            $table->string( 'remote_ip' )->nullable();

            $table->string( 'status' )->nullable();
            $table->string( 'state' )->nullable();
            $table->string( 'applied_rule_ids' )->nullable();
            $table->string( 'global_currency_code' )->nullable();
            $table->string( 'base_currency_code' )->nullable();
            $table->string( 'store_currency_code' )->nullable();
            $table->string( 'order_currency_code' )->nullable();
            $table->string( 'shipping_method' )->nullable();
            $table->string( 'shipping_description' )->nullable();
            $table->string( 'customer_email' )->nullable();
            $table->string( 'customer_firstname' )->nullable();
            $table->string( 'customer_lastname' )->nullable();

            $table->integer( 'quote_id' )->nullable();
            $table->boolean( 'is_virtual' )->default( false );

            $table->integer( 'customer_group_id' )->nullable();
            $table->integer( 'customer_note_notify' )->nullable();
            $table->boolean( 'customer_is_guest' )->default( false );
            $table->boolean( 'email_sent' )->default( false );

            $table->increments( 'order_id' );

            $table->integer( 'gift_message_id' )->nullable();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'orders' );
    }

}
