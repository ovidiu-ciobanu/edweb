<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeOptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'eav_attribute_option', function( $table ) {

            $table->increments( 'option_id' );
            $table->integer( 'attribute_id' )->unsigned();
            $table->integer( 'sort_order' )->default( 0 );
            
            $table->foreign( 'attribute_id' )->references( 'attribute_id' )->on( 'eav_attribute' )->onDelete( 'cascade' );
            
        } );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'eav_attribute_option' );
	}

}
