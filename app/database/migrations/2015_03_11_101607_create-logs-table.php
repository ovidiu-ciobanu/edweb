<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'logs', function( $table ) {
            $table->increments( 'log_id' );
            $table->integer( 'store_id' );
            $table->integer( 'entity_id' )->nullable();
            $table->integer( 'entity_type_id' )->nullable();
            $table->text( 'data' );
            $table->timestamps();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'logs' );
    }

}
