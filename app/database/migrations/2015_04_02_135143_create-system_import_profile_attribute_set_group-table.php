<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemImportProfileAttributeSetGroupTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'system_import_profile_attribute_set_group', function($table) {
            $table->increments( 'system_import_profile_attribute_set_group_id' );
            $table->string( 'name' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'system_import_profile_attribute_set_group' );
    }

}
