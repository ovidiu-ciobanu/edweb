<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeGroupAttributeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'eav_attribute_group_attribute', function( $table ) {
            
            $table->increments( 'attribute_group_attribute_id' );
            $table->integer( 'attribute_set_id' )->unsigned();
            $table->integer( 'attribute_group_id' )->unsigned();
            $table->integer( 'attribute_id' )->unsigned();
            $table->integer( 'sort_order' )->default( 0 );
            
            // Delete attributes from this table if the attribute sets are deleted.
            $table->foreign( 'attribute_set_id' )->references( 'attribute_set_id' )->on( 'eav_attribute_set' )->onDelete( 'cascade' );
            
            // Delete attributes from this table if the attribute set groups are deleted.
            $table->foreign( 'attribute_group_id' )->references( 'attribute_group_id' )->on( 'eav_attribute_group' )->onDelete( 'cascade' );
            
            // Delete attributes from this table if the attributes are deleted.
            $table->foreign( 'attribute_id' )->references( 'attribute_id' )->on( 'eav_attribute' )->onDelete( 'cascade' );
            
        } );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'eav_attribute_group_attribute' );
	}

}
