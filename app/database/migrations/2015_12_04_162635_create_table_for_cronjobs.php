<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForCronjobs extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cron_jobs', function( Blueprint $table ) {

            $table->increments('cron_job_id');
            $table->string('status');
            $table->integer('total')->unsigned();
            $table->integer('step')->unsigned();
            $table->string('cron_type',100);
            $table->string('params');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('cron_jobs');
    }

}
