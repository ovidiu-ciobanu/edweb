<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeLabelTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'eav_attribute_label', function( $table ) {

            $table->increments( 'attribute_label_id' );
            $table->integer( 'attribute_id' )->unsigned();
            $table->integer( 'store_id' )->default( 0 );
            $table->string( 'value' )->nullable();
            
            $table->foreign( 'attribute_id' )->references( 'attribute_id' )->on( 'eav_attribute' )->onDelete( 'cascade' );
            
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop( 'eav_attribute_label' );
    }

}
