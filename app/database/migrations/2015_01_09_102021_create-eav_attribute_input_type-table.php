<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEavAttributeInputTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'eav_attribute_input_type', function( $table ) {
            
            $table->increments( 'input_type_id' );
            $table->string( 'input_type_code' )->unique();
            $table->string( 'input_type_name' );
            
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'eav_attribute_input_type' );
	}

}
