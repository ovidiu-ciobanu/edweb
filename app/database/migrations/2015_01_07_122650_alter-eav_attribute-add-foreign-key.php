<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEavAttributeAddForeignKey extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'eav_attribute', function( $table ) {

            $table->foreign( 'entity_type_id' )->references( 'entity_type_id' )->on( 'eav_entity_type' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'eav_attribute', function( $table ) {

            $table->dropForeign( 'eav_attribute_entity_type_id_foreign' );
        } );
    }

}
