<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreStoreTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'core_stores', function( $table ) {
			$table->smallInteger( 'store_id', true );
			$table->string( 'code', 32 )->unique();
			$table->smallInteger( 'website_id' );
			$table->smallInteger( 'group_id' );
			$table->string( 'name', 64 );
			$table->smallInteger( 'sort_order' )->default( 0 );
			$table->tinyInteger( 'is_active' )->default( 0 );
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'core_stores' );
	}

}
