<?php

class EavAttributeTableSeeder extends Seeder
{

    public function run()
    {
        $entityTypeProduct = App\Models\Eav\Entity\Type::where( 'entity_type_code', App\Models\Eav\Entity\Type::PRODUCT_CODE )->first();
        $entityTypeCategory = App\Models\Eav\Entity\Type::where( 'entity_type_code', App\Models\Eav\Entity\Type::CATEGORY_CODE )->first();
        $store_ids = \App\Models\Core\Store::all()->lists( 'store_id' );

        // SKU attribure
        $skuAttribute = new \App\Models\Eav\Attribute();
        $skuAttribute->entity_type_id = $entityTypeProduct->entity_type_id;
        $skuAttribute->attribute_code = 'sku';
        $skuAttribute->frontend_input = App\Models\Eav\Attribute\Input\Type::TEXT;
        $skuAttribute->is_required = 1;
        $skuAttribute->default_value = '';
        $skuAttribute->save();

        foreach ( $store_ids as $store_id ) {
            $skuAttributeLabel = new App\Models\Eav\Attribute\Label();
            $skuAttributeLabel->attribute_id = $skuAttribute->attribute_id;
            $skuAttributeLabel->store_id = $store_id;
            $skuAttributeLabel->value = 'SKU';
            $skuAttributeLabel->save();
        }

        // Product name attribute
        $productNameAttribute = new \App\Models\Eav\Attribute();
        $productNameAttribute->entity_type_id = $entityTypeProduct->entity_type_id;
        $productNameAttribute->attribute_code = 'product_name';
        $productNameAttribute->frontend_input = App\Models\Eav\Attribute\Input\Type::TEXT;
        $productNameAttribute->is_required = 1;
        $productNameAttribute->default_value = '';
        $productNameAttribute->save();

        foreach ( $store_ids as $store_id ) {
            $skuAttributeLabel = new App\Models\Eav\Attribute\Label();
            $skuAttributeLabel->attribute_id = $productNameAttribute->attribute_id;
            $skuAttributeLabel->store_id = $store_id;
            $skuAttributeLabel->value = 'Product name';
            $skuAttributeLabel->save();
        }

        // Category name attribute
        $categoryNameAttribute = new \App\Models\Eav\Attribute();
        $categoryNameAttribute->entity_type_id = $entityTypeCategory->entity_type_id;
        $categoryNameAttribute->attribute_code = 'category_name';
        $categoryNameAttribute->frontend_input = App\Models\Eav\Attribute\Input\Type::TEXT;
        $categoryNameAttribute->is_required = 1;
        $categoryNameAttribute->default_value = '';
        $categoryNameAttribute->save();

        foreach ( $store_ids as $store_id ) {
            $skuAttributeLabel = new App\Models\Eav\Attribute\Label();
            $skuAttributeLabel->attribute_id = $categoryNameAttribute->attribute_id;
            $skuAttributeLabel->store_id = $store_id;
            $skuAttributeLabel->value = 'Name';
            $skuAttributeLabel->save();
        }
    }

}
