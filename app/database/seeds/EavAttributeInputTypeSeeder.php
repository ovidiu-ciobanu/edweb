<?php

class EavAttributeInputTypeSeeder extends Seeder
{

    public function run()
    {
        DB::unprepared( "SET foreign_key_checks = 0;" );
        DB::table( 'eav_attribute_input_type' )->truncate();
        DB::unprepared( "SET foreign_key_checks = 1;" );

        \App\Models\Eav\Attribute\Input\Type::insert( [
            [
                'input_type_code' => 'text',
                'input_type_name' => 'Text Field'
            ],
            [
                'input_type_code' => 'decimal',
                'input_type_name' => 'Decimal'
            ],
            [
                'input_type_code' => 'textarea',
                'input_type_name' => 'Textarea'
            ],
            [
                'input_type_code' => 'boolean',
                'input_type_name' => 'Yes/No'
            ],
            [
                'input_type_code' => 'select',
                'input_type_name' => 'Dropdown'
            ],
            [
                'input_type_code' => 'multiselect',
                'input_type_name' => 'Multiple select'
            ],
            [
                'input_type_code' => 'datetime',
                'input_type_name' => 'Date time'
            ]
        ] );
    }

}
