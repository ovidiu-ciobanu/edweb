<?php

class CoreStoreTableSeeder extends Seeder
{

    public function run()
    {
        DB::table( 'core_stores' )->truncate();
        DB::unprepared( "SET sql_mode='NO_AUTO_VALUE_ON_ZERO';" );

        $admin_store = new \App\Models\Core\Store();
        $admin_store->store_id = 0;
        $admin_store->code = 'admin';
        $admin_store->group_id = 0;
        $admin_store->website_id = 0;
        $admin_store->name = 'Admin';
        $admin_store->is_active = 1;
        $admin_store->save();

        DB::unprepared( "SET sql_mode='';" );
        
        $default_store = new \App\Models\Core\Store();
        $default_store->code = 'default';
        $default_store->group_id = 1;
        $default_store->website_id = 1;
        $default_store->name = 'Default';
        $default_store->is_active = 1;
        $default_store->save();
    }

}
