<?php

class EavAttributeSetTableSeeder extends Seeder
{

    public function run()
    {
        // Product attribute set
        $entityTypeProduct = App\Models\Eav\Entity\Type::where( 'entity_type_code', App\Models\Eav\Entity\Type::PRODUCT_CODE )->first();
        $attribute_ids = \App\Models\Eav\Attribute::where( 'entity_type_id', $entityTypeProduct->entity_type_id )->lists( 'attribute_id' );

        $productAttributeSet = new \App\Models\Eav\Attribute\Set();
        $productAttributeSet->entity_type_id = $entityTypeProduct->entity_type_id;
        $productAttributeSet->attribute_set_name = 'Product';
        $productAttributeSet->save();

        $productAttributeSetGroup = new \App\Models\Eav\Attribute\Group();
        $productAttributeSetGroup->attribute_set_id = $productAttributeSet->attribute_set_id;
        $productAttributeSetGroup->attribute_group_name = 'General';
        $productAttributeSetGroup->sort_order = 1;
        $productAttributeSetGroup->save();

        $sort_order = 1;
        foreach ( $attribute_ids as $attribute_id ) {
            $productAttributeSetGroupAttribute = new App\Models\Eav\Attribute\Group\Attribute();
            $productAttributeSetGroupAttribute->attribute_set_id = $productAttributeSet->attribute_set_id;
            $productAttributeSetGroupAttribute->attribute_group_id = $productAttributeSetGroup->attribute_group_id;
            $productAttributeSetGroupAttribute->attribute_id = $attribute_id;
            $productAttributeSetGroupAttribute->sort_order = $sort_order;
            $productAttributeSetGroupAttribute->save();
            $sort_order++;
        }

        // Category attribute set
        $entityTypeCategory = App\Models\Eav\Entity\Type::where( 'entity_type_code', App\Models\Eav\Entity\Type::CATEGORY_CODE )->first();
        $attribute_ids = \App\Models\Eav\Attribute::where( 'entity_type_id', $entityTypeCategory->entity_type_id )->lists( 'attribute_id' );

        $categoryAttributeSet = new \App\Models\Eav\Attribute\Set();
        $categoryAttributeSet->entity_type_id = $entityTypeCategory->entity_type_id;
        $categoryAttributeSet->attribute_set_name = 'Category';
        $categoryAttributeSet->save();

        $categoryAttributeSetGroup = new \App\Models\Eav\Attribute\Group();
        $categoryAttributeSetGroup->attribute_set_id = $categoryAttributeSet->attribute_set_id;
        $categoryAttributeSetGroup->attribute_group_name = 'General';
        $categoryAttributeSetGroup->sort_order = 1;
        $categoryAttributeSetGroup->save();

        $sort_order = 1;
        foreach ( $attribute_ids as $attribute_id ) {
            $categoryAttributeSetGroupAttribute = new App\Models\Eav\Attribute\Group\Attribute();
            $categoryAttributeSetGroupAttribute->attribute_set_id = $categoryAttributeSet->attribute_set_id;
            $categoryAttributeSetGroupAttribute->attribute_group_id = $categoryAttributeSetGroup->attribute_group_id;
            $categoryAttributeSetGroupAttribute->attribute_id = $attribute_id;
            $categoryAttributeSetGroupAttribute->sort_order = $sort_order;
            $categoryAttributeSetGroupAttribute->save();
            $sort_order++;
        }
    }

}
