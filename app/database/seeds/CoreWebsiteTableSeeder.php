<?php

class CoreWebsiteTableSeeder extends Seeder
{

    public function run()
    {
        DB::table( 'core_websites' )->truncate();
        DB::unprepared( "SET sql_mode='NO_AUTO_VALUE_ON_ZERO';" );

        $admin_website = new \App\Models\Core\Website();
        $admin_website->website_id = 0;
        $admin_website->code = 'admin';
        $admin_website->name = 'Admin';
        $admin_website->save();

        DB::unprepared( "SET sql_mode='';" );
        
        $main_website = new \App\Models\Core\Website();
        $main_website->code = 'base';
        $main_website->name = 'Main Website ';
        $main_website->save();
    }

}
