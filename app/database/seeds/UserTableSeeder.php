<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table( 'users' )->truncate();

        \App\Models\User::create( [
            'firstname' => 'Nistor',
            'lastname' => 'Andrei',
            'email' => 'ovidiu.ciobanu@nextlogic.ro',
            'password' => Hash::make( 'nextlogic' )
        ] );
    }

}
