<?php

class CoreStoreGroupTableSeeder extends Seeder
{

    public function run()
    {
        DB::table( 'core_store_groups' )->truncate();
        DB::unprepared( "SET sql_mode='NO_AUTO_VALUE_ON_ZERO';" );

        $admin_store_group = new \App\Models\Core\Store\Group();
        $admin_store_group->group_id = 0;
        $admin_store_group->website_id = 0;
        $admin_store_group->name = 'Admin';
        $admin_store_group->default_store_id = 0;
        $admin_store_group->save();

        DB::unprepared( "SET sql_mode='';" );
        
        $main_store_group = new \App\Models\Core\Store\Group();
        $main_store_group->website_id = 1;
        $main_store_group->name = 'Main website store';
        $main_store_group->default_store_id = 1;
        $main_store_group->save();
    }

}
