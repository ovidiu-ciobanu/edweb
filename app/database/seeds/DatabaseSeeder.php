<?php

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call( 'UserTableSeeder' );
        $this->call( 'CoreWebsiteTableSeeder' );
        $this->call( 'CoreStoreGroupTableSeeder' );
        $this->call( 'CoreStoreTableSeeder' );

        $this->call( 'EavEntityTypeTableSeeder' );
        $this->call( 'EavAttributeInputTypeSeeder' );

        $this->call( 'EavAttributeTableSeeder' );
        $this->call( 'EavAttributeSetTableSeeder' );
    }

}
