<?php

class EavEntityTypeTableSeeder extends Seeder
{

    public function run()
    {
        DB::unprepared( "SET foreign_key_checks = 0;" );
        DB::table( 'eav_entity_type' )->truncate();
        DB::unprepared( "SET foreign_key_checks = 1;" );

        \App\Models\Eav\Entity\Type::insert( [
            [
            'entity_type_code' => 'product',
            'entity_type_name' => 'Product'
            ],
            [
                'entity_type_code' => 'category',
                'entity_type_name' => 'Category'
            ]
        ] );
        
    }

}
