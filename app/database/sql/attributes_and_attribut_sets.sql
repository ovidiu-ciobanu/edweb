SET foreign_key_checks = 0;

--
-- Dumping data for table `eav_attribute`
--

INSERT INTO `eav_attribute` (`attribute_id`, `entity_type_id`, `attribute_code`, `frontend_input`, `is_required`, `default_value`) VALUES
(2, 1, 'sku', 'text', 1, '0'),
(3, 2, 'name', 'text', 1, '0'),
(4, 1, 'product_name', 'text', 1, '0'),
(5, 1, 'description', 'textarea', 0, '0'),
(6, 1, 'unit_price', 'decimal', 0, '0'),
(7, 1, 'image', 'text', 0, '0');

--
-- Dumping data for table `eav_attribute_group`
--

INSERT INTO `eav_attribute_group` (`attribute_group_id`, `attribute_set_id`, `attribute_group_name`, `sort_order`) VALUES
(2, 2, 'General', 1),
(3, 3, 'General', 1),
(4, 2, 'Price', 2),
(5, 2, 'Images', 3);

--
-- Dumping data for table `eav_attribute_group_attribute`
--

INSERT INTO `eav_attribute_group_attribute` (`attribute_group_attribute_id`, `attribute_set_id`, `attribute_group_id`, `attribute_id`, `sort_order`) VALUES
(2, 2, 2, 2, 1),
(3, 3, 3, 3, 1),
(4, 2, 2, 4, 2),
(5, 2, 2, 5, 3),
(6, 2, 4, 6, 1),
(7, 2, 5, 7, 1);

--
-- Dumping data for table `eav_attribute_label`
--

INSERT INTO `eav_attribute_label` (`attribute_label_id`, `attribute_id`, `store_id`, `value`) VALUES
(3, 2, 0, 'SKU'),
(4, 2, 1, 'SKU'),
(5, 3, 0, 'Name'),
(6, 3, 1, 'Name'),
(7, 4, 0, 'Product name'),
(8, 4, 1, 'Product name'),
(9, 5, 0, 'Description'),
(10, 5, 1, 'Description'),
(11, 6, 0, 'Unit price'),
(12, 6, 1, 'Unit price'),
(13, 7, 0, 'Image'),
(14, 7, 1, 'Image');

--
-- Dumping data for table `eav_attribute_set`
--

INSERT INTO `eav_attribute_set` (`attribute_set_id`, `entity_type_id`, `attribute_set_name`, `sort_order`) VALUES
(2, 1, 'Product', 0),
(3, 2, 'Category', 0);

SET foreign_key_checks = 1;
