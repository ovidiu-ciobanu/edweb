<div class="row">
    <div class="col-md-6">
        Manage attribute sets
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                <a href="{{ URL::route('entity.attribute.set.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Add new attribute set</button>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Entity Type
                </th>
                <th>
                    Actions
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $attribute_sets as $attribute_set ) { ?>
                <tr>
                    <td>
                        {{ $attribute_set->attribute_set_name }}
                    </td>
                    <td>
                        {{ $attribute_set->entity_type->entity_type_name }}
                    </td>
                    <td>
                        <a href="{{ URL::route( 'entity.attribute.set.edit', $attribute_set->attribute_set_id ) }}">
                            <button type="button" class="btn btn-default btn-sm">Edit</button>
                        </a>
                        {{ Form::open( [ 'route' => [ 'entity.attribute.set.destroy', $attribute_set->attribute_set_id ], 'method' => 'delete', 'class' => 'pull-right' ] ) }}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

</div>