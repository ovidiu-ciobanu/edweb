<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

{{ Form::open( [ 'url' => [ 'entity/attribute/set', $attribute_set->attribute_set_id ], 'method' => 'put', 'class' => 'form form-horizontal', 'id' => 'attribute_set_form' ] ) }}

<div class="row">
    <div class="header">
        <div class="col-md-6">
            <h3>Edit attribute set</h3>
        </div>
        <div class="col-md-6">

            <ul class="pull-right list-inline">
                <li>
                    {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                </li>
                <li>
                    {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                </li>
            </ul>

        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-3">

        <div class="content">
            <div class="header-content">
                Set Name
            </div>
            <div class="entry-content">
                {{ Form::text('name', $attribute_set->attribute_set_name, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '' ] ) }}
            </div>
        </div>
        
        <div class="content">
            <div class="header-content">
                Entity type
            </div>
            <div class="entry-content">
                {{ Form::text(null, $attribute_set->entity_type->entity_type_name, [ 'class' => 'input-block-level form-control', 'id' => 'entity_type_select', 'disabled' => 'disabled' ] ) }}
            </div>
        </div>

    </div>
    <div class="col-md-3">
        <span class="inner_title">Groups</span>

        <ul class="list-inline">
            <li>
                <button type="button" class="btn btn-default btn-sm" id="attribute_set_btn_add_group">Add Group</button>
            </li>
            <li>
                <button type="button" class="btn btn-danger btn-sm" id="attribute_set_btn_del_group">Delete selected group</button>
            </li>
        </ul>

        <div id="groups_column">
            <ul class="group-root">
                <?php foreach( $attribute_set->groups as $group ) { ?>
                <li>
                    <div class="group-name">{{ $group->attribute_group_name }}</div>
                    <input type="hidden" rel="{{ $group->attribute_group_id }}" name="group_name[{{ $group->attribute_group_id }}]" value="{{ $group->attribute_group_name }}" class="input_group_name" />
                    <ul id="group_node_{{ $group->attribute_group_id }}" class="group-node connectedSortable">
                        <?php foreach( $group->attributes as $group_attribute ) { ?>
                            <li>
                                {{ $group_attribute->attribute->attribute_code }}
                                <input type="hidden" value="{{ $group_attribute->attribute->attribute_id }}"  class="attribute_id" />
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="col-md-6">
        <span class="inner_title">Unused Attributes</span>

        <div>
            <ul id="unused_attributes" class="connectedSortable clearfix">

                @foreach ( $attributes as $attribute )
                <li>
                    {{ $attribute->attribute_code }}
                    <input type="hidden" value="{{ $attribute->attribute_id }}"  class="attribute_id" />
                </li>
                @endforeach

            </ul>
        </div>

    </div>

</div>

{{ Form::close() }}

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
(function ($) {
    $(function () {

        var groups = {sort: {}, attributes: {}};

        var sort_order_groups = function () {
            groups = {sort: {}, attributes: {}};

            $('#groups_column > ul.group-root > li').each(function (i, v) {
                var group_id = $('input', this).attr('rel');
                groups.sort[ group_id ] = (i + 1);

                $('ul.group-node > li', this).each(function (index, value) {
                    if (!groups.attributes.hasOwnProperty(group_id)) {
                        groups.attributes[ group_id ] = [];
                    }

                    var attribute_id = $('.attribute_id', this).val();
                    groups.attributes[ group_id ].push(attribute_id);
                });

            });
            
        };

        sort_order_groups();

        $('#attribute_set_form').on('submit', function (e) {

            var html = '';
            for (var group_index in groups.attributes) {
                var group = groups.attributes[ group_index ];
                for (var index in group) {
                    html += '<input type="hidden" name="group[' + group_index + '][]" value="' + group[ index ] + '" />';
                    html += '<input type="hidden" name="attribute_sort_order[' + group[ index ] + ']" value="' + (parseInt(index) + 1) + '" />';
                }
            }

            for (var group_index in groups.sort) {
                html += '<input type="hidden" name="group_sort_order[' + group_index + ']" value="' + groups.sort[ group_index ] + '" />';
            }

            $(this).append(html);

        });

        // Sort root groups
        $('.group-root').sortable({update: sort_order_groups});

        // Dragable elements
        $('#unused_attributes, .group-node').sortable({
            connectWith: ".connectedSortable", update: sort_order_groups
        }).disableSelection();

        var group_node_increment = parseInt( $( '.input_group_name' ).last().attr( 'rel' ) ) + 1;
        $('#attribute_set_btn_add_group').on('click', function (e) {

            var answer = prompt("Please enter a new group name!");
            if (answer === null || answer === '') {
                return false;
            }

            var html = '<li><div class="group-name">' + answer + '</div><input class="input_group_name" type="hidden" rel="' + group_node_increment + '" name="group_name[' + group_node_increment + ']" value="' + answer + '" /><ul id="group_node_' + group_node_increment + '" class="group-node connectedSortable"></ul></li>';
            $('#groups_column .group-root').append(html);
            group_node_increment++;

            $('.group-node').sortable({connectWith: '.connectedSortable', update: sort_order_groups});

        });

        $('#attribute_set_btn_del_group').on('click', function (e) {

            if ($('#groups_column .selected').length <= 0) {
                return false;
            }

            if (confirm("Are you sure?") === false) {
                return false;
            }

            $('#unused_attributes').append($('ul.group-node > li', '#groups_column .selected'));

            $('#groups_column .selected').remove();
        });

        $(document.body).on('click', '.group-name', function (e) {
            $(this).parent().toggleClass('selected');
        });

        $(document.body).on('dblclick', '.group-name', function (e) {
            var answer = prompt("Please enter a new group name!", $(this).html());
            if (answer === null || answer === '') {
                return false;
            }

            $('.input_group_name', $(this).parent()).val(answer);
            $(this).html(answer);
        });
    });
})(jQuery);
</script>
