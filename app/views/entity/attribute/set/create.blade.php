<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

{{ Form::open( [ 'url'=>'entity/attribute/set', 'class' => 'form form-horizontal', 'id' => 'attribute_set_form' ] ) }}

<div class="row">
    <div class="header">
        <div class="col-md-6">
            <h3>New attribute set</h3>
        </div>
        <div class="col-md-6">

            <ul class="pull-right list-inline">
                <li>
                    {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                </li>
                <li>
                    {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                </li>
            </ul>

        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-3">

        <div class="content">
            <div class="header-content">
                Set Name
            </div>
            <div class="entry-content">
                {{ Form::text('name', Input::old( 'name' ), [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => 'required' ] ) }}
            </div>
        </div>

        <div class="content">
            <div class="header-content">
                Entity type
            </div>
            <div class="entry-content">
                {{ Form::select('entity_type', [ 0 => 'Please select' ] + $entity_types, null, [ 'class' => 'input-block-level form-control', 'required' => 'required', 'id' => 'entity_type_select' ] ) }}
            </div>
        </div>

    </div>
    <div class="col-md-3">
        <span class="inner_title">Groups</span>

        <ul class="list-inline">
            <li>
                <button type="button" class="btn btn-default btn-sm" id="attribute_set_btn_add_group">Add Group</button>
            </li>
            <li>
                <button type="button" class="btn btn-danger btn-sm" id="attribute_set_btn_del_group">Delete selected group</button>
            </li>
        </ul>

        <div id="groups_column">
            <ul class="group-root"></ul>
        </div>
    </div>

    <div class="col-md-6">
        <span class="inner_title">Unused Attributes</span>

        <div>
            <ul id="unused_attributes" class="connectedSortable clearfix">

                <?php foreach ( $attributes as $attribute ) { ?>
                    <li class="entity_type_<?php echo $attribute->entity_type_id; ?>" style="display: none;">
                        {{ $attribute->attribute_code }}
                        <input type="hidden" value="{{ $attribute->attribute_id }}"  class="attribute_id" />
                    </li>
                <?php } ?>

            </ul>
        </div>

    </div>

</div>

{{ Form::close() }}

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript">
(function ($) {
    $(function () {

        var groups = {sort: {}, attributes: {}};

        var sort_order_groups = function () {
            groups = {sort: {}, attributes: {}};

            $('#groups_column > ul.group-root > li').each(function (i, v) {
                var group_id = $('input', this).attr('rel');
                groups.sort[ group_id ] = (i + 1);

                $('ul.group-node > li', this).each(function (index, value) {
                    if (!groups.attributes.hasOwnProperty(group_id)) {
                        groups.attributes[ group_id ] = [];
                    }

                    var attribute_id = $('.attribute_id', this).val();
                    groups.attributes[ group_id ].push(attribute_id);
                });

            });

            console.log(groups);

        };
        
        // On form submit add groups and attributes.
        $('#attribute_set_form').on('submit', function (e) {

            var html = '';
            for (var group_index in groups.attributes) {
                var group = groups.attributes[ group_index ];
                for (var index in group) {
                    html += '<input type="hidden" name="group[' + group_index + '][]" value="' + group[ index ] + '" />';
                    html += '<input type="hidden" name="attribute_sort_order[' + group[ index ] + ']" value="' + (parseInt(index) + 1) + '" />';
                }
            }

            for (var group_index in groups.sort) {
                html += '<input type="hidden" name="group_sort_order[' + group_index + ']" value="' + groups.sort[ group_index ] + '" />';
            }

            $(this).append(html);

        });

        // Sort root groups
        $('.group-root').sortable({update: sort_order_groups});

        // Dragable elements
        $('#unused_attributes, .group-node').sortable({
            connectWith: ".connectedSortable"
        }).disableSelection();

        var group_node_increment = 0;
        $('#attribute_set_btn_add_group').on('click', function (e) {

            var answer = prompt("Please enter a new group name!");
            if (answer === null || answer === '') {
                return false;
            }

            var html = '<li><div class="group-name">' + answer + '</div><input class="input_group_name" type="hidden" rel="' + group_node_increment + '" name="group_name[' + group_node_increment + ']" value="' + answer + '" /><ul id="group_node_' + group_node_increment + '" class="group-node connectedSortable"></ul></li>';
            $('#groups_column .group-root').append(html);
            group_node_increment++;

            $('.group-node').sortable({connectWith: '.connectedSortable', update: sort_order_groups});

        });

        $('#attribute_set_btn_del_group').on('click', function (e) {

            if ($('#groups_column .selected').length <= 0) {
                return false;
            }

            if (confirm("Are you sure?") === false) {
                return false;
            }

            $('#unused_attributes').append($('ul.group-node > li', '#groups_column .selected'));

            $('#groups_column .selected').remove();

            sort_order_groups();
        });

        $(document.body).on('click', '.group-name', function (e) {
            $(this).parent().toggleClass('selected');
        });

        $(document.body).on('dblclick', '.group-name', function (e) {
            var answer = prompt("Please enter a new group name!", $(this).html());
            if (answer === null || answer === '') {
                return false;
            }

            $('.input_group_name', $(this).parent()).val(answer);
            $(this).html(answer);
        });
        
        var unused_attributes_html = $('#unused_attributes').html();
        // Entity type change
        $('#entity_type_select').on('change', function() {
            var value = $( this ).val();
            
            groups = {sort: {}, attributes: {}};
            
            $('#unused_attributes').html( unused_attributes_html );
            
            var class_to_show = '.entity_type_' + value;
            
            $('li', '#unused_attributes').hide();
            $('#groups_column .group-root').html('');
            
            $(class_to_show, '#unused_attributes').show();
            
        });
    });
})(jQuery);
</script>
