<div class="row left-tabbed-content">

    <?php if ( count( $errors ) > 0 ) { ?>
        <div class="container-fluid">
            <ul class="errors">
                <?php foreach ( $errors->all() as $error ) { ?>
                    <li class="alert alert-warning"><?php echo $error; ?></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <div class="col-md-2 side-col no-padding-right">
        <h3>Attribute Information</h3>
        <ul class="tabs">
            <li>
                <a id="product_attribute_tabs_main" class="active" href="#">
                    <span>
                        Properties
                    </span>
                </a>
            </li>
            <li>
                <a id="product_attribute_tabs_labels" href="#">
                    <span>
                        Manage Label / Options
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-10 main-col">

        {{ Form::open( [ 'url' => [ 'entity/attribute', $attribute->attribute_id ], 'method' => 'put', 'class' => 'form form-horizontal' ] ) }}

        <div class="main-col-inner">
            <div class="header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Edit product attribute</h3>
                    </div>
                    <div class="col-md-6">

                        <ul class="pull-right list-inline">
                            <li>
                                {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                            </li>
                            <li>
                                {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="content">

                <div id="product_attribute_tabs_main_content">
                    <div class="header-content"><span class="fieldset-legend">Attribute Properties</span></div>
                    <div class="entry-content">

                        <div class="form-group form-group-sm">
                            <label for="attirbute_code" class="col-sm-2 control-label">Attribute Code</label>
                            <div class="col-sm-5">
                                {{ Form::text(null, $attribute->attribute_code, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Attribute Code', 'id' => 'attirbute_code', 'disabled' => 'disabled' ] ) }}
                            </div>
                        </div>
                        
                        <div class="form-group form-group-sm">
                            <label for="inputName" class="col-sm-2 control-label">Input type</label>
                            <div class="col-sm-5">
                                {{ Form::text(null, $attribute->input_type->input_type_name, [ 'class' => 'input-block-level form-control', 'id' => 'attribute_input_type', 'disabled' => 'disabled' ] ) }}
                            </div>
                        </div>
                        
                        <div class="form-group form-group-sm">
                            <label for="attirbute_code" class="col-sm-2 control-label">Entity type</label>
                            <div class="col-sm-5">
                                {{ Form::text(null, $attribute->entity_type->entity_type_name, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Attribute Code', 'id' => 'attirbute_code', 'disabled' => 'disabled' ] ) }}
                            </div>
                        </div>
                        
                        <div id="attribute_input_types">

                            <div id="attribute_input_type_text" style="<?php if ( $attribute->frontend_input == 'text' ) { ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                                <div class="form-group form-group-sm">
                                    <label for="default_value_text" class="col-sm-2 control-label">Default value</label>
                                    <div class="col-sm-5">
                                        {{ Form::text('attribute[default_value_text]', $attribute->default_value, [ 'class' => 'input-block-level form-control', 'id' => 'default_value_text' ] ) }}
                                    </div>
                                </div>
                            </div>

                            <div id="attribute_input_type_decimal" style="<?php if ( $attribute->frontend_input == 'decimal' ) { ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                                <div class="form-group form-group-sm">
                                    <label for="default_value_decimal" class="col-sm-2 control-label">Default value</label>
                                    <div class="col-sm-5">
                                        {{ Form::text('attribute[default_value_decimal]', $attribute->default_value, [ 'class' => 'input-block-level form-control', 'id' => 'default_value_decimal' ] ) }}
                                    </div>
                                </div>
                            </div>

                            <div id="attribute_input_type_datetime" style="<?php if ( $attribute->frontend_input == 'datetime' ) { ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                                <div class="form-group form-group-sm">
                                    <label for="default_value_datetime" class="col-sm-2 control-label">Default value</label>
                                    <div class="col-sm-5">
                                        {{ Form::text('attribute[default_value_datetime]', $attribute->default_value, [ 'class' => 'input-block-level form-control datetime_picker', 'id' => 'default_value_datetime' ] ) }}
                                    </div>
                                </div>
                            </div>

                            <div id="attribute_input_type_textarea" style="<?php if ( $attribute->frontend_input == 'textarea' ) { ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                                <div class="form-group">
                                    <label for="default_value_textarea" class="col-sm-2 control-label">Default value</label>
                                    <div class="col-sm-5">
                                        {{ Form::textarea('attribute[default_value_textarea]', $attribute->default_value, [ 'class' => 'input-block-level form-control', 'id' => 'default_value_textarea', 'rows' => 5 ] ) }}
                                    </div>
                                </div>
                            </div>

                            <div id="attribute_input_type_boolean" style="<?php if ( $attribute->frontend_input == 'boolean' ) { ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                                <div class="form-group form-group-sm">
                                    <label for="default_value_boolean" class="col-sm-2 control-label">Default value</label>
                                    <div class="col-sm-5">
                                        {{ Form::select('attribute[default_value_boolean]', [ 0 => 'No', 1 => 'Yes' ], $attribute->default_value, [ 'class' => 'input-block-level form-control', 'id' => 'default_value_boolean' ] ) }}
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group form-group-sm">
                            <label for="attribute_required" class="col-sm-2 control-label">Required</label>
                            <div class="col-sm-5">
                                {{ Form::select('attribute[required]', [ 0 => 'No', 1 => 'Yes' ], $attribute->is_required, [ 'class' => 'input-block-level form-control', 'id' => 'attribute_required' ] ) }}
                            </div>
                        </div>

                    </div>
                </div>

                <div id="product_attribute_tabs_labels_content" style="display: none;">

                    <div id="attribute_manage_titles">
                        <div class="header-content"><span class="fieldset-legend">Manage Titles</span></div>
                        <div class="entry-content">

                            <?php $first = true ?>
                            <?php foreach ( $attribute->labels as $label ) { ?>
                                <div class="form-group form-group-sm">
                                    <label for="inputName" class="col-sm-2 control-label">{{ $label->store->name }}</label>
                                    <div class="col-sm-5">
                                        <?php
                                        if ( $first == true ) {
                                            $first = false;
                                            ?>
                                            {{ Form::text('frontend_label[' . $label->attribute_label_id . ']', $label->value, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => 'required', 'id' => 'inputName' ] ) }}
                                        <?php } else { ?>
                                            {{ Form::text('frontend_label[' . $label->attribute_label_id . ']', $label->value, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'id' => 'inputName' ] ) }}
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div id="attribute_manage_options" style="<?php if ( count( $attribute->options ) <= 0 ) { ?> display: none; <?php } ?>">
                        <div class="header-content"><span class="fieldset-legend">Manage Options (values of your attribute)</span></div>
                        <div class="entry-content">

                            <table class="table" id="attribute_options">
                                <tr>
                                    @foreach( $stores as $store_id => $name )
                                    <th>
                                        {{ $name }}
                                    </th>
                                    @endforeach
                                    <th>
                                        Position
                                    </th>
                                    <th>
                                        <button type="button" class="btn btn-default btn-sm" id="attribute_btn_add_option">Add option</button>
                                    </th>
                                </tr>

                                @if( count( $attribute->options ) > 0 )
                                @foreach ( $attribute->options as $key => $option )
                                <tr>
                                    @foreach ( $option->values as $option_value )
                                    <td>
                                        {{ Form::text('old_option[' . $option->option_id . '][value][' . $option_value->value_id . ']', $option_value->value, [ 'class'=>'input-block-level form-control' ] ) }}
                                    </td>
                                    @endforeach
                                    <td>
                                        {{ Form::text('old_option[' . $option->option_id . '][sort_order]', $option->sort_order, [ 'class'=>'input-block-level form-control', 'id' => 'inputName' ] ) }}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm attribute_btn_delete_old_option" id="{{ $option->option_id }}">Delete</button>
                                    </td>
                                </tr>
                                @endforeach
                                @endif

                            </table>

                        </div>
                    </div>

                </div>

            </div>

        </div>

        {{ Form::close() }}

    </div>

    <script type="text/javascript">
        (function ($) {
            $(function () {

                $('ul.tabs li a').on('click', function (e) {

                    $('ul.tabs li a').removeClass('active');
                    $(this).addClass('active');

                    var id = $(this).attr('id');
                    $('.main-col-inner .content > div').css('display', 'none');
                    $('#' + id + '_content', '.main-col-inner .content').css('display', 'block');

                    e.preventDefault();
                });

                $('#attribute_input_type').on('change', function (e) {
                    $('#attribute_input_types > div').css('display', 'none');
                    var select_value = $(this).val()

                    var selector = '#attribute_input_type_' + select_value;
                    $(selector).css('display', 'block');

                    if (select_value == 'select' || select_value == 'multiselect') {
                        $('#attribute_manage_options').css('display', 'block');
                    } else {
                        $('#attribute_manage_options').css('display', 'none');
                    }
                });

                var option_increment = 0;
                $('#attribute_btn_add_option').on('click', function (e) {

                    var option_row = '<tr>';
                            @foreach($stores as $store_id => $name)
                            option_row += '<td>';
                    option_row += '<input type="text" name="option[' + option_increment + '][value][{{ $store_id }}]" class="input-block-level form-control" />';
                    option_row += '</td>';
                            @endforeach

                            option_row += '<td>';
                    option_row += '<input type="text" name="option[' + option_increment + '][sort_order]" class="input-block-level form-control" />';
                    option_row += '</td>';
                    option_row += '<td>\
                                            <button type="button" class="btn btn-danger btn-sm attribute_btn_delete_option">Delete</button>\
                                        </td>\
                                    </tr>';
                    option_increment++;
                    $('#attribute_options').append(option_row);

                    e.preventDefault();
                });

                // Delete for new added option
                $(document.body).on('click', '.attribute_btn_delete_option', function (e) {
                    $(this).parent().parent().remove();
                    e.preventDefault();
                });

                // Delete old options
                $('.attribute_btn_delete_old_option').on('click', function () {
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'delete_option[]',
                        value: $(this).attr('id')
                    }).appendTo($(this).parent());

                    $(this).parent().parent().css('display', 'none');
                });

            });
        })(jQuery);
    </script>
</div>