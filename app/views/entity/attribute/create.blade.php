<div class="row left-tabbed-content">
    
    @if ( count($errors) > 0 )
        <div class="container-fluid">
                <ul class="errors">
                    @foreach($errors->all() as $error)
                    <li class="alert alert-warning">{{ $error }}</li>
                    @endforeach
                </ul>
        </div>
    @endif
    
    <div class="col-md-2 side-col no-padding-right">
        <h3>Attribute Information</h3>
        <ul class="tabs">
            <li>
                <a id="product_attribute_tabs_main" class="active" href="#">
                    <span>
                        Properties
                    </span>
                </a>
            </li>
            <li>
                <a id="product_attribute_tabs_labels" href="#">
                    <span>
                        Manage Label / Options
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-10 main-col">
        
        {{ Form::open( [ 'url'=>'entity/attribute', 'class' => 'form form-horizontal' ] ) }}
        
            <div class="main-col-inner">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>New product attribute</h3>
                        </div>
                        <div class="col-md-6">
                            
                            <ul class="pull-right list-inline">
                                <li>
                                    {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                                </li>
                                <li>
                                    {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
                <div class="content">

                    <div id="product_attribute_tabs_main_content">
                        <div class="header-content"><span class="fieldset-legend">Attribute Properties</span></div>
                        <div class="entry-content">

                            <div class="form-group form-group-sm">
                                <label for="inputName" class="col-sm-2 control-label">Attribute Code <span class="required">*</span></label>
                                <div class="col-sm-5">
                                    {{ Form::text('attribute[code]', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
                                </div>
                            </div>
                            
                            <div class="form-group form-group-sm">
                                <label for="attribute_input_type" class="col-sm-2 control-label">Input type <span class="required">*</span></label>
                                <div class="col-sm-5">
                                    {{ Form::select('attribute[input_type]', $input_types, 'text', [ 'class' => 'input-block-level form-control', 'required' => '', 'id' => 'attribute_input_type' ] ) }}
                                </div>
                            </div>
                            
                            <div class="form-group form-group-sm">
                                <label for="attribute_entity_type" class="col-sm-2 control-label">Entity type <span class="required">*</span></label>
                                <div class="col-sm-5">
                                    {{ Form::select('attribute[entity_type]', $entity_types, 'text', [ 'class' => 'input-block-level form-control', 'required' => '', 'id' => 'attribute_entity_type' ] ) }}
                                </div>
                            </div>
                            
                            <div id="attribute_input_types">
                                
                                <div id="attribute_input_type_text">
                                    <div class="form-group form-group-sm">
                                        <label for="default_value_text" class="col-sm-2 control-label">Default value</label>
                                        <div class="col-sm-5">
                                            {{ Form::text('attribute[default_value_text]', Input::old( 'default_value_text' ), [ 'class' => 'input-block-level form-control', 'id' => 'default_value_text' ] ) }}
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="attribute_input_type_decimal" style="display: none;">
                                    <div class="form-group form-group-sm">
                                        <label for="default_value_decimal" class="col-sm-2 control-label">Default value</label>
                                        <div class="col-sm-5">
                                            {{ Form::text('attribute[default_value_decimal]', Input::old( 'default_value_decimal' ), [ 'class' => 'input-block-level form-control', 'id' => 'default_value_decimal' ] ) }}
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="attribute_input_type_datetime" style="display: none;">
                                    <div class="form-group form-group-sm">
                                        <label for="default_value_datetime" class="col-sm-2 control-label">Default value</label>
                                        <div class="col-sm-5">
                                            {{ Form::text('attribute[default_value_datetime]', Input::old( 'default_value_datetime' ), [ 'class' => 'input-block-level form-control datetime_picker', 'id' => 'default_value_datetime' ] ) }}
                                        </div>
                                    </div>
                                </div>

                                <div id="attribute_input_type_textarea" style="display: none;">
                                    <div class="form-group">
                                        <label for="default_value_textarea" class="col-sm-2 control-label">Default value</label>
                                        <div class="col-sm-5">
                                            {{ Form::textarea('attribute[default_value_textarea]', Input::old( 'default_value_textarea' ), [ 'class' => 'input-block-level form-control', 'id' => 'default_value_textarea', 'rows' => 5 ] ) }}
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="attribute_input_type_boolean" style="display: none;">
                                    <div class="form-group form-group-sm">
                                        <label for="default_value_boolean" class="col-sm-2 control-label">Default value</label>
                                        <div class="col-sm-5">
                                            {{ Form::select('attribute[default_value_boolean]', [ 0 => 'No', 1 => 'Yes' ], Input::old( 'default_value_boolean' ), [ 'class' => 'input-block-level form-control', 'id' => 'default_value_boolean' ] ) }}
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="form-group form-group-sm">
                                <label for="attribute_required" class="col-sm-2 control-label">Required</label>
                                <div class="col-sm-5">
                                    {{ Form::select('attribute[required]', [ 0 => 'No', 1 => 'Yes' ], Input::old( 'attribute_required' ), [ 'class' => 'input-block-level form-control', 'id' => 'attribute_required' ] ) }}
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="product_attribute_tabs_labels_content" style="display: none;">
                        
                        <div id="attribute_manage_titles">
                            <div class="header-content"><span class="fieldset-legend">Manage Titles</span></div>
                            <div class="entry-content">
                                
                                <?php $first = true ?>
                                @foreach( $stores as $store_id => $name )
                                <div class="form-group form-group-sm">
                                    <label for="inputName" class="col-sm-2 control-label">{{ $name }}</label>
                                    <div class="col-sm-5">
                                        @if ( $first == true )
                                        {{ Form::text('frontend_label[' . $store_id . ']', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => 'required', 'id' => 'inputName' ] ) }}
                                        <?php $first = false; ?>
                                        @else
                                        {{ Form::text('frontend_label[' . $store_id . ']', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'id' => 'inputName' ] ) }}
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        
                        <div id="attribute_manage_options" style="display: none;">
                            <div class="header-content"><span class="fieldset-legend">Manage Options (values of your attribute)</span></div>
                            <div class="entry-content">
                                
                                <table class="table" id="attribute_options">
                                    <tr>
                                        @foreach( $stores as $store_id => $name )
                                        <th>
                                            {{ $name }}
                                        </th>
                                        @endforeach
                                        <th>
                                            Position
                                        </th>
                                        <th>
                                            <button type="button" class="btn btn-default btn-sm" id="attribute_btn_add_option">Add option</button>
                                        </th>
                                    </tr>
                                    
                                </table>
                                
                            </div>
                        </div>
                        
                    </div>

                </div>

            </div>
        
        {{ Form::close() }}
        
    </div>
    
    <script type="text/javascript">
        (function( $ ) {
            $(function() {
                
                $( 'ul.tabs li a' ).on( 'click', function( e ) {
                    
                    $( 'ul.tabs li a' ).removeClass( 'active' );
                    $( this ).addClass( 'active' );
                    
                    var id = $( this ).attr( 'id' );
                    $( '.main-col-inner .content > div' ).css( 'display', 'none' );
                    $( '#' + id + '_content', '.main-col-inner .content' ).css( 'display', 'block' );
                    
                    e.preventDefault();
                });
                
                $( '#attribute_input_type' ).on( 'change', function( e ) {
                    $( '#attribute_input_types > div' ).css( 'display', 'none' );
                    var select_value = $( this ).val()
                    
                    var selector = '#attribute_input_type_' + select_value;
                    $( selector ).css( 'display', 'block' );
                    
                    if ( select_value == 'select' || select_value == 'multiselect' ) {
                        $( '#attribute_manage_options' ).css( 'display', 'block' );
                    } else {
                        $( '#attribute_manage_options' ).css( 'display', 'none' );
                    }
                });
                
                var option_increment = 0;
                $( '#attribute_btn_add_option' ).on( 'click', function( e ) {
                    
                    var option_row = '<tr>';
                    @foreach( $stores as $store_id => $name )
                        option_row += '<td>';
                        option_row += '<input type="text" name="option[' + option_increment + '][value][{{ $store_id }}]" class="input-block-level form-control" />';
                        option_row += '</td>';
                                        @endforeach
                                        
                        option_row += '<td>';
                        option_row += '<input type="text" name="option[' + option_increment + '][sort_order]" class="input-block-level form-control" />';
                        option_row += '</td>';
                        option_row += '<td>\
                                            <button type="button" class="btn btn-danger btn-sm attribute_btn_delete_option">Delete</button>\
                                        </td>\
                                    </tr>';
                    option_increment++;
                    $( '#attribute_options' ).append( option_row );
                    
                    e.preventDefault();
                });
                
                $( document.body ).on( 'click', '.attribute_btn_delete_option', function( e ) {
                    $( this ).parent().parent().remove();
                    e.preventDefault();
                });
                
            });
        })( jQuery );
    </script>
</div>