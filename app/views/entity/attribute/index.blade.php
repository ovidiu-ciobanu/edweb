<div class="row">
    <div class="col-md-6">
        Manage attributes
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                <a href="{{ URL::route('entity.attribute.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Add new attribute</button>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row filters">
    <div class="col-sm-2">
        {{ Form::select(null, $search_options, Input::get('option', null), [ 'class' => 'form-control' ]) }}

    </div>
    <div class="col-sm-2">
        {{ Form::text(null, Input::get('search', null), [ 'class' => 'form-control' ]) }}
    </div>
    <div class="col-sm-1">
        <button id="filter_submit_button" class="btn btn-primary">Search</button>
    </div>
    <div class="col-sm-1">
        <a href="{{ URL::route('entity.attribute.index') }}">
            <button type="button" class="btn btn-default btn-danger">Reset</button>
        </a>
    </div>
</div>

<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    Attribute code
                </th>
                <th>
                    Attribute label
                </th>
                <th>
                    Entity Type
                </th>
                <th>
                    Required
                </th>
                <th>
                    Actions
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $attributes as $attribute ) { ?>
                <tr>
                    <td>
                        {{ $attribute->attribute_code }}
                    </td>
                    <td>
                        {{ $attribute->label->value }}
                    </td>
                    <td>
                        {{ $attribute->entity_type->entity_type_name }}
                    </td>
                    <td>
                        <?php if ( $attribute->is_required ) { ?>
                            Yes
                        <?php } else { ?>
                            No
                        <?php } ?>
                    </td>
                    <td>
                        <a href="{{ URL::route( 'entity.attribute.edit', $attribute->attribute_id ) }}">
                            <button type="button" class="btn btn-default btn-sm">Edit</button>
                        </a>
                        {{ Form::open( [ 'route' => [ 'entity.attribute.destroy', $attribute->attribute_id ], 'method' => 'delete', 'class' => 'pull-right' ] ) }}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm( 'Are you sure?' )">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <div class="text-center">
        <?php
        $appends = [ ];
        if ( Input::has( 'option' ) && Input::has( 'search' ) ) {
            $appends['option'] = Input::get( 'option' );
            $appends['search'] = Input::get( 'search' );
        }
        echo $attributes->appends( $appends )->links();
        ?>
    </div>

</div>

<script type="text/javascript">
    (function ( $ ) {
        $( function () {
            // Filter submit
            $( '#filter_submit_button' ).on( 'click', function () {
                var select_option_value = $( '.filters select' ).val();
                var input_search = $( '.filters input' ).val();

                var href = window.location.href.split( '?' )[0];
                if (select_option_value !== '' && input_search !== '') {
                    href += '?option=' + select_option_value;
                    href += '&search=' + input_search;
                }

                window.location.href = href;
            } );
        } );
    })( jQuery );
</script>
