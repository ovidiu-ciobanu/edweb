<div class="navbar-collapse collapse" id="main-navbar-collapse">
    <ul class="nav navbar-nav">
        <?php if ( !Auth::check() ) { ?>
            <li>{{ HTML::link('login', 'Login') }}</li>
        <?php } else { ?>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Catalog <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li>{{ HTML::link('catalog/product', 'Manage Products') }}</li>
                    <li>{{ HTML::link('catalog/category', 'Manage Categories') }}</li>
                    <li class="divider"></li>
                    <li>{{ HTML::link('entity/attribute', 'Manage Attributes') }}</li>
                    <li>{{ HTML::link('entity/attribute/set', 'Manage Attribute Sets') }}</li>
                </ul>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sales <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li>{{ HTML::link('sales/order', 'Orders') }}</li>
                </ul>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">System <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li>{{ HTML::link('system/import', 'Import') }}</li>
                    <li>{{ HTML::link('system/import/profile', 'Import profile') }}</li>
                    <li>{{ HTML::link('system/import/profile/attribute/set/group', 'Import settings') }}</li>
                    <li class="divider"></li>
                    <li>{{ HTML::link('system/store', 'Stores') }}</li>
                    <li class="divider"></li>
                    <li>{{ HTML::link('log', 'Logs') }}</li>
                    <li class="divider"></li>
                    <li>{{ HTML::link('system/user', 'Users') }}</li>
                </ul>
            </li>
        <?php } ?>
    </ul>

    <ul class="nav navbar-nav navbar-right">
        <?php if ( Auth::check() ) { ?>
            <li>{{ HTML::link('logout', 'Logout') }}</li>
        <?php } ?>
    </ul>
</div>