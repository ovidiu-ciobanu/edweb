<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>EDWEB</title>

        <link href='http://fonts.googleapis.com/css?family=Lato:400,900italic,900,700italic,700,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>

        {{ HTML::style('theme/bootstrap/css/bootstrap.css') }}
        {{ HTML::style('theme/css/jquery.datetimepicker.css') }}
        {{ HTML::style('theme/css/main.css') }}

        <meta name="_token" content="{{ csrf_token() }}" />

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    </head>

    <body>

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{ HTML::link('/', 'EDWEB', array( 'class' => 'navbar-brand' ) ) }}
                </div>

                @include('layouts.menu')

            </div>
        </nav>

        <div class="container-fluid">
            @if( Session::has('message') )
            <p class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                {{ Session::get('message') }}
            </p>
            @endif {{ $content }}
        </div>

        <div id="overlay">
            <div class="background">
                <div class="loading"></div>
            </div>
        </div>

        {{ HTML::script('theme/bootstrap/js/bootstrap.js')}}
        {{ HTML::script('theme/js/jquery.datetimepicker.js')}}
        {{ HTML::script('theme/tinymce/tinymce.min.js') }}
        {{ HTML::script('theme/js/main.js') }}
    </body>
</html>