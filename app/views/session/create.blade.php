{{ Form::open(array('url'=>'login', 'class'=>'form-signin')) }}
    <h2 class="form-signin-heading">Please Login</h2>
    
    <div class="form-group">
    	{{ Form::text('email', null, array('class'=>'input-block-level form-control', 'placeholder'=>'Email Address', 'required' => '')) }}
    </div>
    <div class="form-group">
    	{{ Form::password('password', array('class'=>'input-block-level form-control', 'placeholder'=>'Password', 'required' => '')) }}
    </div>
 
    {{ Form::submit('Login', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ Form::close() }}