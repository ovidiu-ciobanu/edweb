{{ Form::open(array('url'=>'register', 'class'=>'form-signup')) }}
    <h2 class="form-signup-heading">Please Register</h2>
    
    @if ( count($errors) > 0 )
	    <ul>
	        @foreach($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
    @endif
 	
 	<div class="form-group">
    	{{ Form::text('firstname', null, array('class'=>'input-block-level form-control', 'placeholder'=>'First Name', 'required' => '')) }}
    </div>
    
    <div class="form-group">
    	{{ Form::text('lastname', null, array('class'=>'input-block-level form-control', 'placeholder'=>'Last Name', 'required' => '')) }}
    </div>
    
    <div class="form-group">
    	{{ Form::text('email', null, array('class'=>'input-block-level form-control', 'placeholder'=>'Email Address', 'required' => '')) }}
    </div>
    
    <div class="form-group">
    	{{ Form::password('password', array('class'=>'input-block-level form-control', 'placeholder'=>'Password', 'required' => '')) }}
    </div>
    
    <div class="form-group">
    	{{ Form::password('password_confirmation', array('class'=>'input-block-level form-control', 'placeholder'=>'Confirm Password', 'required' => '')) }}
    </div>
 
    {{ Form::submit('Register', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ Form::close() }}