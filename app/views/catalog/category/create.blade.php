<div class="row left-tabbed-content">

    <?php if ( count( $errors ) > 0 ) { ?>
        <div class="container-fluid">
            <ul class="errors">
                <?php foreach ( $errors->all() as $error ) { ?>
                    <li class="alert alert-warning">{{ $error }}</li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <div class="col-md-2 side-col no-padding-right">
        <h3>Category Information</h3>
        <ul class="tabs">
            <li>
                <a id="product_attribute_tabs_main" class="active" href="#">
                    <span>
                        Settings
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-10 main-col">

        {{ Form::open( [ 'url'=>'catalog/category', 'class' => 'form form-horizontal' ] ) }}

        <div class="main-col-inner">
            <div class="header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>New category</h3>
                    </div>
                    <div class="col-md-6">

                        <ul class="pull-right list-inline">
                            <li>
                                {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                            </li>
                            <li>
                                {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="content">

                <div id="product_attribute_tabs_main_content">
                    <div class="header-content"><span class="fieldset-legend">Category settings</span></div>
                    <div class="entry-content">

                        <div class="form-group form-group-sm">
                            <label for="inputName" class="col-sm-2 control-label">Attribute Set <span class="required">*</span></label>
                            <div class="col-sm-5">
                                {{ Form::select('attribute[set]', $attribute_sets, null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        {{ Form::close() }}

    </div>

</div>