<?php
$section_products = Input::has( 'section' ) && Input::get( 'section' ) == 'products';
?>
<div class="row left-tabbed-content">

    <?php if ( count( $errors ) > 0 ) { ?>
        <div class="container-fluid">
            <ul class="errors">
                <?php foreach ( $errors->all() as $error ) { ?>
                    <li class="alert alert-warning"><?php echo $error; ?></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <div class="col-md-2 side-col no-padding-right">
        <h3>Attribute Information</h3>
        <ul class="tabs">
            <?php
            $first = true;
            foreach ( $category->attribute_set->groups as $group ) {
                ?>
                <li>
                    <a id="category_tabs_<?php echo strtolower( str_replace( ' ', '_', $group->attribute_group_name ) ); ?>" class="<?php
                    echo ( $first == true && !$section_products ) ? 'active' : '';
                    $first = false;
                    ?>" href="#">
                        <span>
                            <?php echo $group->attribute_group_name; ?>
                        </span>
                    </a>
                </li>
            <?php } ?>

            <li>
                <a id="category_tabs_products" class="<?php echo $section_products ? 'active' : ''; ?>" href="#">
                    <span>
                        Products
                    </span>
                </a>
            </li>
            <li>
                <a id="category_tabs_specials" href="#">
                    <span>
                        Specials
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-10 main-col">

        {{ Form::open( [ 'url' => [ 'catalog/category', $category->entity_id ], 'method' => 'put', 'class' => 'form form-horizontal' ] ) }}

        <div class="main-col-inner">
            <div class="header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Edit category</h3>
                    </div>
                    <div class="col-md-6">

                        <ul class="pull-right list-inline">
                            <li>
                                {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                            </li>
                            <li>
                                {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="content">

                <?php
                $first = true;
                foreach ( $category->attribute_set->groups as $group ) {
                    ?>

                    <div id="category_tabs_<?php echo strtolower( str_replace( ' ', '_', $group->attribute_group_name ) ); ?>_content" style="<?php
                    echo ( $first == true && !$section_products ) ? 'display: block;' : 'display: none;';
                    $first = false;
                    ?>">
                        <div class="header-content"><span class="fieldset-legend"><?php echo $group->attribute_group_name; ?></span></div>
                        <div class="entry-content">

                            <?php foreach ( $group->attributes as $attribute ) { ?>
                                @include( 'eav.attribute.type.' . $attribute->attribute->frontend_input, [ 'attribute' => $attribute->attribute, 'entity_id' => $category->entity_id ] )
                            <?php } ?>

                        </div>
                    </div>

                <?php } ?>

                <div id="category_tabs_products_content" style="<?php echo $section_products ? 'display: block;' : 'display: none;'; ?>">

                    <div class="header-content"><span class="fieldset-legend">Products</span></div>
                    <div class="entry-content">

                        <div class="" id="filters">
                            <div class="col-sm-2">
                                {{ Form::select(null, $select_options, Input::get('option'), [ 'class' => 'form-control' ] ) }}
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" />
                            </div>

                            <button type="button" id="filter_submit_button" class="btn btn-primary">Filter</button>
                        </div>

                        <hr />

                        <table class="table table-hover table-striped" id="products">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="check_all" /></th>
                                    <th>ID</th>
                                    <th>SKU</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $category_products = $category->products->lists( 'entity_id' ); ?>
                                <?php foreach ( $products as $product ) { ?>
                                    <tr>
                                        <td><input type="checkbox" value="<?php echo $product->entity_id; ?>" <?php echo in_array( $product->entity_id, $category_products ) ? 'checked="checked"' : ''; ?> class="product_id" /></td>
                                        <td><?php echo $product->entity_id; ?></td>
                                        <td><?php echo $product->attributeSku()->value; ?></td>
                                        <td><?php echo $product->attributeProductName()->value; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                        <?php
                        $appends = [ 'section' => 'products' ];
                        if ( Input::has( 'option' ) ) {
                            $appends['option'] = Input::get( 'option' );
                        }
                        if ( Input::has( 'search' ) ) {
                            $appends['search'] = Input::get( 'search' );
                        }
                        echo $products->appends( $appends )->links();
                        ?>

                    </div>

                </div>
                
                <div id="category_tabs_specials_content" style="display: none;">
                    <div class="header-content"><span class="fieldset-legend">Specials</span></div>
                    <div class="entry-content">

                        <div class="form-group form-group-sm">
                            <label for="input_profit_percent" class="col-sm-2 control-label">
                                Profit %
                            </label>
                            <div class="col-sm-5">
                                {{ Form::text('specials[profit_percent]', ($category->special == null ? '0' : $category->special->value), [ 'class' => 'input-block-level form-control', 'placeholder' => 'Profit ( in % )', 'id' => 'input_profit_percent' ] ) }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        {{ Form::close() }}

    </div>

    <script type="text/javascript">
        (function ($) {
            $(function () {

                // Correct tab display
                $('ul.tabs li a').on('click', function (e) {

                    $('ul.tabs li a').removeClass('active');
                    $(this).addClass('active');

                    var id = $(this).attr('id');
                    $('.main-col-inner .content > div').css('display', 'none');
                    $('#' + id + '_content', '.main-col-inner .content').css('display', 'block');

                    e.preventDefault();
                });

                // Filter submit
                $('#filter_submit_button').on('click', function () {
                    var select_option_value = $('#filters select').val();
                    var input_search = $('#filters input').val();

                    var section = 'products';

                    var href = window.location.href.split('?')[0];
                    href += '?section=' + section;
                    if (select_option_value !== '' && select_option_value > 0) {
                        href += '&option=' + select_option_value;
                    }
                    if (input_search !== '') {
                        href += '&search=' + input_search;
                    }

                    window.location.href = href;
                });

                // Check all product checkboxes
                var add_products_to_category = 1;
                $('#products .check_all').on('change', function () {
                    add_products_to_category = $('#products input.product_id').length;
                    $('#products input.product_id').click();
                });

                $('#products input.product_id').on('change', function () {
                    $('#overlay').css('visibility', 'visible');

                    var product_id = $(this).val();
                    var checked = 0;

                    if ($(this).prop('checked')) {
                        checked = 1;
                    }

                    $.ajax({
                        type: "POST",
                        url: "{{ url( 'ajax/catalog/category', [ 'id' => $category->entity_id ] ) }}",
                        headers: {'X-CSRF-Token': $('meta[name="_token"]').attr('content')},
                        data: {_method: 'PUT', product_id: product_id, checked: checked}
                    }).done(function (msg) {
                        if (add_products_to_category === 1) {
                            $('#overlay').css('visibility', 'hidden');
                        } else {
                            add_products_to_category--;
                        }
                    });
                });

            });
        })(jQuery);
    </script>
</div>