<div class="row left-tabbed-content">

    <?php if ( count( $errors ) > 0 ) { ?>
        <div class="container-fluid">
            <ul class="errors">
                <?php foreach ( $errors->all() as $error ) { ?>
                    <li class="alert alert-warning"><?php echo $error; ?></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <div class="col-md-2 side-col no-padding-right">
        <h3>Attribute Information</h3>
        <ul class="tabs">
            <?php
            $first = true;
            foreach ( $product->attribute_set->groups as $group ) {
                ?>
                <li>
                    <a id="product_tabs_<?php echo strtolower( str_replace( ' ', '_', $group->attribute_group_name ) ); ?>" class="<?php
                    echo ( $first == true ) ? 'active' : '';
                    $first = false;
                    ?>" href="#">
                        <span>
                            <?php echo $group->attribute_group_name; ?>
                        </span>
                    </a>
                </li>
            <?php } ?>

            <li>
                <a id="product_tabs_categories" href="#">
                    <span>
                        Categories
                    </span>
                </a>
            </li>
            <li>
                <a id="product_tabs_specials" href="#">
                    <span>
                        Specials
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-10 main-col">

        {{ Form::open( [ 'url' => [ 'catalog/product', $product->entity_id ], 'method' => 'put', 'class' => 'form form-horizontal' ] ) }}

        <div class="main-col-inner">
            <div class="header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Edit product attribute</h3>
                    </div>
                    <div class="col-md-6">

                        <ul class="pull-right list-inline">
                            <li>
                                {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                            </li>
                            <li>
                                {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="content">

                <?php
                $first = true;
                foreach ( $product->attribute_set->groups as $group ) {
                    ?>

                    <div id="product_tabs_<?php echo strtolower( str_replace( ' ', '_', $group->attribute_group_name ) ); ?>_content" style="<?php
                    echo ( $first == true ) ? 'display: block;' : 'display: none;';
                    $first = false;
                    ?>">
                        <div class="header-content"><span class="fieldset-legend"><?php echo $group->attribute_group_name; ?></span></div>
                        <div class="entry-content">

                            <?php foreach ( $group->attributes as $attribute ) { ?>
                                @include( 'eav.attribute.type.' . $attribute->attribute->frontend_input, [ 'attribute' => $attribute->attribute, 'entity_id' => $product->entity_id ] )
                            <?php } ?>

                        </div>
                    </div>

                <?php } ?>

                <div id="product_tabs_categories_content" style="display: none;">
                    <div class="header-content"><span class="fieldset-legend">Categories</span></div>
                    <div class="entry-content">

                        <?php
                        $current_categories = $product->categories->lists( 'entity_id' );
                        foreach ( $categories as $category ) {
                            ?>
                            <?php if ( in_array( $category->entity_id, $current_categories ) ) { ?>
                                <input type="checkbox" name="categories[]" value="<?php echo $category->entity_id ?>" checked="checked" /> <?php echo $category->attributeCategoryName()->value; ?>
                            <?php } else { ?>
                                <input type="checkbox" name="categories[]" value="<?php echo $category->entity_id ?>" /> <?php echo $category->attributeCategoryName()->value; ?>
                            <?php } ?>
                        <?php } ?>

                    </div>
                </div>

                <div id="product_tabs_specials_content" style="display: none;">
                    <div class="header-content"><span class="fieldset-legend">Specials</span></div>
                    <div class="entry-content">

                        <div class="form-group form-group-sm">
                            <label for="input_profit_percent" class="col-sm-2 control-label">
                                Profit %
                            </label>
                            <div class="col-sm-5">
                                {{ Form::text('specials[profit_percent]', ($product->special == null ? '0' : $product->special->value), [ 'class' => 'input-block-level form-control', 'placeholder' => 'Profit ( in % )', 'id' => 'input_profit_percent' ] ) }}
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

        {{ Form::close() }}

    </div>

    <script type="text/javascript">
        (function ( $ ) {
            $( function () {

                $( 'ul.tabs li a' ).on( 'click', function ( e ) {

                    $( 'ul.tabs li a' ).removeClass( 'active' );
                    $( this ).addClass( 'active' );

                    var id = $( this ).attr( 'id' );
                    $( '.main-col-inner .content > div' ).css( 'display', 'none' );
                    $( '#' + id + '_content', '.main-col-inner .content' ).css( 'display', 'block' );

                    e.preventDefault();
                } );

            } );
        })( jQuery );
    </script>
</div>