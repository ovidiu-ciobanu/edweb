<div class="row">
    <div class="col-md-6">
        Manage products
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                <a href="{{ url('export/products/magento') }}" target="_blank">
                    <button type="button" class="btn btn-primary btn-sm">Export products to Magento</button>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('catalog.product.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Add new product</button>
                </a>
            </li>
             <li>
                <a href="{{ url('catalog/product/destroyall') }}"  onclick="return confirm( 'Are you sure?' )">
                    <button type="button" class="btn btn-danger btn-sm">Delete ALL</button>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row filters pad-bot-5">
    <div class="col-sm-2">
        {{ Form::select(null, $search_options, Input::get('option', null), [ 'class' => 'form-control' ]) }}

    </div>
    <div class="col-sm-2">
        {{ Form::text(null, Input::get('search', null), [ 'class' => 'form-control' ]) }}
    </div>
    <div class="col-sm-2">
        <button id="filter_submit_button" class="btn btn-primary">Search</button>
        <a href="{{ URL::route('catalog.product.index') }}" class="pull-right">
            <button type="button" class="btn btn-default btn-danger">Reset</button>
        </a>
    </div>
</div>

<div class="row pad-bot-5">
    <div class="col-sm-2">
        {{ Form::select(null, [ 'profit_percent' => 'Profit %' ], Input::get('option', null), [ 'class' => 'form-control' ]) }}
    </div>
    <div class="col-md-2">
        {{ Form::text(null, Input::get('profit_percent', null), [ 'class' => 'form-control', 'id' => 'profit_percent_input' ]) }}
    </div>
    <div class="col-sm-1">
        <button id="profit_percent_submit" class="btn btn-primary">Save</button>
    </div>
</div>

<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    <input type="checkbox" class="checkbox_table_select_all" />
                </th>
                <th>
                    ID
                </th>
                <th>
                    Name
                </th>
                <th>
                    SKU
                </th>
                <th>
                    Profit %
                </th>
                <th>
                    Attribute Set Name
                </th>
                <th>
                    Action
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $products as $product ) { ?>
                <tr>
                    <td>
                        <input type='checkbox' class="checkbox" value="<?php echo $product->entity_id; ?>" />
                    </td>
                    <td>
                        <?php echo $product->entity_id; ?>
                    </td>
                    <td>
                        <?php echo $product->attributeProductName()->value; ?>
                    </td>
                    <td>
                        <?php echo $product->attributeSku()->value; ?>
                    </td>
                    <td>
                        <?php echo ( $product->special !== null ? $product->special->value : '' ); ?>
                    </td>
                    <td>
                        <?php echo $product->attribute_set->attribute_set_name; ?>
                    </td>
                    <td>
                        <a href="{{ URL::route( 'catalog.product.edit', $product->entity_id ) }}">
                            <button type="button" class="btn btn-default btn-sm">Edit</button>
                        </a>
                        {{ Form::open( [ 'route' => [ 'catalog.product.destroy', $product->entity_id ], 'method' => 'delete', 'class' => 'pull-right' ] ) }}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm( 'Are you sure?' )">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <div class="text-center">
        <?php
        $appends = [ ];
        if ( Input::has( 'option' ) && Input::has( 'search' ) ) {
            $appends['option'] = Input::get( 'option' );
            $appends['search'] = Input::get( 'search' );
        }
        echo $products->appends( $appends )->links();
        ?>
    </div>

</div>

<script type="text/javascript">
    (function ( $ ) {
        $( function () {
            // Filter submit
            $( '#filter_submit_button' ).on( 'click', function () {
                var select_option_value = $( '.filters select' ).val();
                var input_search = $( '.filters input' ).val();

                var href = window.location.href.split( '?' )[0];
                if (select_option_value !== '' && input_search !== '') {
                    href += '?option=' + select_option_value;
                    href += '&search=' + input_search;
                }

                window.location.href = href;
            } );

            // Submit the profit percent
            $( '#profit_percent_submit' ).on( 'click', function () {
                var profit_percent = parseFloat( $( '#profit_percent_input' ).val() );

                if (isNaN( profit_percent )) {
                    alert( 'Please specify the "Profit %"' );
                    $( '#profit_percent_input' ).focus();
                    return false;
                }

                var data = {
                    profit_percent: profit_percent
                };

                if ($( '.checkbox_table_select_all' ).hasClass( 'all' )) {
                    data.all = 1;
                } else {

                    var products = [];
                    $( '.checkbox' ).each( function () {
                        if (this.checked) {
                            products.push( $( this ).val() );
                        }
                    } );

                    if (products.length > 0) {
                        data.products = products;
                    }

                }

                $.ajax( {
                    type: "POST",
                    url: "{{ url( 'ajax/catalog/product/profit/percent' ) }}",
                    headers: {'X-CSRF-Token': $( 'meta[name="_token"]' ).attr( 'content' )},
                    data: data
                } ).done( function ( response ) {
                    window.location.reload( true );
                } );
            } );
        } );
    })( jQuery );
</script>
