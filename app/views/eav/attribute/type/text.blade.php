<?php
$attributes = [
    'class' => 'input-block-level form-control',
    'placeholder' => $attribute->label->value,
    'id' => 'input_' . strtolower( str_replace( ' ', '_', $attribute->label->value ) )
];

if ( $attribute->is_required ) {
    $attributes[ 'required' ] = 'required';
}

?>

<div class="form-group form-group-sm">
    <label for="input_<?php echo strtolower( str_replace( ' ', '_', $attribute->label->value ) ); ?>" class="col-sm-2 control-label">
        <?php echo $attribute->label->value; ?> 
        <?php if ( $attribute->is_required ) { ?>
            <span class="required">*</span>
        <?php } ?>
    </label>
    <div class="col-sm-5">
        {{ Form::text('eav[attribute][' . $attribute->attribute_id . ']', $attribute->entity( $entity_id )->first()->value, $attributes ) }}
    </div>
</div>