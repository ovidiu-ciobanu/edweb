<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Order #</th>
                <th>Bill to name</th>
                <th>Grand total</th>
                <th>Base grand total</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders as $order) { ?>
                <tr>
                    <td><?php echo $order->increment_id; ?></td>
                    <td><?php echo $order->billing_firstname; ?> <?php echo $order->billing_lastname; ?></td>
                    <td><?php echo $order->base_grand_total; ?></td>
                    <td><?php echo $order->grand_total; ?></td>
                    <td><?php echo ucfirst($order->status); ?></td>
                    <td>
                        <a href="{{ URL::route( 'sales.order.show', $order->order_id ) }}">
                            <button type="button" class="btn btn-default btn-sm">View</button>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>