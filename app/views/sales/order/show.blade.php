
<div class="content">

    <div class="row">
        <div class="col-md-6">
            <div class="header-content">
                Order # <?php echo $order->increment_id; ?>
            </div>
            <div class="entry-content">

                <div class="v-bot-5">
                    <div class="col-md-4">
                        Order Date
                    </div>
                    <div class="col-md-8">
                        <strong><?php echo date( 'M d, Y H:i:s', strtotime( $order->created_at ) ); ?></strong>
                    </div>
                </div>

                <div class="v-bot-5">
                    <div class="col-md-4">
                        Order Status
                    </div>
                    <div class="col-md-8">
                        <strong><?php echo ucfirst( $order->status ); ?></strong>
                    </div>
                </div>

                <div class="v-bot-5">
                    <div class="col-md-4">
                        Placed from IP
                    </div>
                    <div class="col-md-8">
                        <strong><?php echo $order->remote_ip; ?></strong>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-6">
            <div class="header-content">
                Account Information
            </div>
            <div class="entry-content">
                <div class="v-bot-5">
                    <div class="col-md-4">
                        Customer name
                    </div>
                    <div class="col-md-8">
                        <strong><?php echo $order->customer_firstname; ?> <?php echo $order->customer_lastname; ?></strong>
                    </div>
                </div>

                <div class="v-bot-5">
                    <div class="col-md-4">
                        E-mail
                    </div>
                    <div class="col-md-8">
                        <strong><a href="mailto: <?php echo $order->customer_email; ?>"><?php echo $order->customer_email; ?></a></strong>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php $billing = $order->billing; ?>
    <div class="row">
        <div class="col-md-6">
            <div class="header-content">
                Billing Address
            </div>
            <div class="entry-content">

                <div class="v-bot-5">
                    <?php echo $billing->firstname ?> <?php echo $billing->lastname; ?>
                </div>
                <div class="v-bot-5">
                    <?php echo $billing->street ?>
                </div>
                <div class="v-bot-5">
                    <?php echo $billing->city ?>, <?php echo $billing->region ?>, <?php echo $billing->postcode ?>
                </div>
                <div class="v-bot-5">
                    <?php echo $billing->country_id ?>
                </div>
                <div class="v-bot-5">
                    Phone: <?php echo $billing->telephone ?>
                </div>

            </div>
        </div>

        <?php $shipping = $order->shipping; ?>
        <div class="col-md-6">
            <div class="header-content">
                Shipping Address
            </div>
            <div class="entry-content">

                <div class="v-bot-5">
                    <?php echo $shipping->firstname ?> <?php echo $shipping->lastname; ?>
                </div>
                <div class="v-bot-5">
                    <?php echo $shipping->street ?>
                </div>
                <div class="v-bot-5">
                    <?php echo $shipping->city ?>, <?php echo $shipping->region ?>, <?php echo $shipping->postcode ?>
                </div>
                <div class="v-bot-5">
                    <?php echo $shipping->country_id ?>
                </div>
                <div class="v-bot-5">
                    Phone: <?php echo $shipping->telephone ?>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="header-content">
                Items Ordered
            </div>
            <div class="entry-content">

                <table class="table table-hover table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>
                                Product
                            </th>
                            <th class="text-center">
                                SKU
                            </th>
                            <th class="text-center">
                                Original Price
                            </th>
                            <th class="text-center">
                                Price
                            </th>
                            <th class="text-center">
                                Qty
                            </th>
                            <th class="text-center">
                                Subtotal
                            </th>
                            <th class="text-center">
                                Tax Amount
                            </th>
                            <th class="text-center">
                                Tax Percent
                            </th>
                            <th class="text-center">
                                Discount Amount
                            </th>
                            <th class="text-center">
                                Row Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ( $order->items as $item ) { ?>
                            <tr>
                                <td>
                                    <?php echo $item->name; ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $item->sku; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo currency_convert_price( $item->original_price, $order->order_currency_code ); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo currency_convert_price( $item->price, $order->order_currency_code ); ?>
                                </td>
                                <td class="text-center">
                                    <?php echo (int) $item->qty_ordered; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo currency_convert_price( $item->price * $item->qty_ordered, $order->order_currency_code ); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo currency_convert_price( $item->tax_amount, $order->order_currency_code ); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo number_format( $item->tax_percent, 2 ); ?> %
                                </td>
                                <td class="text-right">
                                    <?php echo currency_convert_price( $item->discount_amount, $order->order_currency_code ); ?>
                                </td>
                                <td class="text-right">
                                    <?php echo currency_convert_price( $item->original_price * $item->qty_ordered, $order->order_currency_code ); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="header-content">
                Payment Information
            </div>
            <div class="entry-content">

                <div class="v-bot-5">
                    Order was placed using <?php echo $order->order_currency_code; ?>
                </div>

            </div>
        </div>

        <div class="col-md-6">
            <div class="header-content">
                Order Totals
            </div>
            <div class="entry-content">

                <div class="v-bot-5">

                    <div class="col-md-10 text-right">
                        Subtotal
                    </div>
                    <div class="col-md-2 text-right">
                        <?php echo currency_convert_price( $order->subtotal, $order->order_currency_code ); ?>
                    </div>

                </div>

                <div class="v-bot-5">

                    <div class="col-md-10 text-right">
                        Shipping & Handling <?php echo ( $order->shipping_description ) ? '( ' . $order->shipping_description . ' )' : ''; ?>
                    </div>
                    <div class="col-md-2 text-right">
                        <?php echo currency_convert_price( $order->shipping_amount, $order->order_currency_code ); ?>
                    </div>

                </div>

                <div class="v-bot-5">

                    <div class="col-md-10 text-right">
                        Grand Total
                    </div>
                    <div class="col-md-2 text-right">
                        <?php echo currency_convert_price( $order->grand_total, $order->order_currency_code ); ?>
                    </div>

                </div>

                <div class="v-bot-5">

                    <div class="col-md-10 text-right">
                        Total Paid
                    </div>
                    <div class="col-md-2 text-right">
                        <?php echo currency_convert_price( $order->total_paid, $order->order_currency_code ); ?>
                    </div>

                </div>

                <div class="v-bot-5">

                    <div class="col-md-10 text-right">
                        Total Refunded
                    </div>
                    <div class="col-md-2 text-right">
                        <?php echo currency_convert_price( $order->total_refunded, $order->order_currency_code ); ?>
                    </div>

                </div>

            </div>
        </div>

    </div>

</div>
