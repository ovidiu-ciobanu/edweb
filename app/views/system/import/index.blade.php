<div class="row">
    <div class="col-md-6">
        Manage imports
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                <a href="{{ URL::route('system.import.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Add new import</button>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    Path
                </th>
                <th>
                    Action
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $imports as $import ) { ?>
                <tr>
                    <td>
                        <?php echo $import->path; ?>
                    </td>
                    <td>
                        <?php if ($active_import) {
                            if ($active_import->params->import_id==$import->import_id) { 
                                $total = ($active_import->total)? $active_import->total : 100; ?>
                        <b class="progress-staus">{{$active_import->status}}</b> 
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" 
                                 aria-valuemax="100" style="width: {{($active_import->step*100)/$total}}%;">
                              <span class="sr-only">{{($active_import->step*100)/$total}}% Complete</span>
                            </div>
                          </div>
                        <script>
                            function update_progress_bar(){
                                $.ajax({
                                    url: "{{url('system/import/getimportstep')}}",
                                    success: function(data) {
                                        var active_import = JSON.parse(data);
                                        var precentage = (active_import.step*100)/active_import.total
                                        $('.progress-bar').width(precentage+'%');
                                        $('.progress-staus').html(active_import.status);
                                        if (active_import.total>active_import.step){
                                            setTimeout(function(){
                                                update_progress_bar()
                                            },4000);
                                        }
                                    }
                                });
                            }
                            update_progress_bar();
                        </script>
                            <?php }
                        } else {  ?>
                        <?php if ( null !== $import->profile->system_import_profile_attribute_set_group_id ) { ?>
                            <a href="{{ url( 'system/import/execute', $import->import_id ) }}">
                                <button type="button" class="btn btn-default btn-sm">Import</button>
                            </a>
                        <?php } ?>
                        {{ Form::open( [ 'route' => [ 'system.import.destroy', $import->import_id ], 'method' => 'delete', 'class' => 'pull-right' ] ) }}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm( 'Are you sure?' )">Delete</button>
                        {{ Form::close() }}
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

</div>