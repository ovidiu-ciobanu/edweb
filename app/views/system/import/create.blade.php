
{{ Form::open( [ 'url'=>'system/import', 'class' => 'form form-horizontal', 'files' => true ] ) }}
<h2 class="form-heading">New import</h2>

@if ( count($errors) > 0 )
<ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>
@endif

<div class="form-group">
    <label for="inputName" class="col-sm-2 control-label">File <span class="required">*</span></label>
    <div class="col-sm-5">
        {{ Form::file('import', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
    </div>
</div>

<div class="form-group">
    <label for="profile" class="col-sm-2 control-label">Profile</label>
    <div class="col-sm-5">
        {{ Form::select('profile', [ 0 => 'Create new' ] + $profiles, 0, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'profile' ] ) }}
    </div>
</div>

<div class="form-group" id="clone_profile_group" style="display: none;">
    <label for="clone_profile" class="col-sm-2 control-label">Clone profile</label>
    <div class="col-sm-5">
        {{ Form::checkbox('clone_profile', 'clone', false, [ 'class'=>'input-block-level form-control', 'id' => 'clone_profile' ] ) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-2">
        {{ Form::submit('Save', array('class'=>'btn btn-large btn-primary btn-block'))}}
    </div>
</div>
{{ Form::close() }}

<script type="text/javascript">
(function( $ ){
    $( function() {
        $( '#profile' ).on( 'change', function( e ) {
            if ( parseInt( $( this ).val() ) !== 0 ) {
                $( '#clone_profile_group' ).css( 'display', 'block' );
            } else {
                $( '#clone_profile_group' ).css( 'display', 'none' );
            }
        });
    });
})( jQuery );
</script>
