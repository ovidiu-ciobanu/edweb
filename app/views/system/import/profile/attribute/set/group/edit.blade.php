{{ Form::open( [ 'url'=>[ 'system/import/profile/attribute/set/group', $import_profile_attribute_set_group->system_import_profile_attribute_set_group_id ], 'method' => 'put', 'class' => 'form form-horizontal' ] ) }}

<div class="row">
    <div class="header">
        <div class="col-md-6">
            <h3>Edit profile attribute set group</h3>
        </div>
        <div class="col-md-6">

            <ul class="pull-right list-inline">
                <li>
                    {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                </li>
                <li>
                    {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                </li>
            </ul>

        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-3">

        <div class="content">
            <div class="header-content">
                Set Name
            </div>
            <div class="entry-content">
                {{ Form::text('name', $import_profile_attribute_set_group->name, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => 'required' ] ) }}
            </div>
        </div>

    </div>
    <div class="col-md-3">

        <div class="content">
            <div class="header-content">
                Attribute sets
            </div>
            <div class="entry-content">

                <ul>
                    <?php
                    $attribute_set_ids = $import_profile_attribute_set_group->sets->lists( 'attribute_set_id' );
                    foreach ( $attribute_sets as $attribute_set ) {
                        ?>
                        <li>
                            <label>{{ Form::checkbox('attribute_sets[]', $attribute_set->attribute_set_id, in_array( $attribute_set->attribute_set_id, $attribute_set_ids ) ) }} <?php echo $attribute_set->attribute_set_name; ?></label>
                        </li>
                    <?php } ?>
                </ul>

            </div>
        </div>

    </div>

</div>

{{ Form::close() }}