<div class="row">
    <div class="col-md-6">
        Manage import attribute sets groups
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                <a href="{{ URL::route('system.import.profile.attribute.set.group.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Add new attribute set group</button>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Action
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $import_profile_attribute_set_groups as $import_profile_attribute_set_group ) { ?>
                <tr>
                    <td>
                        <?php echo $import_profile_attribute_set_group->name; ?>
                    </td>
                    <td>
                        <a href="{{ URL::route( 'system.import.profile.attribute.set.group.edit', $import_profile_attribute_set_group->system_import_profile_attribute_set_group_id ) }}">
                            <button type="button" class="btn btn-default btn-sm">Edit</button>
                        </a>
                        {{ Form::open( [ 'route' => [ 'system.import.profile.attribute.set.group.destroy', $import_profile_attribute_set_group->system_import_profile_attribute_set_group_id ], 'method' => 'delete', 'class' => 'pull-right' ] ) }}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm( 'Are you sure?' )">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

</div>