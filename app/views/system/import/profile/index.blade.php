<div class="row">
    <div class="col-md-6">
        Manage import profile
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Action
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $profiles as $profile ) { ?>
            <tr>
                <td>
                    <?php echo $profile->name; ?>
                </td>
                <td>
                    <a href="{{ URL::route( 'system.import.profile.edit', $profile->import_profile_id ) }}">
                        <button type="button" class="btn btn-default btn-sm">Edit</button>
                    </a>
                    {{ Form::open( [ 'route' => [ 'system.import.profile.destroy', $profile->import_profile_id ], 'method' => 'delete', 'class' => 'pull-right' ] ) }}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                    {{ Form::close() }}
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

</div>
