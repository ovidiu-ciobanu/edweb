<?php
$attribute_select = [ ];
foreach ( $attribute_sets as $attribute_set ) {
    $attribute_group_attributes = $attribute_set->groups_attributes;
    foreach ( $attribute_group_attributes as $attribute_group_attribute ) {
        $attribute = $attribute_group_attribute->attribute;
        $attribute_select[$attribute->attribute_id] = $attribute_set->attribute_set_name . ' - ' . $attribute->label->value;
    }
}

$import_keys_select = [ ];
foreach ( $import_keys as $import_key ) {
    $import_keys_select[$import_key] = $import_key;
}
$row_html = '';
?>

{{ Form::open( [ 'url' => [ 'system/import/profile', $import_profile->import_profile_id ], 'method' => 'put', 'class' => 'form form-horizontal' ] ) }}

<div class="form-group">
    <label for="inputName" class="col-sm-2 control-label">Name <span class="required">*</span></label>
    <div class="col-sm-3">
        {{ Form::text('profile[name]', $import_profile->name, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
    </div>
</div>

<div class="form-group">
    <label for="attribute_sets_select" class="col-sm-2 control-label">Profile attribute set group <span class="required">*</span></label>
    <div class="col-sm-3">
        {{ Form::select('', [ 0 => 'Please select'] + $profile_attribute_set_groups, (int)$import_profile->system_import_profile_attribute_set_group_id,[ 'class'=>'input-block-level form-control', 'required' => '', 'id' => 'profile_attribute_set_groups_select' ] ) }}
    </div>
</div>

<?php if ( $import_profile->system_import_profile_attribute_set_group_id ) { ?>
    <div class="row">
        <table class="table table-hover table-striped" id="attributes_table">
            <thead>
                <tr>
                    <th>
                        Attributes
                    </th>
                    <th>
                        Keys
                    </th>
                    <th>
                        Default value
                    </th>
                </tr>
            </thead>

            <tbody>
                <?php
                $import_profile_keys = $import_profile->attributes;

                foreach ( $import_profile_keys as $profile_key ) {
                    ?>
                    <tr>
                        <td>
                            {{ Form::select('profile[attribute][]', [ 0 => '' ] + $attribute_select, (int)$profile_key->attribute_id, [ 'class'=>'input-block-level form-control' ] ) }}
                        </td>
                        <td>
                            {{ Form::select('profile[key][]', [ '' ] + $import_keys_select, $profile_key->key, [ 'class'=>'input-block-level form-control' ] ) }}
                        </td>
                        <td>
                            {{ Form::text('profile[default_value][]', $profile_key->default_value, [ 'class' => 'input-block-level form-control' ] ) }}
                        </td>
                    </tr>

                    <?php
                }

                $row_html = '
                    <tr>
                        <td>
                            <select class="input-block-level form-control" name="profile[attribute][]">
                                <option value="0"></option>';
                foreach ( $attribute_select as $key => $value ) {
                    $row_html .= '<option value="' . $key . '">' . $value . '</option>';
                }
                $row_html .= '</select>
                        </td>
                        <td>
                            <select class="input-block-level form-control" name="profile[key][]">
                                <option value="0"></option>';
                foreach ( $import_keys_select as $key => $attribute ) {
                    $row_html .= '<option value="' . $key . '">' . $attribute . '</option>';
                }
                $row_html .= '</td>
                        <td>
                            <input class="input-block-level form-control" type="text" name="profile[default_value][]">
                        </td>
                    </tr>';

                echo $row_html;
                ?>

            </tbody>
        </table>
    </div>
<?php } ?>

<div class="form-group">
    <div class="col-sm-offset-1 col-sm-1">
        {{ Form::submit('Save', array('class'=>'btn btn-large btn-primary btn-block'))}}
    </div>

    <?php if ( $import_profile->system_import_profile_attribute_set_group_id ) { ?>
        <div class="col-sm-offset-1 col-sm-2">
            {{ Form::button('Add new attribute', array('class'=>'btn btn-large btn-primary btn-block', 'id' => 'add_new_attribute_btn'))}}
        </div>
    <?php } ?>
</div>

{{ Form::close() }}

<script type="text/javascript">
    (function ( $ ) {
        $( function () {

            $( '#profile_attribute_set_groups_select' ).on( 'change', function ( e ) {

                var value = $( this ).val();
                if (value <= 0) {
                    return false;
                }

                if (confirm( 'Data will be lost. Are you sure?' )) {
                    $( '#overlay' ).css( 'visibility', 'visible' );

                    $.ajax( {
                        type: "POST",
                        url: "{{ url( 'system/import/profile', [ 'id' => $import_profile->import_profile_id ] ) }}",
                        headers: {'X-CSRF-Token': $( 'meta[name="_token"]' ).attr( 'content' )},
                        data: {_method: "PUT", profile_attribute_set_group: value}
                    } ).done( function ( msg ) {
                        $( 'table select' ).val( '0' );
                        window.location.reload();
                    } );
                }

            } );

            $( '#add_new_attribute_btn' ).on( 'click', function () {
                var html = '<?php echo str_replace( [ "\n", "\r", "\n\r" ], '', $row_html ); ?>';
                $( '#attributes_table tbody' ).append( html );
            } );
        } );
    })( jQuery );
</script>
