<div class="row left-tabbed-content">

    <?php if ( count( $errors ) > 0 ) { ?>
        <div class="container-fluid">
            <ul class="errors">
                <?php foreach ( $errors->all() as $error ) { ?>
                    <li class="alert alert-warning"><?php echo $error; ?></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>

    <div class="col-md-2 side-col no-padding-right">
        <h3>User Information</h3>
        <ul class="tabs">
            <li>
                <a id="user_tabs_general" href="#" class="active">
                    <span>
                        General
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-10 main-col">
        {{ Form::open( [ 'url' => [ 'system/user', $user->id ],'method' => 'put', 'class' => 'form form-horizontal' ] ) }}

        <div class="main-col-inner">
            <div class="header">
                <div class="row">
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">

                        <ul class="pull-right list-inline">
                            <li>
                                {{ link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-sm']) }}
                            </li>
                            <li>
                                {{ Form::submit('Save', [ 'class'=>'btn btn-primary btn-sm' ] ) }}
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="content">

                <div id="user_tabs_general_content">
                    <div class="header-content"><span class="fieldset-legend">General</span></div>
                    <div class="entry-content">

                        <div class="form-group form-group-sm">
                            <label for="input_firstname" class="col-sm-2 control-label">
                                First name <span class="required">*</span>
                            </label>
                            <div class="col-sm-5">
                                {{ Form::text('firstname', $user->firstname, [ 'class' => 'input-block-level form-control', 'id' => 'input_firstname', 'placeholder' => 'First name' ] ) }}
                            </div>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="input_lastname" class="col-sm-2 control-label">
                                Last name <span class="required">*</span>
                            </label>
                            <div class="col-sm-5">
                                {{ Form::text('lastname', $user->lastname, [ 'class' => 'input-block-level form-control', 'id' => 'input_lastname', 'placeholder' => 'Last name' ] ) }}
                            </div>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="input_email" class="col-sm-2 control-label">
                                E-mail <span class="required">*</span>
                            </label>
                            <div class="col-sm-5">
                                {{ Form::text('email', $user->email, [ 'class' => 'input-block-level form-control', 'id' => 'input_email', 'placeholder' => 'E-mail' ] ) }}
                            </div>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="input_password" class="col-sm-2 control-label">
                                Password
                            </label>
                            <div class="col-sm-5">
                                {{ Form::password('password', [ 'class' => 'input-block-level form-control', 'id' => 'input_password', 'placeholder' => 'Password' ] ) }}
                            </div>
                        </div>

                        <div class="form-group form-group-sm">
                            <label for="input_password_confirmation" class="col-sm-2 control-label">
                                Password confirmation
                            </label>
                            <div class="col-sm-5">
                                {{ Form::password('password_confirmation', [ 'class' => 'input-block-level form-control', 'id' => 'input_password_confirmation', 'placeholder' => 'Password confirmation' ] ) }}
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function ( $ ) {
        $( function () {

            $( 'ul.tabs li a' ).on( 'click', function ( e ) {

                $( 'ul.tabs li a' ).removeClass( 'active' );
                $( this ).addClass( 'active' );

                var id = $( this ).attr( 'id' );
                $( '.main-col-inner .content > div' ).css( 'display', 'none' );
                $( '#' + id + '_content', '.main-col-inner .content' ).css( 'display', 'block' );

                e.preventDefault();
            } );

        } );
    })( jQuery );
</script>
