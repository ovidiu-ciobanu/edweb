
<div class="row">
    <div class="col-md-6">
        Manage stores
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                <a href="{{ URL::route('system.user.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Create user</button>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>

                </th>
                <th>
                    User name
                </th>
                <th>
                    E-mail
                </th>
                <th>
                    Action
                </th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ( $users as $user ) { ?>
                <tr>
                    <td>
                        <input type='checkbox' />
                    </td>
                    <td>
                        <?php echo $user->firstname; ?> <?php echo $user->lastname; ?>
                    </td>
                    <td>
                        <?php echo $user->email; ?>
                    </td>
                    <td>
                        <a href="{{ URL::route( 'system.user.edit', $user->id ) }}">
                            <button type="button" class="btn btn-default btn-sm">Edit</button>
                        </a>
                        {{ Form::open( [ 'route' => [ 'system.user.destroy', $user->id ], 'method' => 'delete', 'class' => 'pull-right' ] ) }}
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm( 'Are you sure?' )">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <div class="text-center">
        <?php echo $users->links(); ?>
    </div>

</div>
