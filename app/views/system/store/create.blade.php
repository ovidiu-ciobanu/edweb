
{{ Form::open( [ 'url'=>'system/store', 'class' => 'form form-horizontal' ] ) }}
    <h2 class="form-heading">New store</h2>
    
    @if ( count($errors) > 0 )
	    <ul>
	        @foreach($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
    @endif
 	
 	<div class="form-group">
 		<label for="inputName" class="col-sm-2 control-label">Website <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::select('website', $websites, Input::old('website'), [ 'class'=>'input-block-level form-control', 'required' => '', 'id' => 'inputCode' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
 		<label for="inputName" class="col-sm-2 control-label">Name <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::text('name', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
    	<div class="col-sm-offset-2 col-sm-2">
    		{{ Form::submit('Save store', array('class'=>'btn btn-large btn-primary btn-block'))}}
    	</div>
    </div>
{{ Form::close() }}
