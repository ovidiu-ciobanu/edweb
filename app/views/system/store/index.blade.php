
<div class="row">
    <div class="col-md-6">
        Manage stores
    </div>
    <div class="col-md-6">
        <ul class="pull-right list-inline">
            <li>
                <a href="{{ URL::route('system.store_website.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Create website</button>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('system.store.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Create store</button>
                </a>
            </li>
            <li>
                <a href="{{ URL::route('system.store_view.create') }}">
                    <button type="button" class="btn btn-default btn-sm">Create store view</button>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="stores">
    <div class="row heading">
        <div class="col-md-3">Website Name</div>
        <div class="col-md-6">Store Name</div>
        <div class="col-md-3">Store View Name</div>
    </div>
    <?php foreach ( $core_websites as $key => $core_website ) { ?>
        <div class="row <?php if( $key % 2 == 0 ) { ?> even <?php } ?>">
            <div class="col-md-3">

                <a href="{{ URL::route('system.store_website.edit', $core_website->website_id) }}">
                    {{ $core_website->name }}
                </a>
                <br />
                <span class="information">
                    ( Code: {{ $core_website->code }} )
                </span>
            </div>
            <div class="col-md-9 no-padding">
                <?php foreach ( $core_website->store_groups as $store_group ) { ?>
                    <div class="col-md-8">
                        <a href="{{ URL::route('system.store.edit', $store_group->group_id) }}">
                            {{ $store_group->name }}
                        </a>
                        <br />
                        <span class="information">
                            ( Root category: {{ $store_group->root_category_id }} )
                        </span>
                    </div>
                    <div class="col-md-4">
                        <ul>
                            <?php foreach ( $store_group->store_views as $store_view ) { ?>
                                <li>
                                    <a href="{{ URL::route('system.store_view.edit', $store_view->store_id) }}">
                                        {{ $store_view->name }}
                                    </a>
                                    <br />
                                    <span class="information">
                                        ( Code: {{ $store_view->code }} )
                                    </span>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</div>
