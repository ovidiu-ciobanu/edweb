
{{ Form::open( [ 'url' => [ 'system/store', $store_group->group_id ], 'method' => 'put', 'class' => 'form form-horizontal' ] ) }}
    <h2 class="form-heading">Edit store</h2>
    
    @if ( count($errors) > 0 )
	    <ul>
	        @foreach($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
    @endif
 	
 	<div class="form-group">
 		<label for="selectWebsite" class="col-sm-2 control-label">Website <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::select('website', $websites, $store_group->website_id, [ 'class'=>'input-block-level form-control', 'required' => '', 'id' => 'selectWebsite' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
 		<label for="inputName" class="col-sm-2 control-label">Name <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::text('name', $store_group->name, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
 		<label for="inputRootCategory" class="col-sm-2 control-label">Root category</label>
	    <div class="col-sm-5">
	    	! NOT AVAILABLE YET !
	    </div>
    </div>
    
    <div class="form-group">
 		<label for="selectDefaultStoreView" class="col-sm-2 control-label">Default store view</label>
	    <div class="col-sm-5">
	    	{{ Form::select('default_store', $store_group->store_views->lists( 'name', 'store_id' ), $store_group->default_store_id, [ 'class'=>'input-block-level form-control', 'id' => 'selectDefaultStoreView' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
    	<div class="col-sm-offset-2 col-sm-2">
    		{{ Form::submit('Save store', array('class'=>'btn btn-large btn-primary btn-block'))}}
    	</div>
    </div>
{{ Form::close() }}
