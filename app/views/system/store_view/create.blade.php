
{{ Form::open( [ 'url'=>'system/store_view', 'class' => 'form form-horizontal' ] ) }}
    <h2 class="form-heading">New store view</h2>
    
    @if ( count($errors) > 0 )
	    <ul>
	        @foreach($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
    @endif
 	
 	<div class="form-group">
 		<label for="selectStore" class="col-sm-2 control-label">Store <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::select('store', $store_groups, Input::old('store'), [ 'class'=>'input-block-level form-control', 'required' => '', 'id' => 'selectStore' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
 		<label for="inputName" class="col-sm-2 control-label">Name <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::text('name', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
    	<label for="inputCode" class="col-sm-2 control-label">Code <span class="required">*</span></label>
	    <div class="col-sm-5">
    		{{ Form::text('code', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Code', 'required' => '', 'id' => 'inputCode' ] ) }}
    	</div>
    </div>
    
    <div class="form-group">
 		<label for="selectStatus" class="col-sm-2 control-label">Status <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::select('status', [ 0 => 'Disabled', 1 => 'Enabled' ], Input::old('status'), [ 'class'=>'input-block-level form-control', 'required' => '', 'id' => 'selectStatus' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
    	<label for="inputSortOrder" class="col-sm-2 control-label">Sort order</label>
	    <div class="col-sm-5">
    		{{ Form::text('sort_order', null, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Sort order', 'id' => 'inputSortOrder' ] ) }}
    	</div>
    </div>
    
    <div class="form-group">
    	<div class="col-sm-offset-2 col-sm-2">
    		{{ Form::submit('Save store view', array('class'=>'btn btn-large btn-primary btn-block'))}}
    	</div>
    </div>
{{ Form::close() }}
