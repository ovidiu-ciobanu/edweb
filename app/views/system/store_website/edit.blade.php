
{{ Form::open( [ 'url' => [ 'system/store_website', $website->website_id ], 'method' => 'put', 'class' => 'form form-horizontal' ] ) }}
    <h2 class="form-heading">Edit website</h2>
    
    @if ( count($errors) > 0 )
	    <ul>
	        @foreach($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
    @endif
 	
 	<div class="form-group">
 		<label for="inputName" class="col-sm-2 control-label">Name <span class="required">*</span></label>
	    <div class="col-sm-5">
	    	{{ Form::text('name', $website->name, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Name', 'required' => '', 'id' => 'inputName' ] ) }}
	    </div>
    </div>
    
    <div class="form-group">
    	<label for="inputCode" class="col-sm-2 control-label">Code <span class="required">*</span></label>
	    <div class="col-sm-5">
    		{{ Form::text('code', $website->code, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Code', 'required' => '', 'id' => 'inputCode' ] ) }}
    	</div>
    </div>
    
    <div class="form-group">
    	<label for="inputSortOrder" class="col-sm-2 control-label">Sort order</label>
	    <div class="col-sm-5">
    		{{ Form::text('sort_order', $website->sort_order, [ 'class'=>'input-block-level form-control', 'placeholder'=>'Sort order', 'id' => 'inputSortOrder' ] ) }}
    	</div>
    </div>
    
    <div class="form-group">
    	<label for="inputDefaultStore" class="col-sm-2 control-label">Default store</label>
	    <div class="col-sm-5">
    		{{ Form::select('default_store', $website->store_groups->lists( 'name', 'group_id' ), $website->default_group_id, [ 'class'=>'input-block-level form-control', 'id' => 'inputDefaultStore' ] ) }}
    	</div>
    </div>
    
    <div class="form-group">
    	<div class="col-sm-offset-2 col-sm-2">
    		{{ Form::submit('Save website', array('class'=>'btn btn-large btn-primary btn-block'))}}
    	</div>
    </div>
{{ Form::close() }}
