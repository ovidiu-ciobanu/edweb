<?php

namespace App\Commands\Magento\Orders;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Sales\Order;
use App\Models\Sales\Order\Item as OrderItem;
use App\Models\Sales\Order\Shipping as OrderShipping;
use App\Models\Sales\Order\Billing as OrderBilling;

class Import extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'magento:orders:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products from profiles.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $orders = \MagentoApi::salesOrderList()->getCollection();

        foreach ( $orders as $order ) {

            $objOrder = Order::where( 'increment_id', $order->getIncrementId() )->first();
            if ( !$objOrder ) {
                $objOrder = new Order();
                $objOrder->increment_id = $order->getIncrementId();
            }

            if ( $order->hasParentId() ) {
                $objOrder->parent_id = $order->getParentId();
            }
            $objOrder->store_id = $order->getStoreId();
            $objOrder->created_at = $order->getCreatedAt();
            $objOrder->updated_at = $order->getUpdatedAt();
            if ( $order->hasIsActive() ) {
                $objOrder->is_active = $order->getIsActive();
            }
            if ( $order->hasCustomerId() ) {
                $objOrder->customer_id = $order->getCustomerId();
            }
            $objOrder->tax_amount = $order->getTaxAmount();
            $objOrder->shipping_amount = $order->getShippingAmount();
            $objOrder->discount_amount = $order->getDiscountAmount();
            $objOrder->subtotal = $order->getSubtotal();
            $objOrder->grand_total = $order->getGrandTotal();
            if ( $order->hasTotalPaid() ) {
                $objOrder->total_paid = $order->getTotalPaid();
            }
            if ( $order->hasTotalRefunded() ) {
                $objOrder->total_refunded = $order->getTotalRefunded();
            }
            if ( $order->hasTotalQtyOrdered() ) {
                $objOrder->total_qty_ordered = $order->getTotalQtyOrdered();
            }
            if ( $order->hasTotalCanceled() ) {
                $objOrder->total_canceled = $order->getTotalCanceled();
            }
            if ( $order->hasTotalInvoiced() ) {
                $objOrder->total_invoiced = $order->getTotalInvoiced();
            }
            if ( $order->hasTotalOnlineRefunded() ) {
                $objOrder->total_online_refunded = $order->getTotalOnlineRefunded();
            }
            if ( $order->hasTotalOfflineRefunded() ) {
                $objOrder->total_offline_refunded = $order->getTotalOfflineRefunded();
            }

            if ( $order->hasBaseTaxAmount() ) {
                $objOrder->base_tax_amount = $order->getBaseTaxAmount();
            }
            if ( $order->hasBaseShippingAmount() ) {
                $objOrder->base_shipping_amount = $order->getBaseShippingAmount();
            }
            if ( $order->hasBaseDiscountAmount() ) {
                $objOrder->base_discount_amount = $order->getBaseDiscountAmount();
            }
            if ( $order->hasBaseSubtotal() ) {
                $objOrder->base_subtotal = $order->getBaseSubtotal();
            }
            if ( $order->hasBaseGrandTotal() ) {
                $objOrder->base_grand_total = $order->getBaseGrandTotal();
            }
            if ( $order->hasBaseTotalPaid() ) {
                $objOrder->base_total_paid = $order->getBaseTotalPaid();
            }
            if ( $order->hasBaseTotalRefunded() ) {
                $objOrder->base_total_refunded = $order->getBaseTotalRefunded();
            }
            if ( $order->hasBaseTotalQtyOrdered() ) {
                $objOrder->base_total_qty_ordered = $order->getBaseTotalQtyOrdered();
            }
            if ( $order->hasBaseTotalCanceled() ) {
                $objOrder->base_total_canceled = $order->getBaseTotalCanceled();
            }
            if ( $order->hasBaseTotalInvoiced() ) {
                $objOrder->base_total_invoiced = $order->getBaseTotalInvoiced();
            }
            if ( $order->hasBaseTotalOnlineRefunded() ) {
                $objOrder->base_total_online_refunded = $order->getBaseTotalOnlineRefunded();
            }
            if ( $order->hasBaseTotalOfflineRefunded() ) {
                $objOrder->base_total_offline_refunded = $order->getBaseTotalOfflineRefunded();
            }
            if ( $order->hasBillingAddressId() ) {
                $objOrder->billing_address_id = $order->getBillingAddressId();
            }
            if ( $order->hasBillingFirstname() ) {
                $objOrder->billing_firstname = $order->getBillingFirstname();
            }
            if ( $order->hasBillingLastname() ) {
                $objOrder->billing_lastname = $order->getBillingLastname();
            }
            if ( $order->hasShippingAddressId() ) {
                $objOrder->shipping_address_id = $order->getShippingAddressId();
            }
            if ( $order->hasShippingFirstname() ) {
                $objOrder->shipping_firstname = $order->getShippingFirstname();
            }
            if ( $order->hasShippingLastname() ) {
                $objOrder->shipping_lastname = $order->getShippingLastname();
            }
            if ( $order->hasBillingName() ) {
                $objOrder->billing_name = $order->getBillingName();
            }
            if ( $order->hasShippingName() ) {
                $objOrder->shipping_name = $order->getShippingName();
            }
            if ( $order->hasStoreToBaseRate() ) {
                $objOrder->store_to_base_rate = $order->getStoreToBaseRate();
            }
            if ( $order->hasStoreToOrderRate() ) {
                $objOrder->store_to_order_rate = $order->getStoreToOrderRate();
            }
            if ( $order->hasBaseToGlobalRate() ) {
                $objOrder->base_to_global_rate = $order->getBaseToGlobalRate();
            }
            if ( $order->hasBaseToOrderRate() ) {
                $objOrder->base_to_order_rate = $order->getBaseToOrderRate();
            }
            if ( $order->hasWeight() ) {
                $objOrder->weight = $order->getWeight();
            }
            if ( $order->hasStoreName() ) {
                $objOrder->store_name = $order->getStoreName();
            }
            if ( $order->hasRemoteIp() ) {
                $objOrder->remote_ip = $order->getRemoteIp();
            }
            if ( $order->hasStatus() ) {
                $objOrder->status = $order->getStatus();
            }
            if ( $order->hasState() ) {
                $objOrder->state = $order->getState();
            }
            if ( $order->hasAppliedRuleIds() ) {
                $objOrder->applied_rule_ids = $order->getAppliedRuleIds();
            }
            if ( $order->hasGlobalCurrencyCode() ) {
                $objOrder->global_currency_code = $order->getGlobalCurrencyCode();
            }
            if ( $order->hasBaseCurrencyCode() ) {
                $objOrder->base_currency_code = $order->getBaseCurrencyCode();
            }
            if ( $order->hasStoreCurrencyCode() ) {
                $objOrder->store_currency_code = $order->getStoreCurrencyCode();
            }
            if ( $order->hasOrderCurrencyCode() ) {
                $objOrder->order_currency_code = $order->getOrderCurrencyCode();
            }
            if ( $order->hasShippingMethod() ) {
                $objOrder->shipping_method = $order->getShippingMethod();
            }
            if ( $order->hasShippingDescription() ) {
                $objOrder->shipping_description = $order->getShippingDescription();
            }
            if ( $order->hasCustomerEmail() ) {
                $objOrder->customer_email = $order->getCustomerEmail();
            }
            if ( $order->hasCustomerFirstname() ) {
                $objOrder->customer_firstname = $order->getCustomerFirstname();
            }
            if ( $order->hasCustomerLastname() ) {
                $objOrder->customer_lastname = $order->getCustomerLastname();
            }
            if ( $order->hasQuoteId() ) {
                $objOrder->quote_id = $order->getQuoteId();
            }
            if ( $order->hasIsVirtual() ) {
                $objOrder->is_virtual = $order->getIsVirtual();
            }
            if ( $order->hasCustomerGroupId() ) {
                $objOrder->customer_group_id = $order->getCustomerGroupId();
            }
            if ( $order->hasCustomerNoteNotify() ) {
                $objOrder->customer_note_notify = $order->getCustomerNoteNotify();
            }
            if ( $order->hasCustomerIsGuest() ) {
                $objOrder->customer_is_guest = $order->getCustomerIsGuest();
            }
            if ( $order->hasEmailSent() ) {
                $objOrder->email_sent = $order->getEmailSent();
            }
            if ( $order->hasOrderId() ) {
                $objOrder->order_id = $order->getOrderId();
            }
            if ( $order->hasGiftMessageId() ) {
                $objOrder->gift_message_id = $order->getGiftMessageId();
            }

            $objOrder->save();

            $orderInfo = \MagentoApi::salesOrderInfo( [ 'orderIncrementId' => $order->getIncrementId() ] );
            
            // Products
            foreach ( $orderInfo->getItems() as $orderItem ) {

                $objOrderItem = OrderItem::where( 'item_id', $orderItem->item_id )->first();
                if ( !$objOrderItem ) {
                    $objOrderItem = new OrderItem();
                    $objOrderItem->item_id = $orderItem->item_id;
                }

                $objOrderItem->order_id = $orderItem->order_id;
                $objOrderItem->quote_item_id = $orderItem->quote_item_id;
                $objOrderItem->created_at = $orderItem->created_at;
                $objOrderItem->updated_at = $orderItem->updated_at;

                $objOrderItem->product_id = $orderItem->product_id;
                $objOrderItem->product_type = $orderItem->product_type;

                $objOrderItem->sku = $orderItem->sku;
                $objOrderItem->name = $orderItem->name;

                $objOrderItem->qty_ordered = $orderItem->qty_ordered;

                $objOrderItem->price = $orderItem->price;
                $objOrderItem->base_price = $orderItem->base_price;
                $objOrderItem->original_price = $orderItem->original_price;
                $objOrderItem->base_original_price = $orderItem->base_original_price;
                $objOrderItem->tax_percent = $orderItem->tax_percent;
                $objOrderItem->tax_amount = $orderItem->tax_amount;
                $objOrderItem->base_tax_amount = $orderItem->base_tax_amount;

                $objOrderItem->save();
            }

            // Shipping information
            $shipping = $orderInfo->getShippingAddress();

            $objShipping = OrderShipping::find( $shipping->address_id );
            if ( !$objShipping ) {
                $objShipping = new OrderShipping();
                $objShipping->id = $shipping->address_id;
            }

            $objShipping->order_id = $shipping->parent_id;
            $objShipping->firstname = $shipping->firstname;
            $objShipping->lastname = $shipping->lastname;
            $objShipping->street = $shipping->street;
            $objShipping->city = $shipping->city;
            $objShipping->region = $shipping->region;
            $objShipping->postcode = $shipping->postcode;
            $objShipping->telephone = $shipping->telephone;
            $objShipping->country_id = $shipping->country_id;
            $objShipping->save();

            // Billing information
            $billing = $orderInfo->getBillingAddress();
            $objBilling = OrderBilling::find( $billing->address_id );
            if ( !$objBilling ) {
                $objBilling = new OrderBilling();
                $objBilling->id = $billing->address_id;
            }

            $objBilling->order_id = $billing->parent_id;
            $objBilling->firstname = $billing->firstname;
            $objBilling->lastname = $billing->lastname;
            $objBilling->street = $billing->street;
            $objBilling->city = $billing->city;
            $objBilling->region = $billing->region;
            $objBilling->postcode = $billing->postcode;
            $objBilling->telephone = $billing->telephone;
            $objBilling->country_id = $billing->country_id;
            $objBilling->save();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
                //array( 'example', InputArgument::REQUIRED, 'An example argument.' ),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
                //array( 'exmple', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Import ID.', null ),
        );
    }

}
