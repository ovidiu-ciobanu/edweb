<?php namespace App\Commands\Export\Magento;
use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Entity\Category as Category;
use App\Models\Entity\Export as EntityExport;

class Categories extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'export:magento:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exports categories to Magento throught Magento API.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info( "Started categories export!" );

        if ( !\Session::has( 'store_id' ) ) {
            \Session::put( 'store_id', 0 );
        }

        $root_category_id = \MagentoApi::catalogCategoryTree()->getChildren()[ 0 ]->children[0]->category_id;
        $categories = Category::select('category_entity.*','eav_entity_varchar.value as parent_id','cat_id.value as category_id')
                ->leftJoin('eav_attribute',function($join){
                    $join->on('category_entity.entity_type_id','=','eav_attribute.entity_type_id')
                        ->where('eav_attribute.attribute_code','=','parent_id');

                })->leftJoin('eav_entity_varchar',function($join){
                    $join->on('category_entity.entity_id','=','eav_entity_varchar.entity_id')
                        ->on('category_entity.entity_type_id','=','eav_entity_varchar.entity_type_id')
                        ->on('eav_entity_varchar.attribute_id','=','eav_attribute.attribute_id');

                })->leftJoin('eav_attribute as cat_id_attr',function($join){
                    $join->on('category_entity.entity_type_id','=','cat_id_attr.entity_type_id')
                        ->where('cat_id_attr.attribute_code','=','category_id');

                })->leftJoin('eav_entity_varchar as cat_id',function($join){
                    $join->on('category_entity.entity_id','=','cat_id.entity_id')
                        ->on('category_entity.entity_type_id','=','cat_id.entity_type_id')
                        ->on('cat_id.attribute_id','=','cat_id_attr.attribute_id');
                })->orderBy(DB::raw(" LENGTH(parent_id),parent_id"),'ASC')->groupBy('category_entity.entity_id')->get();
        foreach ( $categories as $category ) {
            
            $catalogCategoryEntityCreate = new \stdClass();

            $catalogCategoryEntityCreate->name = $category->attributecategory_name()->value; // Name of the created category
            $catalogCategoryEntityCreate->is_active = 1; // Defines whether the category will be visible in the frontend
            $catalogCategoryEntityCreate->available_sort_by = [ 'position', 'name', 'price' ]; // All available options by which products in the category can be sorted
            $catalogCategoryEntityCreate->custom_design = null; // The custom design for the category (optional)
            $catalogCategoryEntityCreate->custom_apply_to_products = null; // Apply the custom design to all products assigned to the category (optional)
            $catalogCategoryEntityCreate->custom_design_from = null; // Date starting from which the custom design will be applied to the category (optional)
            $catalogCategoryEntityCreate->custom_design_to = null; // Date till which the custom design will be applied to the category (optional)
            $catalogCategoryEntityCreate->custom_layout_update = null; // Custom layout update (optional)
            $catalogCategoryEntityCreate->default_sort_by = 'position'; // The default option by which products in the category are sorted
            $catalogCategoryEntityCreate->description = null; // Category description (optional)
            $catalogCategoryEntityCreate->display_mode = null; // Content that will be displayed on the category view page (optional)
            $catalogCategoryEntityCreate->is_anchor = null; // Defines whether the category will be anchored (optional)
            $catalogCategoryEntityCreate->landing_page = null; // Landing page (optional)
            $catalogCategoryEntityCreate->meta_description = null; // Category meta description (optional)
            $catalogCategoryEntityCreate->meta_keywords = null; // Category meta keywords (optional)
            $catalogCategoryEntityCreate->meta_title = null; // Category meta title (optional)
            $catalogCategoryEntityCreate->page_layout = null; // Type of page layout that the category should use (optional)
            $catalogCategoryEntityCreate->url_key = null; // A relative URL path which can be entered in place of the standard target path (optional)
            $catalogCategoryEntityCreate->include_in_menu = 1; // Defines whether the category is visible on the top menu bar
            $catalogCategoryEntityCreate->filter_price_range = null; // Price range of each price level displayed in the layered navigation block (optional)
            $catalogCategoryEntityCreate->custom_use_parent_settings = 1; // Defines whether the category will inherit custom design settings of the category to which it is assigned. 1 - Yes, 0 - No (optional) 

            if ( $category->entity_export != null ) {

                // Update
                $this->line( "<info>Updating category: </info>" . $category->attributecategory_name()->value );
               
                $categoryArgs = [
                    'categoryId' => $category->entity_export->export_id,
                    'categoryData' => $catalogCategoryEntityCreate
                ];
                \MagentoApi::catalogCategoryUpdate( $categoryArgs );
            } else {

                // Create
                $this->line( "<info>Addign category: </info>" . $category->attributecategory_name()->value );
                if ($category->parent_id == 0) {
                    $parent_id = $root_category_id;
                } else {
                    foreach ($categories as $cat) {
                        if($cat->category_id == $category->parent_id) {
                            break;
                        }
                    }
                    if ($cat->entity_export) {
                        $parent_id = $cat->entity_export->export_id;
                    } else {
                        $parent_id = $root_category_id;
                    }
                }
                $categoryArgs = [
                    'parentId' => $parent_id,
                    'categoryData' => $catalogCategoryEntityCreate
                ];
                $export_id = \MagentoApi::catalogCategoryCreate( $categoryArgs );

                if ( $export_id ) {
                    $entity_export = new EntityExport();
                    $entity_export->entity_id = $category->entity_id;
                    $entity_export->entity_type_id = $category->entity_type_id;
                    $entity_export->export_id = $export_id;
                    $entity_export->save();
                }
            }
        }

        $this->info( "Finished categories export!" );
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
                //array( 'example', InputArgument::REQUIRED, 'An example argument.' ),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
                //array( 'example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null ),
        );
    }

}
