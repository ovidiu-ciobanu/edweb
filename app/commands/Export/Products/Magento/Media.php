<?php

namespace App\Commands\Export\Products\Magento;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Entity\Product as ProductEntity;

class Media extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'export:products:magento:media';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exports product media to Magento throught Magento API.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        if (!\Session::has('store_id')) {
            \Session::put('store_id', 0);
        }

        $product_ids = $this->option('product_id');
        if (count($product_ids) <= 0) {
            $this->error('No product id specified!');
            exit;
        }

        $this->info('Started image import!');

        $products = ProductEntity::whereIn('entity_id', $product_ids)->get();

        foreach ($products as $product) {
            $images = array();
            if ($product->attributeImage()->value) {
                $images[] = $product->attributeImage()->value;
            }
            if ($product->attributeExtraimage1()->value) {
                $images[] = $product->attributeExtraimage1()->value;
            }
            if ($product->attributeExtraimage2()->value) {
                $images[] = $product->attributeExtraimage2()->value;
            }
            if ($product->attributeExtraimage3()->value) {
                $images[] = $product->attributeExtraimage3()->value;
            }
            if (count($images)) {
                foreach ($images as $image) {
                    $image = $product->attributeImage()->value;
                    $imageContent = false;
                    try {
                        $imageContent = file_get_contents($image);
                    } catch (\Exception $e) {
                        
                    }
                    if (false === $imageContent) {
                        continue;
                    }

                    $imageNameArray = explode('/', $image);
                    $imageName = explode('.', $imageNameArray[count($imageNameArray) - 1]);
                    array_pop($imageName);
                    $label = implode('_', $imageName);

                    $currentImageCollection = [];
                    try {
                        $currentImageCollection = \MagentoApi::catalogProductAttributeMediaList([ 'product' => $product->attributeSku()->value, 'storeView' => null, 'identifierType' => 'sku'])->getCollection();
                    } catch (\Exception $e) {
                        
                    }

                    $currentMediaList = [];
                    foreach ($currentImageCollection as $imageCollection) {
                        $currentMediaList[$imageCollection->getLabel()] = $imageCollection;
                    }

                    $catalogProductImageFileEntity = new \stdClass();
                    $catalogProductImageFileEntity->content = base64_encode($imageContent);
                    $catalogProductImageFileEntity->mime = 'image/jpeg';
                    $catalogProductImageFileEntity->name = $label;

                    $catalogProductAttributeMediaCreateEntity = new \stdClass();
                    $catalogProductAttributeMediaCreateEntity->file = $catalogProductImageFileEntity;
                    $catalogProductAttributeMediaCreateEntity->label = $label;
                    $catalogProductAttributeMediaCreateEntity->position = '';
                    $catalogProductAttributeMediaCreateEntity->types = [ 'image', 'small_image', 'thumbnail'];
                    $catalogProductAttributeMediaCreateEntity->exclude = '';
                    $catalogProductAttributeMediaCreateEntity->remove = '';

                    if (array_key_exists($label, $currentMediaList)) {

                        $this->info('Updating image!');
                        \MagentoApi::catalogProductAttributeMediaUpdate([ 'product' => $product->attributeSku()->value, 'file' => $currentMediaList[$label]->getFile(), 'data' => $catalogProductAttributeMediaCreateEntity, 'storeView' => null, 'identifierType' => 'sku']);
                    } else {

                        $this->info('Adding image!');
                        \MagentoApi::catalogProductAttributeMediaCreate([ 'product' => $product->attributeSku()->value, 'data' => $catalogProductAttributeMediaCreateEntity, 'storeView' => null, 'identifierType' => 'sku']);
                    }
                }
            }
        }

        $this->info('Finished image import!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
                //array( 'example', InputArgument::REQUIRED, 'An example argument.' ),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
            array('--product_id', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Product SKU for which the image to import.', null),
        );
    }

}
