<?php namespace App\Commands\Export\Products;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Entity\Product as ProductEntity;
use App\Models\Entity\Export as EntityExport;

class Magento extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'export:products:magento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exports products to Magento throught Magento API.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Started export!");
        
        if ( !\Session::has( 'store_id' ) ) {
            \Session::put( 'store_id', 0 );
        }
        
        // Export all categories so we can use the ids below when sending products.
        $this->call( 'export:magento:categories' );
        
        $default_category_id = \MagentoApi::catalogCategoryTree()->getChildren()[ 0 ]->children[0]->category_id;
        foreach ( ProductEntity::all() as $product ) {
            
            $categories = [$default_category_id];
            foreach( $product->categories as $category ) {
                if ( $category->entity_export != null ) {
                    $categories[] = $category->entity_export->export_id;
                }
            }
           
            $catalogProductCreateEntity = new \stdClass();
            $catalogProductCreateEntity->categories = $categories;
            $catalogProductCreateEntity->websites = [ 1 ];
            $catalogProductCreateEntity->name = $product->attributeProductName()->value;
            $catalogProductCreateEntity->description = $product->attributeDescription()->value;
            $catalogProductCreateEntity->short_description = $product->attributeDescription()->value;
            $catalogProductCreateEntity->weight = '0';
            $catalogProductCreateEntity->status = '1';
            $catalogProductCreateEntity->url_key = '';
            $catalogProductCreateEntity->url_path = '';
            $catalogProductCreateEntity->visibility = '4';
            $catalogProductCreateEntity->category_ids = '';
            $catalogProductCreateEntity->website_ids = '';
            $catalogProductCreateEntity->has_options = '';
            $catalogProductCreateEntity->gift_message_available = '';
            $catalogProductCreateEntity->price = $product->attributeUnitPrice()->value;
            $catalogProductCreateEntity->special_price = '';
            $catalogProductCreateEntity->special_from_date = '';
            $catalogProductCreateEntity->special_to_date = '';
            $catalogProductCreateEntity->tax_class_id = '0';

            $catalogProductTierPriceEntity = new \stdClass();
            $catalogProductTierPriceEntity->customer_group_id = '';
            $catalogProductTierPriceEntity->website = '';
            $catalogProductTierPriceEntity->qty = 1;
            $catalogProductTierPriceEntity->price = 1.0;
            $catalogProductCreateEntity->tier_price = $catalogProductTierPriceEntity;

            $catalogProductCreateEntity->meta_title = $product->attributeProductName()->value;
            $catalogProductCreateEntity->meta_keyword = $product->attributeProductName()->value;
            $catalogProductCreateEntity->meta_description = $product->attributeDescription()->value;
            $catalogProductCreateEntity->custom_design = '';
            $catalogProductCreateEntity->custom_layout_update = '';
            $catalogProductCreateEntity->options_container = '';

            $catalogProductAdditionalAttributesEntity = new \stdClass();
            $catalogProductAdditionalAttributesEntity->multi_data = [ ];
            $catalogProductAdditionalAttributesEntity->single_data = [ ];
            $catalogProductCreateEntity->additional_attributes = $catalogProductAdditionalAttributesEntity;


            $catalogInventoryStockItemUpdateEntity = new \stdClass();
            $catalogInventoryStockItemUpdateEntity->qty = '10';
            $catalogInventoryStockItemUpdateEntity->is_in_stock = 1;
            $catalogInventoryStockItemUpdateEntity->manage_stock = 0;
            $catalogInventoryStockItemUpdateEntity->use_config_manage_stock = 1;
            $catalogInventoryStockItemUpdateEntity->min_qty = 0;
            $catalogInventoryStockItemUpdateEntity->use_config_min_qty = 1;
            $catalogInventoryStockItemUpdateEntity->min_sale_qty = 0;
            $catalogInventoryStockItemUpdateEntity->use_config_min_sale_qty = 1;
            $catalogInventoryStockItemUpdateEntity->max_sale_qty = 0;
            $catalogInventoryStockItemUpdateEntity->use_config_max_sale_qty = 1;
            $catalogInventoryStockItemUpdateEntity->is_qty_decimal = 1;
            $catalogInventoryStockItemUpdateEntity->backorders = 0;
            $catalogInventoryStockItemUpdateEntity->use_config_backorders = 1;
            $catalogInventoryStockItemUpdateEntity->notify_stock_qty = 0;
            $catalogInventoryStockItemUpdateEntity->use_config_notify_stock_qty = 1;
            $catalogProductCreateEntity->stock_data = $catalogInventoryStockItemUpdateEntity;
            
            if ( $product->entity_export != null ) {

                // Update
                $this->line( "<info>Updating product: </info>" . $product->attributeProductName()->value );

                // Update
                $productArgs = [
                    'product' => $product->attributeSku()->value,
                    'productData' => $catalogProductCreateEntity,
                    'storeView' => null,
                    'identifierType' => 'sku'
                ];
                \MagentoApi::catalogProductUpdate( $productArgs );
            } else {

                // Create
                $this->line( "<info>Addign product: </info>" . $product->attributeProductName()->value );

                $productArgs = [
                    'type' => 'simple',
                    'set' => 4,
                    'sku' => $product->attributeSku()->value,
                    'productData' => $catalogProductCreateEntity
                ];
                $export_id = \MagentoApi::catalogProductCreate( $productArgs );

                if ( $export_id ) {
                    $entity_export = new EntityExport();
                    $entity_export->entity_id = $product->entity_id;
                    $entity_export->entity_type_id = $product->entity_type_id;
                    $entity_export->export_id = $export_id;
                    $entity_export->save();
                }
            }
            
            $argument = [];
            $argument['--product_id'][] = $product->entity_id;
            $this->call( 'export:products:magento:media', $argument );
            
            $this->line('');
        }
        
        $this->info("Finished export!");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
                //array( 'example', InputArgument::REQUIRED, 'An example argument.' ),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
                //array( 'example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null ),
        );
    }

}
