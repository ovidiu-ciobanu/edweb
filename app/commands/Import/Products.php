<?php

namespace App\Commands\Import;

use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\System\Import as SystemImport;
use App\Models\System\Cronjobb as Cronjobb;
use App\Models\Entity\Product as Product;
use App\Models\Entity\Category as Category;
use App\Models\Eav\Entity\Type as EavEntityType;
use App\Models\Eav\Attribute\Set as EavAttributeSet;
use App\Models\Eav\Attribute as EavAttribute;
use App\Models\Eav\Attribute\Input\Type as EavAttributeInputType;
use App\Models\Eav\Entity\Text as EavEntityText;
use App\Models\Eav\Entity\Varchar as EavEntityVarchar;
use App\Models\Eav\Entity\Int as EavEntityInt;
use App\Models\Eav\Entity\Decimal as EavEntityDecimal;
use App\Models\Eav\Entity\Datetime as EavEntityDatetime;
use App\Models\Core\Store as CoreStore;

class Products extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:products';
    protected $cronjob = null;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import products from profiles.';

    /**
     * Hold the stores.
     * 
     * @var array
     */
    private $_stores;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if ( !\Session::has( 'store_id' ) ) {
            \Session::put( 'store_id', 0 );
        }
        
       
        $this->cronjob = Cronjobb::where('cron_type','=','import')
                ->where('status','=','started')   
                ->first();
        if ($this->cronjob) {
            $this->cronjob->params = json_decode($this->cronjob->params);
        }
        if ($this->cronjob) {
            $this->info( "Import products started!" );
            $this->_stores = CoreStore::all();

            if ($this->cronjob->params->import_id) {
                $import = SystemImport::where( 'import_id','=', $this->cronjob->params->import_id )->firstOrFail();
                if ($import) {
                    $this->cronjob->status = 'in progress';
                    $this->cronjob->params = json_encode($this->cronjob->params);
                    $this->cronjob->save();
                    $this->setupImport( $import );
                    
                }
                
            }
            
            $this->info( "Import products finished!" );
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
                //array( 'example', InputArgument::REQUIRED, 'An example argument.' ),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array( 'import_id', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Import ID.', null ),
        );
    }

    /**
     * Setup import process
     * 
     * @param App\Models\System\Import $import
     */
    private function setupImport( \App\Models\System\Import $import )
    {
        $xml_obj = simplexml_load_file( storage_path( 'imports' ) . '/' . $import->path );
        $this->cronjob->total = $xml_obj->count();
        $this->cronjob->save();
        if ( is_object( $xml_obj ) ) {
            $this->runImport( $xml_obj, $import->profile );
        }
    }

    /**
     * Add products.
     * 
     * @param \App\Models\System\Import\Profile $profile
     */
    private function runImport( $xml_obj, \App\Models\System\Import\Profile $profile )
    {
       
        foreach ( $xml_obj as $obj ) {
            $this->cronjob = Cronjobb::where('cron_type','=','import')
                ->where('status','=','started')   
                ->first();
            if ($this->cronjob->status=='aborted') {
                $this->info( 'Import aborted by user.' );
                die();
            }
            $this->cronjob->step++;
            $this->cronjob->save();
            
            if ( null !== $profile->attributes->first() ) {
                if ( array_key_exists( $profile->attributes->first()->key, (array) $obj ) ) {
                    $this->addProduct( $obj, $profile );
                }
            }
        }
    }

    private function addProduct( $obj, $profile )
    {
        $product_id = null;
        $category_id = null;
        foreach ( $profile->profile_attribute_set_group->sets as $profile_attribute_set_group_attribute_set ) {
            $eav_attribute_set = $profile_attribute_set_group_attribute_set->attribute_set;
            switch ( $eav_attribute_set->entity_type->entity_type_code ) {
                case EavEntityType::PRODUCT_CODE:
                    // Check by product 'sku' attribute
                    $uniqueAttribute = EavAttribute::where( 'attribute_code', '=', 'sku' )->first();
                    $uniqueKey = $profile->attributes()->where( 'attribute_id', '=', $uniqueAttribute->attribute_id )->first()->key;

                    $productUniqueAttribute = EavEntityVarchar::where( 'value', '=', $obj->{ $uniqueKey } )->where( 'entity_type_id', '=', $eav_attribute_set->entity_type->entity_type_id )->first();
                    if ( null === $productUniqueAttribute ) {
                        $entity = new Product();
                        $this->info( 'Adding new product.' );
                    } else {
                        $entity = Product::find( $productUniqueAttribute->entity_id );
                        $this->line( '<info>Updating: </info>' . $entity->attributeProductName()->value . ' product.' );
                    }

                    break;

                case EavEntityType::CATEGORY_CODE:
                    // Check by category 'category_name' attribute
                    $uniqueAttribute = EavAttribute::where( 'attribute_code', '=', 'category_name' )->first();
                    $categoryProfileAttribute = $profile->attributes()->where( 'attribute_id', '=', $uniqueAttribute->attribute_id )->first();
                    $uniqueKey = $categoryProfileAttribute->key;

                    $categoryUniqueAttribute = EavEntityVarchar::where( function( $query ) use ( $obj, $uniqueKey, $categoryProfileAttribute ) {
                                $query->where( 'value', '=', $obj->{ $uniqueKey } )->orWhere( 'value', '=', $categoryProfileAttribute->default_value )->orWhere( 'value', '=', $categoryProfileAttribute->eav_attribute->default_value );
                            } )->where( 'entity_type_id', '=', $eav_attribute_set->entity_type->entity_type_id )->first();

                    if ( null === $categoryUniqueAttribute ) {
                        $entity = new Category();
                        $this->info( 'Adding new category.' );
                    } else {
                        $entity = Category::find( $categoryUniqueAttribute->entity_id );
                        $this->line( '<info>Updating: </info>' . $entity->attributeCategoryName()->value . ' category.' );
                    }

                    break;
            }

            if ( null === $entity ) {
                return false;
            }

            $entity->entity_type_id = $eav_attribute_set->entity_type->entity_type_id;
            $entity->attribute_set_id = $eav_attribute_set->attribute_set_id;
            $entity->save();

            switch ( $eav_attribute_set->entity_type->entity_type_code ) {
                case EavEntityType::PRODUCT_CODE:
                    $product_id = $entity->entity_id;
                    break;

                case EavEntityType::CATEGORY_CODE:
                    $category_id = $entity->entity_id;
                    break;
            }

            $this->addOrUpdateAttributes( $profile->attributes, $entity, $obj );
        }

        if ( $product_id !== null && $category_id !== null ) {
            $product = Product::find( $product_id );
            $product_categories = $product->categories->lists( 'entity_id' );
            if ( !in_array( $category_id, $product_categories ) ) {
                $product->categories()->attach( $category_id );
            }
        }
    }

    private function addOrUpdateAttributes( $attributes, $entity, $obj )
    {
        
        foreach ( $attributes as $attribute ) {
            
            $eav_attribute = EavAttribute::find( $attribute->attribute_id );
            if ( null === $eav_attribute ) {
                break;
            }
            
            foreach ( $this->_stores as $store ) {
                $eav_entity_value = null;
                
                if ( EavAttributeInputType::TEXTAREA == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityText::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::TEXT == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityVarchar::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::BOOLEAN == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityInt::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::DECIMAL == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityDecimal::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::DATETIME == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityDatetime::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                }

                if ( null !== $eav_entity_value ) {
                    if ($eav_attribute->attribute_code == 'product_category') {
                        $entity->categories()->detach();
                        $value = $this->addOrUpdateCategories(2, $obj->{ $attribute->key },$entity->entity_id);
                    } else {
                        $value = trim( (string) $obj->{ $attribute->key } );
                    }
                    if ( '' === $value ) {
                        $value = $attribute->default_value;
                    }
                    if ( '' === $value ) {
                        $value = $eav_attribute->default_value;
                    }
                    $eav_entity_value->entity_type_id = $entity->entity_type_id;
                    $eav_entity_value->attribute_id = $attribute->attribute_id;
                    $eav_entity_value->store_id = $store->store_id;
                    $eav_entity_value->entity_id = $entity->entity_id;
                    $eav_entity_value->value = $value;

                    if ( $eav_entity_value->isDirty() ) {
                       $eav_entity_value->save();
                    }
                }
            }
            
        }
        //die();
    }
    
    function addOrUpdateCategory($attributes, $obj, $product_id) {
        
        $uniqueAttribute = EavAttribute::where( 'attribute_code', '=', 'category_id' )->first();
        $uniqueKey = $uniqueAttribute->default_value;
        
        $categoryUniqueAttribute = EavEntityVarchar::where( function( $query ) use ( $obj, $uniqueKey ) {
                    $query->where( 'value', '=', $obj->{ $uniqueKey } );
        })->where( 'entity_type_id', '=', $attributes[0]['entity_type_id'] )->first();

        if ( null === $categoryUniqueAttribute ) {
            $entity = new Category();
            $this->info( 'Adding new category.' );
        } else {
            $entity = Category::find( $categoryUniqueAttribute->entity_id );
            $this->line( '<info>Updating: </info>' . $entity->attributeCategoryName()->value . ' category.' );
        }
        $entity->entity_type_id = $attributes[0]['entity_type_id'];
        $entity->attribute_set_id = $attributes[0]['attr_set_id'];
        $entity->save();
        if (!$entity->products->contains($product_id)) {
            $entity->products()->attach($product_id);
        }
        if (isset($obj->tradepoint_category)){
            $parent_id = $this->addOrUpdateCategory($attributes, $obj->tradepoint_category,$product_id);
        } else {
            $parent_id = 0;
            
        }
                
        foreach ( $attributes as $attribute ) {
            
            $eav_attribute = EavAttribute::find( $attribute->attribute_id );
            if ( null === $eav_attribute ) {
                break;
            }
            
            
            foreach ( $this->_stores as $store ) {
                $eav_entity_value = null;
                
                if ( EavAttributeInputType::TEXTAREA == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityText::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::TEXT == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityVarchar::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::BOOLEAN == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityInt::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::DECIMAL == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityDecimal::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                } elseif ( EavAttributeInputType::DATETIME == $eav_attribute->frontend_input ) {
                    $eav_entity_value = EavEntityDatetime::firstOrNew( [ 'entity_type_id' => $entity->entity_type_id, 'attribute_id' => $attribute->attribute_id, 'store_id' => $store->store_id, 'entity_id' => $entity->entity_id ] );
                }
                if ($eav_attribute->attribute_code == 'parent_id') {
                    $value = $parent_id;
                } else {
                    $value = trim( (string) $obj->{ $eav_attribute->default_value } );
                }
                if ($eav_attribute->attribute_code == 'category_id') {
                    $new_parent_id = $value;
                }
                if ( '' === $value ) {
                    $value = $attribute->default_value;
                }
                if ( '' === $value ) {
                    $value = $eav_attribute->default_value;
                }
                $eav_entity_value->entity_type_id = $entity->entity_type_id;
                $eav_entity_value->attribute_id = $attribute->attribute_id;
                $eav_entity_value->store_id = $store->store_id;
                $eav_entity_value->entity_id = $entity->entity_id;
                $eav_entity_value->value = $value;
                if ( $eav_entity_value->isDirty() ) {
                   $eav_entity_value->save();
                }
            }
            
        }
        return $new_parent_id;
        
    }
    
    function addOrUpdateCategories($attr_set_id, $obj,$product_id) {
        $attribute_set = EavAttributeSet::find( $attr_set_id );
        $attribute_group_attributes = $attribute_set->groups_attributes;
        foreach ( $attribute_group_attributes as $attribute_group_attribute ) {
            $attribute = $attribute_group_attribute->attribute;
            $attribute['attr_set_id'] = $attr_set_id;
            $attribute['entity_type_id'] = $attribute_set->entity_type_id;
            $attributes[] = $attribute;
        }
        
        
        foreach($obj as $category) {
             foreach($category as $cat) {
                $this->addOrUpdateCategory($attributes, $cat, $product_id);
             }
        }
        
        $product = Product::find($product_id);
        $result = '';
        foreach ($product->categories as $cat) {
            $result .= $cat->entity_id . ',';
        }
        return trim($result,',');
    }

}
