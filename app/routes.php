<?php

Route::get( '/', 'App\Controllers\DashboardController@index' )->before( 'auth' );

Route::group( [ 'before' => 'guest' ], function() {

    //Route::get( 'register', 'App\Controllers\RegisterController@create' );
    //Route::post( 'register', 'App\Controllers\RegisterController@store' );

    Route::get( 'login', 'App\Controllers\SessionController@create' );
    Route::post( 'login', 'App\Controllers\SessionController@store' );
} );
Route::get( 'logout', 'App\Controllers\SessionController@destroy' );

Route::group( [ 'before' => 'auth' ], function() {

    Route::resource( 'user', 'App\Controllers\UserController' );
    Route::resource( 'log', 'App\Controllers\LogController', [ 'except' => [ 'create', 'store', 'edit', 'update', 'destroy' ] ] );
} );

Route::group( [ 'before' => 'auth', 'prefix' => 'catalog' ], function() {

    Route::resource( 'product', 'App\Controllers\Catalog\ProductController', [ 'except' => [ 'show' ] ] );
    Route::get('product/destroyall', 'App\Controllers\Catalog\ProductController@destroy_all');
    Route::resource( 'category', 'App\Controllers\Catalog\CategoryController', [ 'except' => [ 'show' ] ] );
    Route::get('category/destroyall', 'App\Controllers\Catalog\CategoryController@destroy_all');
} );

Route::group( [ 'before' => 'auth', 'prefix' => 'entity' ], function() {

    Route::resource( 'attribute', 'App\Controllers\Entity\AttributeController', [ 'except' => [ 'show' ] ] );

    Route::group( [ 'prefix' => 'attribute' ], function() {
        Route::resource( 'set', 'App\Controllers\Entity\Attribute\SetController', [ 'except' => [ 'show' ] ] );
    } );
} );

Route::group( [ 'before' => 'auth', 'prefix' => 'system' ], function() {

    Route::resource( 'store', 'App\Controllers\System\StoreController' );
    Route::resource( 'store_view', 'App\Controllers\System\StoreViewController', [ 'except' => [ 'index', 'show' ] ] );
    Route::resource( 'store_website', 'App\Controllers\System\StoreWebsiteController', [ 'except' => [ 'index', 'show' ] ] );

    Route::resource( 'user', 'App\Controllers\System\UserController' );

    Route::resource( 'import', 'App\Controllers\System\ImportController', [ 'except' => 'show' ] );
    Route::group( [ 'prefix' => 'import' ], function() {
        Route::resource( 'profile', 'App\Controllers\System\Import\ProfileController', [ 'except' => 'show' ] );
        Route::get( 'execute/{import_id}', 'App\Controllers\System\ImportController@addImportToCron');
        Route::get( 'getimportstep', 'App\Controllers\System\ImportController@getImportStep');
        Route::resource( 'profile/attribute/set/group', 'App\Controllers\System\Import\Profile\Attribute\Set\GroupController' );
    } );
} );

Route::group( [ 'before' => 'auth', 'prefix' => 'sales' ], function() {
    Route::resource( 'order', 'App\Controllers\Sales\OrderController' );
} );

Route::group( [ 'before' => 'auth', 'prefix' => 'ajax' ], function() {
    Route::group( [ 'prefix' => 'catalog' ], function() {
        Route::resource( 'category', 'App\Controllers\Ajax\Catalog\CategoryController', [ 'only' => 'update' ] );
        
        Route::resource( 'category/profit/percent', 'App\Controllers\Ajax\Catalog\Category\ProfitPercentController', ['only' => 'store' ] );
        Route::resource( 'product/profit/percent', 'App\Controllers\Ajax\Catalog\Product\ProfitPercentController', ['only' => 'store' ] );
    } );
} );

/* Route for import products
Route::get( '/import/execute/{import_ids_string?}', function( $import_ids_string = null ) {
    $outputStream = new \Symfony\Component\Console\Output\StreamOutput( fopen( 'php://output', 'w' ) );

    $command_arguments = [ ];
    $import_ids = explode( '-', $import_ids_string );
    foreach ( $import_ids as $import_id ) {
        if ( $import_id != '' ) {
            $command_arguments['--import_id'][] = $import_id;
        }
    }

    Artisan::call( 'import:products', $command_arguments, $outputStream );
} )->before( 'auth' );*/

// Route to send product to magento
Route::get( '/export/products/magento', function( ) {
    $outputStream = new \Symfony\Component\Console\Output\StreamOutput( fopen( 'php://output', 'w' ) );

    Artisan::call( 'export:products:magento', [ ], $outputStream );
} )->before( 'auth' );

// Route to run migrations
Route::get( '/migrations/run', function() {
    Artisan::call( 'migrate' );
} )->before( 'auth' );
